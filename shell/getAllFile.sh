#!/bin/bash
getAllFile(){
    for ele in `ls $1`
    do
        dir_or_file=$1"/"$ele
        if [ -d $dir_or_file ]
        then
            getAllFile $dir_or_file $2
        elif [ "${dir_or_file##*.}"x = "$2"x ]
        then
            first=${dir_or_file#*/}
            echo ${first#*/}
        fi
    done
}

getAllFile ../src $1