#include "Tileselect_Exp.h"
#include "TL_define.h"

#include <assert.h>
#include <string.h>

void emit(AS_instr i);


// tile gadgets
static Temp_temp isBOP(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_BINOP:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(INST_MAX_SIZE);
        int eb1_id = 1;
        Exp_bind eb0 = getExpType(e->u.BINOP.left, 0);
        if(!eb0->tl) eb1_id = 0;
        Exp_bind eb1 = getExpType(e->u.BINOP.right, eb1_id);
        Temp_tempList src = mergeTempList(eb0->tl, eb1->tl);
        checkTempListSafety(src);
        sprintf(inst, 
            BOP(%s, %s, %s), 
            mapBOP(e->u.BINOP.op), 
            eb0->format, 
            eb1->format);
        emit(
            AS_Oper(
                inst,
                Temp_TempList(r, NULL),
                src,
                NULL
            )
        );
    }
    break;
    case T_CONST:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(INST_MAX_SIZE);
        sprintf(inst, BOP(%s, %d, 0), mapBOP(T_plus), e->u.CONST);
        emit(
            AS_Oper(
                inst,
                Temp_TempList(r, NULL),
                NULL,
                NULL
            )
        );
    }
    break;
    case T_TEMP:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(INST_MAX_SIZE);
        sprintf(inst, BOP(%s, %%`s0, 0), mapBOP(T_plus));
        emit(
            AS_Move(
                inst,
                Temp_TempList(r, NULL),
                Temp_TempList(e->u.TEMP, NULL)
            )
        );
    }
    break;
    case T_NAME:
    {
        assert(0);
    }
    break;
    default:
    break;
    }
   
    return r;
}


static Temp_temp isLOAD(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_MEM:
    {
        string inst1 = checked_malloc(INST_MAX_SIZE);
        string inst2 = checked_malloc(INST_MAX_SIZE);
        string inst3 = checked_malloc(INST_MAX_SIZE);
        Temp_temp r1 = Temp_newtemp();
        Temp_temp r2 = Temp_newtemp();
        r = Temp_newtemp();
        switch (e->u.MEM->kind)
        {
        case T_BINOP:
        {
            int eb1_id = 1;
            Exp_bind eb0 = getExpType(e->u.MEM->u.BINOP.left, 0);
            if(!eb0->tl) eb1_id = 0;
            Exp_bind eb1 = getExpType(e->u.MEM->u.BINOP.right, eb1_id);
            Temp_tempList src = mergeTempList(eb0->tl, eb1->tl);
            checkTempListSafety(src);
            sprintf(inst1, BOP(%s, %s, %s), mapBOP(T_plus), eb0->format, eb1->format);
            sprintf(inst2, INTTOPTR(%%`s0));
           
            sprintf(inst3, LOAD(%%`s0));
            emit(
                AS_Oper(
                    inst1, 
                    Temp_TempList(r1, NULL),
                    src,
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    Temp_TempList(r2, NULL),
                    Temp_TempList(r1, NULL),
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst3,
                    Temp_TempList(r, NULL),
                    Temp_TempList(r2, NULL),
                    NULL
                )
            );

        }
        break;
        default:
        {
            Exp_bind eb = getExpType(e->u.MEM, 0);

            sprintf(inst1, INTTOPTR(%s), eb->format);
            sprintf(inst2, LOAD(%%`s0));
            emit(
                AS_Oper(
                    inst1,
                    Temp_TempList(r1, NULL),
                    eb->tl,
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    Temp_TempList(r, NULL),
                    Temp_TempList(r1, NULL),
                    NULL
                )
            );
        }
        break;
        }
    }
    break;
    
    default:
        break;
    }

    return r;
}



static Temp_temp isCALL_EXP(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_CALL:
    {
        // assert(e->u.CALL.obj->kind == T_TEMP);
        Exp_bind eb0 = getExpType(e->u.CALL.obj, 0);
        assert(eb0->tl);
        r = Temp_newtemp();
        Temp_temp r1 = Temp_newtemp();
        string inst1 = checked_malloc(INST_MAX_SIZE);
        string inst2 = checked_malloc(INST_MAX_SIZE);

        Temp_tempList tl = Temp_TempList(NULL, NULL);
        sprintf(inst1,
            INTTOPTR(%s),
            eb0->format
        );
        sprintf(inst2, 
            CALL_EXP(%%`s0, %s), 
            T_expList_to_string(e->u.CALL.args, tl, 1)
        );
        if(!tl->head){
            tl = NULL;
        }
     
        checkTempListSafety(tl);
        emit(
            AS_Oper(
                inst1,
                Temp_TempList(r1, NULL),
                eb0->tl,
                NULL
            )
        );
        emit(
            AS_Oper(
                inst2,
                Temp_TempList(r, NULL),
                mergeTempList(Temp_TempList(r1, NULL), tl),
                NULL
            )
        );
    }    
    break;
    case T_ExtCALL:
    {
        if(!strcmp(e->u.ExtCALL.extfun, "getint")
        || !strcmp(e->u.ExtCALL.extfun, "getch")
        || !strcmp(e->u.ExtCALL.extfun, "getarray")
        || !strcmp(e->u.ExtCALL.extfun, "malloc")){
            r = Temp_newtemp();
            string inst = checked_malloc(INST_MAX_SIZE);
            Temp_tempList tl = Temp_TempList(NULL, NULL);
            sprintf(inst, 
                CALL_EXP(@%s, %s), 
                e->u.ExtCALL.extfun,
                T_expList_to_string(e->u.ExtCALL.args, tl, 0)
            );
            if(!tl->head){
                tl = NULL;
            }
            //debug
            // fprintf(stderr, "%s\n", inst);
            checkTempListSafety(tl);
            emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    tl,
                    NULL
                )
            );
        }
    }
    break;
    default:
        break;
    }
    return r; 

}



Temp_temp munchExp(T_exp e){
    Temp_temp r;
    r = isLOAD(e);
    if(r) return r;
    r = isCALL_EXP(e);
    if(r) return r;
    r = isBOP(e);
    if(r) return r;
    assert(0);
    return NULL;
}

