#include "Tileselect_Main.h"
#include "Tileselect_Stm.h"
#include "TL_define.h"
#include "../Utils/symbol.h"
#include "../Canon/API_canon.h"
#include <assert.h>
#include <string.h>

static AS_instrList il;
static const int SingleParam_Length = 20;
static const int PrologExtra = 60;


void emit(AS_instr i){
    assert(i);
    il->tail = AS_InstrList(i, NULL);
    il = il->tail;
}



AS_instrList munchFuncProlog(string fname, Temp_tempList args){
    int args_num = 0;
    for(Temp_tempList now = args; now; now = now->tail) args_num++;
    string func_head = NULL;
    if(args_num > 0){
        string args_str = checked_malloc(args_num * SingleParam_Length);
        // sprintf(args_str, "define i64 @%s(", fname);
        while(args){
            string s = checked_malloc(SingleParam_Length);
            sprintf(s, "i64 %%r%s,", Temp_look(Temp_name(), args->head));
            strcat(args_str, s);
            args = args->tail;
        }
        args_str[strlen(args_str) - 1] = '\0';

        func_head = checked_malloc(PrologExtra + strlen(args_str));
        sprintf(func_head, "define i64 @%s(%s){", fname, args_str);
    }else{
        func_head = checked_malloc(PrologExtra);
        sprintf(func_head, "define i64 @%s(){", fname);
    }
    return AS_InstrList(AS_Oper(func_head, NULL, NULL, NULL), NULL);
}

AS_instrList munchFuncEpilog(struct C_block c){
    string last_label = checked_malloc(10);
    sprintf(last_label, "%s:", S_name(c.label));
    return 
    AS_InstrList(
        AS_Label(last_label, c.label),
        AS_InstrList(
            AS_Oper("ret i64 -1", NULL, NULL, NULL),
            AS_InstrList(
                AS_Oper("}", NULL, NULL, NULL), 
                NULL
            )
        )
    );
    // return AS_InstrList(
    //     AS_Oper("}", NULL, NULL, NULL),
    //     NULL
    // );
        
}

static void checkBlockList(AS_blockList bl){
    while(bl){
        assert(bl->head);
        bl = bl->tail;
    }
}

static void print_AS_blockList(AS_blockList bl){
    while(bl){
        AS_block b = bl->head;
        for(AS_instrList il = b->instrs; il; il = il->tail){
            AS_instr i = il->head;
            switch (i->kind)
            {
            case I_OPER:
            {
                fprintf(stderr, "%s\n", i->u.OPER.assem);
            }
            break;
            case I_LABEL:
            {
                fprintf(stderr, "%s\n", i->u.LABEL.assem);
            }
            break;
            case I_MOVE:
            {
                fprintf(stderr, "%s\n", i->u.MOVE.assem);
            }
            break;
            default:
            break;
            }
        }
        bl = bl->tail;
    }
}


AS_blockList munchFunc(struct C_block c){
    AS_blockList bl = AS_BlockList(NULL,NULL);
    AS_blockList bl_hd = bl;
    for(C_stmListList now = c.stmLists; now; now = now->tail){
        
        il = AS_InstrList(NULL, NULL);
        AS_instrList ret = il;
        // 1.处理内部语句
       for(T_stmList s = now->head; s; s = s->tail){
            munchStm(s->head);
        }
        // 2.函数头部
        //   a.组装
        bl->tail = AS_BlockList(AS_Block(ret->tail), NULL);
        bl = bl->tail;
    }
    assert(!bl_hd->head);
    checkBlockList(bl_hd->tail);
    Temp_label pseudo_start = bl_hd->tail->head->label;
    string inst1 = checked_malloc(INST_MAX_SIZE);
    string inst2 = checked_malloc(INST_MAX_SIZE);
    sprintf(inst1, LABEL(start));
    sprintf(inst2, BR(%s), Temp_labelstring(pseudo_start));
    bl_hd->head = 
        AS_Block(
            AS_InstrList(
                AS_Label(inst1, Temp_namedlabel("start")),
                AS_InstrList(
                    AS_Oper(inst2, NULL, NULL, AS_Targets(Temp_LabelList(pseudo_start, NULL))),
                    NULL
                )
            )
        );
    // print_AS_blockList(bl_hd->tail);
    return bl_hd;
}

AS_instrList getBuildinFunc(){
    // statically embedding into .ll
    AS_instr _malloc = AS_Oper("declare i64 @malloc(i64)", NULL, NULL, NULL);
    AS_instr _getint = AS_Oper("declare i64 @getint()", NULL, NULL, NULL);
    AS_instr _getch = AS_Oper("declare i64 @getch()", NULL, NULL, NULL);
    AS_instr _getarray = AS_Oper("declare i64 @getarray(i64)", NULL, NULL, NULL);
    AS_instr _putint = AS_Oper("declare void @putint(i64)", NULL, NULL, NULL);
    AS_instr _putch = AS_Oper("declare void @putch(i64)", NULL, NULL, NULL);
    AS_instr _putarray = AS_Oper("declare void @putarray(i64,i64)", NULL, NULL, NULL);
    AS_instr _starttime = AS_Oper("declare void @_sysy_starttime()", NULL, NULL, NULL);
    AS_instr _stoptime = AS_Oper("declare void @_sysy_stoptime()", NULL, NULL, NULL);
    return 
    AS_InstrList(
        _malloc,
        AS_InstrList(
            _getint,
            AS_InstrList(
                _getch,
                AS_InstrList(
                    _getarray,
                    AS_InstrList(
                        _putint,
                        AS_InstrList(
                            _putch,
                            AS_InstrList(
                                _putarray,
                                AS_InstrList(
                                    _starttime,
                                    AS_InstrList(
                                        _stoptime,
                                        NULL
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    );
}