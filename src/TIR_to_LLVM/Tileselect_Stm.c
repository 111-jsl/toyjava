#include "Tileselect_Stm.h"
#include "Tileselect_Exp.h"
#include "TL_define.h"
#include <assert.h>
#include <string.h>

void emit(AS_instr i);

// tile gadgets
static bool isBOP(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_TEMP){
        switch (s->u.MOVE.src->kind)
        {
        case T_CONST:
        {
            string inst = checked_malloc(INST_MAX_SIZE);
            sprintf(inst, 
                BOP(%s, %d, %d), 
                mapBOP(T_plus),
                s->u.MOVE.src->u.CONST,
                0);
            emit(
                AS_Oper(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    NULL,
                    NULL
                )
            );
            return TRUE;
        }
        break;
        case T_TEMP:
        {
            string inst = checked_malloc(INST_MAX_SIZE);
            sprintf(inst, 
                BOP(%s, %%`s0, %d), 
                mapBOP(T_plus),
                0);
            emit(
                AS_Move(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    Temp_TempList(s->u.MOVE.src->u.TEMP, NULL)
                )
            );
            return TRUE;
        }
        break;
        case T_BINOP:
        {
            string inst = checked_malloc(INST_MAX_SIZE);

            int eb1_id = 1;
            Exp_bind eb0 = getExpType(s->u.MOVE.src->u.BINOP.left, 0);
            if(!eb0->tl) eb1_id = 0;
            Exp_bind eb1 = getExpType(s->u.MOVE.src->u.BINOP.right, eb1_id);
            Temp_tempList src = mergeTempList(eb0->tl, eb1->tl);
            sprintf(inst, BOP(%s, %s, %s),
                mapBOP(s->u.MOVE.src->u.BINOP.op),
                eb0->format, eb1->format);
            emit(
                AS_Oper(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    src,
                    NULL
                )
            );
            return TRUE;
        }
        break;
        default:
        {
            string inst = checked_malloc(INST_MAX_SIZE);
            Temp_temp t1 = munchExp(s->u.MOVE.src);
            sprintf(inst, 
                BOP(%s, %%`s0, %d), 
                mapBOP(T_plus),
                0);
            emit(
                AS_Oper(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    Temp_TempList(t1, NULL),
                    NULL
                )
            );
            return TRUE;

        }
        break;
        }
    }
    return FALSE;
}

static bool isSTORE(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_MEM){
        string inst1 = checked_malloc(INST_MAX_SIZE);
        string inst2 = checked_malloc(INST_MAX_SIZE);
        string inst3 = checked_malloc(INST_MAX_SIZE);
        Temp_temp r1 = Temp_newtemp();
        Temp_temp r2 = Temp_newtemp();

        
        
        switch (s->u.MOVE.dst->u.MEM->kind)
        {
        case T_CONST:
        {
            assert(0);
        }
        break;
        case T_TEMP:
        {
            Exp_bind eb0;
            eb0 = getExpType(s->u.MOVE.src, 0);
            
            sprintf(inst1, INTTOPTR(%%`s0));
            if(eb0->tl){
                sprintf(inst2, STORE(%s, %s, %%`s1), eb0->type, eb0->format);
            }else{
                sprintf(inst2, STORE(%s, %s, %%`s0), eb0->type, eb0->format);
            }
            emit(
                AS_Oper(
                    inst1,
                    Temp_TempList(r1, NULL),
                    Temp_TempList(s->u.MOVE.dst->u.MEM->u.TEMP, NULL),
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    NULL,
                    mergeTempList(eb0->tl, Temp_TempList(r1, NULL)),
                    NULL
                )
            );
            return TRUE;
        }       
        break;
        case T_BINOP:
        {
            int eb1_id = 1;
            Exp_bind eb0 = getExpType(s->u.MOVE.dst->u.MEM->u.BINOP.left, 0);
            if(!eb0->tl) eb1_id = 0;
            Exp_bind eb1 = getExpType(s->u.MOVE.dst->u.MEM->u.BINOP.right, eb1_id);
            Exp_bind eb2 = getExpType(s->u.MOVE.src, 0);
            sprintf(inst1, BOP(%s, %s, %s), mapBOP(T_plus), eb0->format, eb1->format);
            sprintf(inst2, INTTOPTR(%%`s0));
            if(eb2->tl){
                sprintf(inst3, STORE(%s, %s, %%`s1), eb2->type, eb2->format);
            }else{
                sprintf(inst3, STORE(%s, %s, %%`s0), eb2->type, eb2->format);
            }
            emit(
                AS_Oper(
                    inst1,
                    Temp_TempList(r1, NULL),
                    mergeTempList(eb0->tl, eb1->tl),
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    Temp_TempList(r2, NULL),
                    Temp_TempList(r1, NULL),
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst3,
                    NULL,
                    mergeTempList(eb2->tl, Temp_TempList(r2, NULL)),
                    NULL
                )
            );
            return TRUE;
        }
        break;
        default:
        {
            
            Exp_bind eb0 = getExpType(s->u.MOVE.dst->u.MEM, 0);
            Exp_bind eb1 = getExpType(s->u.MOVE.src, 0);
            
            sprintf(inst1, INTTOPTR(%s), eb0->format);
            if(eb1->tl){
                sprintf(inst2, STORE(%s, %s, %%`s1), eb1->type, eb1->format);
            }else{
                sprintf(inst2, STORE(%s, %s, %%`s0), eb1->type, eb1->format);
            }
            emit(
                AS_Oper(
                    inst1,
                    Temp_TempList(r1, NULL),
                    eb0->tl,
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    NULL,
                    mergeTempList(eb1->tl, Temp_TempList(r1, NULL)),
                    NULL
                )
            );
            return TRUE;
        }
        break;
        }
    }
    return FALSE;
}
static bool isCJUMP(T_stm s){
    if(s->kind == T_CJUMP){
        string inst1 = checked_malloc(INST_MAX_SIZE);
        string inst2 = checked_malloc(INST_MAX_SIZE);
        Temp_temp r1 = Temp_newtemp();
        int eb1_id = 1;
        Exp_bind eb0 = getExpType(s->u.CJUMP.left, 0);
        if(!eb0->tl) eb1_id = 0;
        Exp_bind eb1 = getExpType(s->u.CJUMP.right, eb1_id);

        sprintf(inst1, ICMP(%s, %s, %s),
            mapCND(s->u.CJUMP.op),
            eb0->format, eb1->format);
        sprintf(inst2, BR_COND(%%`s0, %s, %s),
            S_name(s->u.CJUMP.true),
            S_name(s->u.CJUMP.false));
        emit(
            AS_Oper(
                inst1,
                Temp_TempList(r1, NULL),
                mergeTempList(eb0->tl, eb1->tl),
                NULL
            )
        );
        emit(
            AS_Oper(
                inst2,
                NULL,
                Temp_TempList(r1, NULL),
                AS_Targets(Temp_LabelList(
                    s->u.CJUMP.true,
                    Temp_LabelList(
                        s->u.CJUMP.false,
                        NULL
                    )
                ))
            )
        );
        return TRUE;

    }
    return FALSE;
}
static bool isJUMP(T_stm s){
    if(s->kind == T_JUMP){
        string inst = checked_malloc(INST_MAX_SIZE);
        sprintf(inst, BR(%s), S_name(s->u.JUMP.jump));
        emit(
            AS_Oper(
                inst,
                NULL,
                NULL,
                AS_Targets(Temp_LabelList(
                    s->u.JUMP.jump, NULL
                ))
            )
        );
        return TRUE;
    }
    return FALSE;
}

static bool isCALL_STM(T_stm s){
    if(s->kind == T_EXP){
        T_exp e = s->u.EXP;
        switch (e->kind)
        {
        case T_CALL:
        {
            // assert(e->u.CALL.obj->kind == T_TEMP);
            Exp_bind eb0 = getExpType(e->u.CALL.obj, 0);
            assert(eb0->tl);

            Temp_temp r1 = Temp_newtemp();
            string inst1 = checked_malloc(INST_MAX_SIZE);
            string inst2 = checked_malloc(INST_MAX_SIZE);

            Temp_tempList tl = Temp_TempList(NULL, NULL);
            sprintf(inst1,
                INTTOPTR(%s),
                eb0->format
            );  
            sprintf(inst2, 
                CALL_STM(%%`s0, %s),
                T_expList_to_string(e->u.CALL.args, tl, 1)
            );
            if(!tl->head) tl = NULL;
            emit(
                AS_Oper(
                    inst1,
                    Temp_TempList(r1, NULL),
                    eb0->tl,
                    NULL
                )
            );
            emit(
                AS_Oper(
                    inst2,
                    NULL,
                    mergeTempList(Temp_TempList(r1, NULL), tl),
                    NULL
                )
            );
            return TRUE;
        }    
        break;
        case T_ExtCALL:
        {
            if(!strcmp(e->u.ExtCALL.extfun, "putint")
            || !strcmp(e->u.ExtCALL.extfun, "putch")
            || !strcmp(e->u.ExtCALL.extfun, "putarray")
            || !strcmp(e->u.ExtCALL.extfun, "starttime")
            || !strcmp(e->u.ExtCALL.extfun, "stoptime")){
                // ?
                if(!strcmp(e->u.ExtCALL.extfun, "starttime")){
                    e->u.ExtCALL.extfun = "_sysy_starttime";
                }else if(!strcmp(e->u.ExtCALL.extfun, "stoptime")){
                    e->u.ExtCALL.extfun = "_sysy_stoptime";
                }
                
                string inst = checked_malloc(INST_MAX_SIZE);
                Temp_tempList tl = Temp_TempList(NULL, NULL);
                sprintf(inst, 
                    CALL_STM(@%s, %s), 
                    e->u.ExtCALL.extfun,
                    T_expList_to_string(e->u.ExtCALL.args, tl, 0)
                );
                if(!tl->head) tl = NULL;
                emit(
                    AS_Oper(
                        inst,
                        NULL,
                        tl,
                        NULL
                    )
                );
            }
            return TRUE;
        }
        break;
        default:
        break;
        }
    }
    return FALSE;
}
static bool isRET(T_stm s){
    if(s->kind == T_RETURN){
        string inst = checked_malloc(INST_MAX_SIZE);
        Exp_bind eb0 = getExpType(s->u.EXP, 0);
        sprintf(inst,
            RET(%s),
            eb0->format
        );
        emit(
            AS_Oper(
                inst,
                NULL,
                eb0->tl,
                NULL
            )
        );
        return TRUE;
    }
    return FALSE;
}

static bool isLABEL(T_stm s){
    if(s->kind == T_LABEL){
        string inst = checked_malloc(INST_MAX_SIZE);
        sprintf(inst, LABEL(%s), S_name(s->u.LABEL));
        emit(
            AS_Label(inst, s->u.LABEL)
        );
        return TRUE;
    }
    return FALSE;
}


bool munchStm(T_stm s){
    // fprintf(stderr, "%d\n", s->kind);
    if(isSTORE(s)){
        return TRUE;
    }else if(isBOP(s)){
        return TRUE;
    }else if(isCJUMP(s)){
        return TRUE;
    }else if(isJUMP(s)){
        return TRUE;
    }else if(isCALL_STM(s)){
        return TRUE;
    }else if(isRET(s)){
        return TRUE;
    }else if(isLABEL(s)){
        return TRUE;
    }
    assert(0);
    return FALSE;
}