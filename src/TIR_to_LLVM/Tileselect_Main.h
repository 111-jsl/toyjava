#pragma once
#include "../Utils/assemblock.h"
#include "../Canon/API_canon.h"

extern void emit(AS_instr i);
AS_instrList munchFuncProlog(string fname, Temp_tempList args);
AS_instrList munchFuncEpilog(struct C_block c);
AS_blockList munchFunc(struct C_block c);
AS_instrList getBuildinFunc();
