#pragma once
#include "../Utils/treep.h"

#define INST_MAX_SIZE 50

#define TYPE "i64"
#define LABEL(l) (#l":")
#define BOP(op, s1, s2) ("%%`d0 = "#op" i64 "#s1", "#s2)
#define ALLOCA() ("%%`d0 = alloca "TYPE", align 4")
#define LOAD(s1) ("%%`d0 = load i64, ptr "#s1)
#define STORE(t1, s1, s2) ("store "#t1" "#s1", ptr "#s2)
#define ICMP(cnd, s1, s2) ("%%`d0 = icmp "#cnd" i64 "#s1", "#s2)
#define BR_COND(s1, l1, l2) ("br i1 "#s1", label %%"#l1", label %%"#l2)
#define BR(l1) ("br label %%"#l1)
#define CALL_EXP(s1, slist) ("%%`d0 = call i64 "#s1"("#slist")")
#define CALL_STM(s1, slist) ("call void "#s1"("#slist")")
#define GETELEMENTPTR(s1, s2) ("%%`d0 = getelementptr i64, ptr "#s1", i64 "#s2)
#define PTRTOINT(s1) ("%%`d0 = ptrtoint ptr "#s1" to i64")
#define INTTOPTR(s1) ("%%`d0 = inttoptr i64 "#s1" to ptr")
#define SEXT(s1, t1, t2) ("%%`d0 = sext "#t1" "#s1" to "#t2)
#define RET_VOID() ("ret void")
#define RET(s1) ("ret i64 "#s1)




const string mapBOP(T_binOp b);
const string mapCND(T_relOp r);

const string T_expList_to_string(T_expList el, Temp_tempList tl, int st);

typedef struct Exp_bind_{
    string format;
    string type;
    Temp_tempList tl;
} *Exp_bind;

Exp_bind Exp_Bind(string f, string t, Temp_tempList tl);

Exp_bind getExpType(T_exp e, int id);

Temp_tempList mergeTempList(Temp_tempList tl1, Temp_tempList tl2);

void checkTempListSafety(Temp_tempList tl);
