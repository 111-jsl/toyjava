#include "TL_define.h"
#include "Tileselect_Exp.h"
#include <assert.h>
#include <string.h>

static const string BOPMap[4] = {
    "add", "sub", "mul", "sdiv"
};

static const string CNDMap[6] = {
    "eq", "ne", "slt", "sgt", "sle", "sge"
};


const string mapBOP(T_binOp b){
    assert(b >= 0 && b < 4);
    return BOPMap[b];
}
const string mapCND(T_relOp r){
    assert(r >= 0 && r < 6);
    return CNDMap[r];
}


static const int SingleParam_Length = 20;
const string T_expList_to_string(T_expList el, Temp_tempList tl, int st){
    assert(tl);
    Temp_tempList last = tl;
    Temp_tempList hd = tl;
    int params_cnt = 0;
    for(T_expList now = el; now; now = now->tail) params_cnt++;
    string ret = NULL;
    if(params_cnt > 0){
        ret = checked_malloc(params_cnt * SingleParam_Length);
        for(T_expList now = el; now; now = now->tail){
            string s = checked_malloc(SingleParam_Length);
            switch (now->head->kind)
            {
            case T_CONST:
            {
                sprintf(s, "i64 %d,", now->head->u.CONST);
            }
            break;
            case T_TEMP:
            {
                sprintf(s, "i64 %%`s%d,", st++);
                tl->head = now->head->u.TEMP;
                tl->tail = Temp_TempList(NULL, NULL);
                last = tl;
                tl = tl->tail;
            }
            break;
            default:
            {
                sprintf(s, "i64 %%`s%d,", st++);
                Temp_temp t = munchExp(now->head);
                tl->head = t;
                tl->tail = Temp_TempList(NULL, NULL);
                last = tl;
                tl = tl->tail;
            }
            break;
            }
            strcat(ret, s);
        }
        if(last->head != NULL){
            assert(last->tail->head == NULL);
            last->tail = NULL;
        }
        // fprintf(stderr, "%s\n", ret);
        ret[strlen(ret) - 1] = '\0';
    }else{
        ret = "";
    }
    return ret;
}

Exp_bind Exp_Bind(string f, string t, Temp_tempList tl){
    Exp_bind eb = checked_malloc(sizeof(*eb));
    eb->format = f;
    eb->type = t;
    eb->tl = tl;
    return eb;
}

Exp_bind getExpType(T_exp e, int id){
    Temp_tempList tl;
    string format = checked_malloc(8);
    string type = checked_malloc(8);

    switch (e->kind)
    {
    case T_CONST:
    {
        sprintf(format, "%d", e->u.CONST);
        sprintf(type, "i64");
        tl = NULL;
    }
    break;
    case T_TEMP:
    {
        sprintf(format, "%%`s%d", id);
        sprintf(type, "i64");
        tl = Temp_TempList(e->u.TEMP, NULL);
    }
    break;
    case T_NAME:
    {
        sprintf(format, "@%s", S_name(e->u.NAME));
        sprintf(type, "ptr");
        tl = NULL;
    }
    break;
    default:
    {
        Temp_temp t = munchExp(e);
        sprintf(format, "%%`s%d", id);
        sprintf(type, "i64");
        tl = Temp_TempList(t, NULL);
    }
    break;
    }
    return Exp_Bind(format, type, tl);
}

Temp_tempList mergeTempList(Temp_tempList tl1, Temp_tempList tl2){
    if(!tl1){
        return tl2;
    }
    if(!tl2){
        return tl1;
    }
    Temp_tempList last = NULL;
    for(Temp_tempList now = tl1; now; now = now->tail){
        last = now;
    }
    last->tail = tl2;
    return tl1;
}

void checkTempListSafety(Temp_tempList tl){
    for(Temp_tempList now = tl; now; now = now->tail){
        assert(now);
    }
}