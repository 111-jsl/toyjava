#pragma once
#include <stdio.h>
#include "../Utils/assemblock.h"
#include "bg.h"
#include "graph.h"

G_nodeList getIG(FILE* f, AS_instrList il);

void printCFG(FILE* f, AS_instrList il);

void printBG(FILE* f, AS_blockList bl);
