#include <stdio.h>
#include <assert.h>
#include "../Utils/util.h"
#include "../Utils/symbol.h"
#include "../Utils/temp.h"
#include "../Utils/assem.h"
#include "graph.h"
#include "flowgraph.h"
#include "liveness.h"
#include "ig.h"
#include "bg.h"

#include "API_cfg.h"

static FILE* OF;

static void show(AS_instr ins) {
    assert(ins);
    FG_Showinfo(OF, ins, Temp_name());
}

G_nodeList Liveness(G_nodeList);

G_nodeList getIG(FILE* f, AS_instrList il){
    OF = f;
    G_graph G=FG_AssemFlowGraph(il);
    G_show(OF, G_nodes(G), (void*)show);
    G_nodeList lg=Liveness(G_nodes(G));
    Show_Liveness(OF, lg);
    G_nodeList ig=Create_ig(lg);
    Show_ig(OF, ig);
    return ig;
}



void printCFG(FILE* f, AS_instrList il){
    OF = f;
    AS_printInstrList(OF, il, Temp_name());
    G_graph G=FG_AssemFlowGraph(il);
    fprintf(stderr, "FG_AssemFlowGraph pass\n");
    G_show(OF, G_nodes(G), (void*)show);
    fprintf(stderr, "G_show pass\n");
    G_nodeList lg=Liveness(G_nodes(G));
    Show_Liveness(OF, lg);
    fprintf(OF, "------Interference Graph---------\n");
    G_nodeList ig=Create_ig(lg);
    Show_ig(OF, ig);
}

void printBG(FILE* f, AS_blockList bl){
    G_nodeList bg=Create_bg(bl);

    printf("------Basic Block Graph---------\n");

    Show_bg(f, bg);
}
