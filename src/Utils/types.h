/*
 * types.h -
 *
 * All types and functions declared in this header file begin with "Ty_"
 * Linked list types end with "..list"
 */
#pragma once
#include "symbol.h"

typedef struct Ty_ty_ *Ty_ty;
typedef struct Ty_tyList_ *Ty_tyList;
typedef struct Ty_field_ *Ty_field;
typedef struct Ty_fieldList_ *Ty_fieldList;
typedef struct S_bind_ *S_bind;

struct S_bind_ {
	// TC
    Ty_ty location;
    Ty_ty value;
    A_pos pos;

	// AT
	void *ast_node;
	int offset;
};
S_bind S_Bind(Ty_ty location, Ty_ty value, A_pos pos, void *ast_node);


struct Ty_ty_
{
	enum
	{
		Ty_record,
		Ty_nil, // use
		Ty_int, // use
		Ty_string,
		Ty_array, // use
		Ty_name, 
		Ty_void,
		Ty_class, // use
		Ty_function // use
	} kind;
	union
	{
		Ty_fieldList record;
		Ty_ty array;
		struct
		{
			S_symbol sym;
			Ty_ty ty;
		} name;
		struct
		{
			S_symbol sym;
			S_table table;
		} module;
		struct 
		{
			S_symbol sym;
			S_symbol class;
			S_table table;
		} function;
	} u;
};

struct Ty_tyList_
{
	Ty_ty head;
	Ty_tyList tail;
};
struct Ty_field_
{
	S_symbol name;
	Ty_ty ty;
};
struct Ty_fieldList_
{
	Ty_field head;
	Ty_fieldList tail;
};

Ty_ty Ty_Nil(void);
Ty_ty Ty_Int(void);
Ty_ty Ty_String(void);
Ty_ty Ty_Void(void);

Ty_ty Ty_Record(Ty_fieldList fields);
Ty_ty Ty_Array(Ty_ty ty);
Ty_ty Ty_Name(S_symbol sym, Ty_ty ty);
Ty_ty Ty_Class(S_symbol sym, S_table table);
Ty_ty Ty_Function(S_symbol sym, S_table table, S_symbol class);

Ty_tyList Ty_TyList(Ty_ty head, Ty_tyList tail);
Ty_field Ty_Field(S_symbol name, Ty_ty ty);
Ty_fieldList Ty_FieldList(Ty_field head, Ty_fieldList tail);

void Ty_print(Ty_ty t);
void TyList_print(Ty_tyList list);
