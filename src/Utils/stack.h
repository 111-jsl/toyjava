#pragma once


typedef struct stack_node_ *stack_node;
typedef struct stack_ *stack;
struct stack_node_{
    void *val;
    stack_node next;
    stack_node prev;
};
struct stack_{
    stack_node top;
};

stack stack_create();
void stack_push(stack s, void *val);
void *stack_pop(stack s);
void *stack_top(stack s);
