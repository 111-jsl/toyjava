#include "stack.h"
#include "util.h"
#include <stdio.h>
#include <assert.h>


stack stack_create()
{
    stack s = checked_malloc(sizeof(*s));
    s->top = NULL;
    return s;
}

void stack_push(stack s, void *val)
{
    stack_node new_n = checked_malloc(sizeof(*new_n));
    new_n->val = val;
    new_n->next = NULL;
    new_n->prev = s->top;
    if(s->top)
        s->top->next = new_n;
    s->top = new_n;
}
void *stack_pop(stack s)
{
    assert(s->top);
    void *ret = s->top->val;
    stack_node now = s->top;
    s->top = s->top->prev;
    checked_free(now);
    if(s->top)
        s->top->next = NULL;
    return ret;
}

void *stack_top(stack s)
{
    return s->top->val;
}
