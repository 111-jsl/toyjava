%{
#include <stdio.h>
#include <assert.h>
#include "fdmjast.h"
#include "printast.h"


extern int yylex();
extern void yyerror(char*);
extern int  yywrap();
extern A_prog program;
// extern int yydebug=1;
%}

/* %parse-param {A_stm ret}  */
// yytext类型定义
%union {
  A_binop bop;
  A_type type;
  A_prog prog;
  A_mainMethod main;
  A_classDecl cd;
  A_classDeclList cdl;
  A_methodDecl md;
  A_methodDeclList mdl;
  A_formal f;
  A_formalList fl;
  A_varDecl vd;
  A_varDeclList vdl;
  A_exp exp; 
  A_expList elist;
  A_stm stm; 
  A_stmList sl;
  struct {
    int v;
    A_pos pos;
  }integer;
  struct {
    char* v;
    A_pos pos;
  }str;
}

// 所有无具体值的关键字
// 类关键字
%token <str> PUBLIC CLASS EXTENDS THIS NEW
// 值类型关键字
%token <str> INT
// 内置值关键字，避免重定义
%token <str> BOOL_TRUE BOOL_FALSE
// 分支关键字
%token <str> IF ELSE WHILE CONTINUE BREAK
// 函数关键字
%token <str> MAIN RETURN
// 输出关键字
%token <str> PUTINT PUTCH PUTARRAY
// 输入关键字
%token <str> GETINT GETCH GETARRAY
// 获取数组长度关键字
%token <str> LENGTH
// 时间关键字
%token <str> STARTTIME STOPTIME
// 算术符号关键字
%token <str> PLUS MINUS TIMES DIV OR AND LESS LE GREATER GE EQ NE

// 所有有具体值的关键字
%token <integer> INT_CONST
%token <str> IDENTIFIER

// 设置优先级
%left OR
%left AND
%left LESS LE GREATER GE EQ NE
%left PLUS MINUS
%left TIMES DIV
%left UMINUS '!'/*supplies precedence for unary minus */
%left '['/*考虑 "-exp[exp]"*/
%left '.' /* 考虑 "classvar.id[exp]" */

// 这里对所有*采用list的方法
// 所有的状态
%type <prog> Program
%type <main> Main

%type <cd> Classdecl
%type <cdl> Classdecllist

%type <vd> Vardecl
%type <vdl> Vardecllist

// 用explist代替，然后再检查
/* %type <exp> Intconst
%type <elist> Intconstlist
%type <elist> Intconstrestlist
%type <exp> Intconstrest */

%type <md> Methoddecl
%type <mdl> Methoddecllist

%type <fl> Formallist
%type <fl> Formalrestlist
%type <f> Formalrest

%type <type> Type

%type <stm> Statement
%type <sl> Statementlist

%type <exp> Exp
%type <elist> Explist
%type <elist> Exprestlist
%type <exp> Exprest


%start Program

%%                   /* beginning of rules section */
Program:    
  Main Classdecllist
  { 
    $$ = A_Prog($1->pos, $1, $2);
    program = $$;
  }
  |
  error error
  {
    yyerrok;
  }
  ;
Main:
  PUBLIC INT MAIN '(' ')' '{' Vardecllist Statementlist '}'
  {
    $$ = A_MainMethod($1.pos, $7, $8);
  }
  ;
Classdecllist:
  {
    $$ = NULL;
  }
  |
  Classdecl Classdecllist
  {
    $$ = A_ClassDeclList($1, $2);
  }
  ;
Classdecl:
  PUBLIC CLASS IDENTIFIER EXTENDS IDENTIFIER '{' Vardecllist Methoddecllist '}'
  {
    $$ = A_ClassDecl($1.pos, $3.v, $5.v, $7, $8);
  }
  |
  PUBLIC CLASS IDENTIFIER '{' Vardecllist Methoddecllist '}'
  {
    $$ = A_ClassDecl($1.pos, $3.v, NULL, $5, $6);
  }
  ;
Vardecllist:
  {
    $$ = NULL;
  }
  |
  Vardecl Vardecllist
  {
    $$ = A_VarDeclList($1, $2);
  }
  ;
Vardecl:
  Type IDENTIFIER ';'
  {
    $$ = A_VarDecl($1->pos, $1, $2.v, NULL);
  }
  |
  Type IDENTIFIER '=' Exp ';'
  {
    assert($1->t == A_intType);
    $$ = A_VarDecl($1->pos, $1, $2.v, A_ExpList($4, NULL));
  }
  |
  Type IDENTIFIER '=' '{' Explist '}' ';'
  {
    assert($1->t == A_intArrType);
    // TODO: check explist
    $$ = A_VarDecl($1->pos, $1, $2.v, $5);
  }
  ;
Methoddecllist:
  {
    $$ = NULL;
  }
  |
  Methoddecl Methoddecllist
  {
    $$ = A_MethodDeclList($1, $2);
  }
  ;
Methoddecl:
  PUBLIC Type IDENTIFIER '(' Formallist ')' '{' Vardecllist Statementlist '}'
  {
    $$ = A_MethodDecl($1.pos, $2, $3.v, $5, $8, $9);
  }
  ;
Formallist:
  {
    $$ = NULL;
  }
  |
  Type IDENTIFIER Formalrestlist
  {
    $$ = A_FormalList(A_Formal($1->pos, $1, $2.v), $3);
  }
  ;
Formalrestlist:
  {
    $$ = NULL;
  }
  |
  Formalrest Formalrestlist
  {
    $$ = A_FormalList($1, $2);
  }
  ;
Formalrest:
  ',' Type IDENTIFIER
  {
    $$ = A_Formal($2->pos, $2, $3.v);
  }
  ;
Type:
  CLASS IDENTIFIER
  {
    $$ = A_Type($1.pos, A_idType, $2.v);
  }
  |
  INT
  {
    $$ = A_Type($1.pos, A_intType, NULL);
  }
  |
  INT '[' ']'
  {
    $$ = A_Type($1.pos, A_intArrType, NULL);
  }
  ;
Statementlist:
  {
    $$ = NULL;
  }
  |
  Statement Statementlist
  {
    $$ = A_StmList($1, $2);
  }
  ;
Statement:    
  '{' Statementlist '}'
  {
    A_pos pos = NULL;
    if($2){
      pos = $2->head->pos;
    }
    $$ = A_NestedStm(pos, $2);
  } 
  |
  IF '(' Exp ')' Statement ELSE Statement
  {
    $$ = A_IfStm($1.pos, $3, $5, $7);
  }
  |
  IF '(' Exp ')' Statement
  {
    $$ = A_IfStm($1.pos, $3, $5, NULL);
  }
  |
  WHILE '(' Exp ')' Statement
  {
    $$ = A_WhileStm($1.pos, $3, $5);
  }
  |
  WHILE '(' Exp ')' ';'
  {
    $$ = A_WhileStm($1.pos, $3, NULL);
  }
  |
  Exp '=' Exp ';'
  {
    $$ = A_AssignStm($1->pos, $1, $3);
  }
  |
  Exp '[' ']' '=' '{' Explist '}' ';'
  {
    $$ = A_ArrayInit($1->pos, $1, $6);
  }
  |
  Exp '.' IDENTIFIER '(' Explist ')' ';'
  {
    $$ = A_CallStm($1->pos, $1, $3.v, $5);
  }
  |
  CONTINUE ';'
  {
    $$ = A_Continue($1.pos);
  }
  |
  BREAK ';'
  {
    $$ = A_Break($1.pos);
  }
  |
  RETURN Exp ';'
  {
    $$ = A_Return($1.pos, $2);
  }
  |
  PUTINT '(' Exp ')' ';'
  {
    $$ = A_Putint($1.pos, $3);
  }
  |
  PUTCH '(' Exp ')' ';'
  {
    $$ = A_Putch($1.pos, $3);
  }
  |
  PUTARRAY '(' Exp ',' Exp ')' ';'
  {
    $$ = A_Putarray($1.pos, $3, $5);
  }
  |
  STARTTIME '(' ')' ';'
  {
    $$ = A_Starttime($1.pos);
  }
  |
  STOPTIME '(' ')' ';'
  {
    $$ = A_Stoptime($1.pos);
  }
  ;
Explist:
  {
    $$ = NULL;
  }
  |
  Exp Exprestlist
  {
    $$ = A_ExpList($1, $2);
  }
  ;
Exprestlist:
  {
    $$ = NULL;
  }
  |
  Exprest Exprestlist
  {
    $$ = A_ExpList($1, $2);
  }
  ;
Exprest:
  ',' Exp
  {
    $$ = $2;
  }
  ;
Exp:
  Exp PLUS Exp
  {
    $$ = A_OpExp($1->pos, $1, A_plus, $3);
  }
  |
  Exp MINUS Exp
  {
    $$ = A_OpExp($1->pos, $1, A_minus, $3);
  }
  |
  Exp TIMES Exp
  {
    $$ = A_OpExp($1->pos, $1, A_times, $3);
  }
  |
  Exp DIV Exp
  {
    $$ = A_OpExp($1->pos, $1, A_div, $3);
  }
  |
  Exp OR Exp
  {
    $$ = A_OpExp($1->pos, $1, A_or, $3);
  }
  |
  Exp AND Exp
  {
    $$ = A_OpExp($1->pos, $1, A_and, $3);
  }
  |
  Exp LESS Exp
  {
    $$ = A_OpExp($1->pos, $1, A_less, $3);
  }
  |
  Exp LE Exp
  {
    $$ = A_OpExp($1->pos, $1, A_le, $3);
  }
  |
  Exp GREATER Exp
  {
    $$ = A_OpExp($1->pos, $1, A_greater, $3);
  }
  |
  Exp GE Exp
  {
    $$ = A_OpExp($1->pos, $1, A_ge, $3);
  }
  |
  Exp EQ Exp
  {
    $$ = A_OpExp($1->pos, $1, A_eq, $3);
  }
  |
  Exp NE Exp
  {
    $$ = A_OpExp($1->pos, $1, A_ne, $3);
  }
  |
  Exp '[' Exp ']'
  {
    $$ = A_ArrayExp($1->pos, $1, $3);
  }
  |
  Exp '.' IDENTIFIER '(' Explist ')'
  {
    $$ = A_CallExp($1->pos, $1, $3.v, $5);
  }
  |
  Exp '.' IDENTIFIER
  {
    $$ = A_ClassVarExp($1->pos, $1, $3.v);
  }
  |
  INT_CONST
  {
    $$ = A_NumConst($1.pos, $1.v);
  }
  |
  BOOL_TRUE
  {
    $$ = A_BoolConst($1.pos, TRUE);
  }
  |
  BOOL_FALSE
  {
    $$ = A_BoolConst($1.pos, FALSE);
  }
  |
  IDENTIFIER
  {
    //printf("id:\t%s\n", $1);
    $$ = A_IdExp($1.pos, $1.v);
  }
  |
  THIS
  {
    $$ = A_ThisExp($1.pos);
  }
  |
  NEW INT '[' Exp ']'
  {
    $$ = A_NewIntArrExp($1.pos, $4);
  }
  |
  NEW IDENTIFIER '(' ')'
  {
    $$ = A_NewObjExp($1.pos, $2.v);
  }
  |
  '!' Exp
  {
    $$ = A_NotExp($2->pos, $2);
  }
  |
  MINUS Exp %prec UMINUS
  {
    $$ = A_MinusExp($1.pos, $2);
  }
  |
  '(' '{' Statementlist '}' Exp ')'
  {
    A_pos pos = NULL;
    if($3){
      pos = $3->head->pos;
    }else{
      pos = $5->pos;
    }
    $$ = A_EscExp(pos, $3, $5);
  }
  |
  '(' Exp ')'
  {
    $$ = $2;
  }
  |
  GETINT '(' ')'
  {
    $$ = A_Getint($1.pos);
  }
  |
  GETCH '(' ')'
  {
    $$ = A_Getch($1.pos);
  }
  |
  GETARRAY '(' Exp ')'
  {
    $$ = A_Getarray($1.pos, $3);
  }
  |
  LENGTH '(' Exp ')'
  {
    $$ = A_LengthExp($1.pos, $3);
  }
  ;


%%

void print_pos(A_pos p)
{
  fprintf(stderr, "line:\t%d\tpos:\t%d\t\n", p->line, p->pos);
}

void yyerror(s)
char *s;
{
  fprintf(stderr, "%s\n",s);
}

int yywrap()
{

  return(1);
}
