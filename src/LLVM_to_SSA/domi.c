#include <stdio.h>
#include <assert.h>
#include "domi.h"


void print_Dominators(FILE* out, domi_table dt){
    domi_print(out, dt, "Dominators");
}

domi_table find_Dominators(G_nodeList nl){
    G_nodeList hd = nl;
    // D[x] means the set of nodes that dominate x
    domi_table passive_D = domi_Table(G_nodecount(nl->head));
    // ith node domi itself
    for(int i = 0; i < passive_D ->n; i++){
        domi_set(passive_D , i, i);
    }
    // iterate
    bool is_hold = FALSE;
    while(!is_hold){
        G_nodeList iter_list = hd;
        bool table_hold = TRUE;
        while(iter_list){
            G_nodeList pred = G_pred(iter_list->head);
            bool have_intersect = FALSE;
            domi_intersect_begin(passive_D);
            while(pred){
                have_intersect = TRUE;
                int pred_id = G_mykey(pred->head);
                domi_intersect(passive_D->table[pred_id]);
                pred = pred->tail;
            }
            if(have_intersect)
                table_hold &= !domi_range_set(passive_D, G_mykey(iter_list->head), domi_intersect_end());
            iter_list = iter_list->tail;
        }
        is_hold = table_hold;
    }
    // passive D look like
    // print_Dominators(stderr, passive_D);
    return passive_D;
}

domi_table find_Dominated(G_nodeList nl){
    domi_table passive_D = find_Dominators(nl);
    // convert to active D
    domi_table active_D = domi_Table(passive_D->n);
    for(int i = 0; i < passive_D->n; i++){
        for(int j = 0; j < passive_D->n; j++){
            if(domi_lookup(passive_D, i, j)){
                domi_set(active_D, j, i);
            }
        }
    }
    // active D look like
    // print_Dominators(stderr, active_D);
    // free passive D
    domi_freeTable(passive_D);
    return active_D;
}




domi_table find_Idom(domi_table passive_D){
    assert(0);
    // int n = passive_D->n;
    // int *idom = checked_malloc(sizeof(int) * n);
    // for(int i = 0; i < n; i++){
    //     idom[i] = -1;
    // }
    
    
}