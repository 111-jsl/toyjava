#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include "LS_define.h"
#include "bitmap.h"
#include "../Utils/util.h"
#include "../TIR_to_LLVM/TL_define.h"



domi_table domi_Table(int n){
    domi_table dt = checked_malloc(sizeof(*dt));
    dt->n = n;
    dt->table = checked_malloc(sizeof(BitmapCell*) * n);
    for(int i = 0; i < n; i++){
        dt->table[i] = checked_malloc(sizeof(BitmapCell) * BITMAP_TO_NUM_CELLS(n));
        init_bitmap(dt->table[i], BITMAP_TO_NUM_CELLS(n));
    }
    return dt;
}
void domi_freeTable(domi_table dt){
    for(int i = 0; i < dt->n; i++){
        checked_free(dt->table[i]);
    }
    checked_free(dt->table);
    checked_free(dt);
}

void domi_print(FILE* out, domi_table dt, string head){
    fprintf(out, "--------------%s--------------\n", head);
    for(int i = 0; i < dt->n; i++){
        fprintf(out, "%d:\t", i);
        for(int j = 0; j < dt->n; j++){
            if(domi_lookup(dt, i, j)){
                fprintf(out, "%d ", j);
            }
        }
        fprintf(out, "\n");
    }
    fprintf(out, "--------------------------------------\n");
}


bool domi_lookup(domi_table dt, int x, int y){
    return bitmap_get(dt->table[x], y);
}
bool domi_set(domi_table dt, int x, int y){
    if(bitmap_get(dt->table[x], y)) return FALSE;
    bitmap_set(dt->table[x], y);
    return TRUE;
}
bool domi_clear(domi_table dt, int x, int y){
    if(!bitmap_get(dt->table[x], y)) return FALSE;
    bitmap_clear(dt->table[x], y);
    return TRUE;
}
bool domi_range_set(domi_table dt, int x, BitmapCell *y){
    bool ret = FALSE;
    for(int i = 0; i < BITMAP_TO_NUM_CELLS(dt->n); i++){
        BitmapCell tmp = dt->table[x][i];
        dt->table[x][i] |= y[i];
        if(tmp != dt->table[x][i]) ret = TRUE;
    }
    return ret;
}
static BitmapCell *intersect_bitmap = NULL;
static int intersect_n = 0;
void domi_intersect_begin(domi_table dt){
    if(intersect_bitmap){
        checked_free(intersect_bitmap);
    }
    intersect_n = dt->n;
    intersect_bitmap = checked_malloc(sizeof(BitmapCell) * BITMAP_TO_NUM_CELLS(dt->n));
    memset(intersect_bitmap, UCHAR_MAX, BITMAP_TO_NUM_CELLS(dt->n));
}
void domi_intersect(BitmapCell *bmp){
    for(int i = 0; i < BITMAP_TO_NUM_CELLS(intersect_n); i++){
        intersect_bitmap[i] &= bmp[i];
    }
}
BitmapCell* domi_intersect_end(){
    assert(intersect_bitmap);
    return intersect_bitmap;
}


PHI_table PHI_empty(){
    return TAB_empty();
}
void PHI_enter(PHI_table t, int id, void *value){
    TAB_enter(t, id, value);
}
void *PHI_look(PHI_table t, int id){
    return TAB_look(t, id);
}

VAR_table VAR_empty(){
    return TAB_empty();
}
void VAR_enter(VAR_table t, Temp_temp tmp, void *value){
    TAB_enter(t, tmp, value);
}
void *VAR_look(VAR_table t, Temp_temp tmp){
    return TAB_look(t, tmp);
}
void *VAR_pop(VAR_table t){
    return TAB_pop(t);
}


Temp_tempList deepcopyTempList(Temp_tempList tl){
    if(!tl) return NULL;
    Temp_tempList ret = Temp_TempList(NULL, NULL);
    Temp_tempList rtail = ret;
    for(Temp_tempList now = tl; now; now = now->tail){
        if(now->head){
            rtail->tail = Temp_TempList(now->head, NULL);
            rtail = rtail->tail;
        }
    }
    return ret->tail;
}

void concatTempList(Temp_tempList dst, Temp_tempList src){
    if(!src) return;
    Temp_tempList src_copy = deepcopyTempList(src);
    mergeTempList(dst, src_copy);
}

void freeTempList(Temp_tempList tl){
    if(!tl) return;
    Temp_tempList last = tl;
    while(tl){
        tl = tl->tail;
        checked_free(last);
        last = tl;
    }
}

bool is_Inttoptr(AS_instr i){
    if(i->kind != I_OPER) return FALSE;
    string instr = i->u.OPER.assem;
    if(strncmp(instr, INTTOPTR_TEMPLATE, INTTOPTR_TEMPLATE_LEN)){
        return FALSE;
    }
    return TRUE;
}

bool is_Phi(AS_instr i){
    if(i->kind != I_OPER) return FALSE;
    string instr = i->u.OPER.assem;
    if(strncmp(instr, PHI_TEMPLATE, PHI_TEMPLATE_LEN)){
        return FALSE;
    }
    return TRUE;
}

bool is_Icmp(AS_instr i){
    if(i->kind != I_OPER) return FALSE;
    string instr = i->u.OPER.assem;
    if(strncmp(instr, ICMP_TEMPLATE, ICMP_TEMPLATE_LEN)){
        return FALSE;
    }
    return TRUE;
}
