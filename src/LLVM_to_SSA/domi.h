#pragma once
#include "../cfg/graph.h"
#include "LS_define.h"


void print_Dominators(FILE* out, domi_table dt);
domi_table find_Dominators(G_nodeList nl);
domi_table find_Dominated(G_nodeList nl);
domi_table find_Idom(domi_table passive_D);
