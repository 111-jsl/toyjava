#include <stdio.h>
#include <assert.h>
#include "var_rename.h"
#include "LS_define.h"
#include "domi_frontier.h"
#include "../Utils/assemblock.h"
#include "../TIR_to_LLVM/TL_define.h"

static VAR_table var_table = NULL;
static VAR_table var_Table(){
    if(!var_table) var_table = VAR_empty();
    return var_table;
}
static void var_clear(){
    if(var_table)
        checked_free(var_table);
    var_table = NULL;
}
static void var_scope_begin(){
    VAR_enter(var_Table(), 1, NULL);
}
static void var_scope_end(){
    void *key = NULL;
    do{
        key = VAR_pop(var_Table());
    }while(key != 1);
}

static VAR_table used_table = NULL;
static VAR_table used_Table(){
    if(!used_table) used_table = VAR_empty();
    return used_table;
}
static void used_clear(){
    if(used_table)
        checked_free(used_table);
    used_table = NULL;
}


static void rename_instr_src(AS_instr i){
    Temp_tempList tl = NULL;
    switch (i->kind)
    {
    case I_OPER:
    {
        tl = i->u.OPER.src;
    }   
    break;
    case I_LABEL:
    {
        //do nothing
    }
    break;
    case I_MOVE:
    {
        tl = i->u.MOVE.src;
    }
    break;
    default:
    assert(0);
    break;
    }
    while(tl){
        // fprintf(stderr, "%s\n", Temp_look(Temp_name(), tl->head));
        Temp_temp top = VAR_look(var_Table(), tl->head);
        // assert(top);
        if(top) 
            tl->head = top;
        tl = tl->tail;
    }
}

static void update_instr_dst(AS_instr i){
    Temp_tempList tl = NULL;
    switch (i->kind)
    {
    case I_OPER:
    {
        tl = i->u.OPER.dst;
    }   
    break;
    case I_LABEL:
    {
        //do nothing
    }
    break;
    case I_MOVE:
    {

        tl = i->u.MOVE.dst;
    }
    break;
    default:
    assert(0);
    break;
    }
    while(tl){
        Temp_temp top = VAR_look(var_Table(), tl->head);
        if(top || VAR_look(used_Table(), tl->head)){
            // fprintf(stderr, "found\n");
            Temp_temp new_tmp = Temp_newtemp();
            VAR_enter(var_Table(), tl->head, new_tmp);
            VAR_enter(used_Table(), new_tmp, new_tmp);
            tl->head = new_tmp;
        }else{
            VAR_enter(var_Table(), tl->head, tl->head);
            VAR_enter(used_Table(), tl->head, tl->head);
        }
        tl = tl->tail;
    }
}

static void update_phi_dst(AS_instr i){
    assert(i->kind == I_OPER);
    Temp_temp ori_tmp = i->u.OPER.dst->head;
    Temp_temp new_tmp = Temp_newtemp();
    VAR_enter(var_Table(), ori_tmp, new_tmp);
    VAR_enter(used_Table(), new_tmp, new_tmp);
    i->u.OPER.dst->head = new_tmp;
}


static void rename_phi_src(AS_instr i, Temp_label from){
    // fprintf(stderr, "in phi rename, from: %s\n", Temp_labelstring(from));
    // fprintf(stderr, "\t%s\n", i->u.OPER.assem);
    assert(i->kind == I_OPER);
    Temp_labelList labels = i->u.OPER.jumps->labels;
    int label_id = 0;
    while(labels){
        // fprintf(stderr, "label iter: %s\n", Temp_labelstring(labels->head));
        if(labels->head == from) break;
        label_id++;
        labels = labels->tail;
    }
    Temp_tempList srcs = i->u.OPER.src;
    while(label_id){
        srcs = srcs->tail;
        label_id--;
    }
    assert(srcs->head);

    // fprintf(stderr, "phi temp: %s\n", Temp_look(Temp_name(), srcs->head));
    Temp_temp top = VAR_look(var_Table(), srcs->head);
    // assert(top);
    srcs->head = top;

}

static int *block_renamed = NULL;
static void var_rename_block(G_node n){
    // fprintf(stderr, "now rename block id : %d\n", G_mykey(n));
    
    // print_gnode(n);



    assert(block_renamed);
    if(block_renamed[G_mykey(n)]) return;
    block_renamed[G_mykey(n)] = 1;
    //定义scope
    var_scope_begin();

    AS_block b = G_nodeInfo(n);
    for(AS_instrList il = b->instrs; il; il = il->tail){
        AS_instr i = il->head;
        if(!is_Phi(i)){
            rename_instr_src(i);
            update_instr_dst(i);
        }else{
            update_phi_dst(i);
        }
        
    }
    G_nodeList succ = G_succ(n);
    G_nodeList son = succ;
    while(succ){
        AS_block succ_b = G_nodeInfo(succ->head);
        for(AS_instrList il = succ_b->instrs; il; il = il->tail){
            AS_instr i = il->head;

            if(is_Phi(i)){
                rename_phi_src(i, b->label);
            }
        }
        
        succ = succ->tail;
    }

    while(son){
        var_rename_block(son->head);
        son = son->tail;
    }

    //清理scope
    var_scope_end();

}



string str_get_next(string assem, char token){
    string next_var = NULL;
    for(int i = 0; i < strlen(assem); i++){
        if(assem[i] == token){
            next_var = assem + i + 1;
            break;
        }
    }
    return next_var;
}

static void handle_phi_null(AS_instr i){
    assert(i->kind == I_OPER);
    // fprintf(stderr, "is phi ? : %s\n", i->u.OPER.assem);

    string now = str_get_next(i->u.OPER.assem, '[');
    Temp_tempList tl = i->u.OPER.src;
    while(now){
        // fprintf(stderr, "now string : %s\n", now);
        if(!tl->head){
            *now = '0';
            for(int i = 1; i < strlen(now); i++){
                if(now[i] == ',') break;
                now[i] = ' ';
            }
        }
        tl = tl->tail;
        now = str_get_next(now, '[');
    }
}


void var_rename(G_nodeList nl, Temp_tempList args){


    //把函数参数放到表里，表示已经定义过
    while(args){
        VAR_enter(var_Table(), args->head, args->head);
        VAR_enter(used_Table(), args->head, args->head);
        args = args->tail;
    }
    
    // 记录已经rename过的block
    block_renamed = checked_malloc(sizeof(int) * G_nodecount(nl->head));

    // 认为0是开始的block
    var_rename_block(nl->head);

    // 清理掉block_renamed表
    checked_free(block_renamed);
    block_renamed = NULL;

    // 对phi里面某些src为空（意思是从某些路径走没有定义）进行处理，直接变0
    for(G_nodeList now = nl; now; now = now->tail){
        AS_block b = G_nodeInfo(now->head);
        // fprintf(stderr, "now in label: %s\n", Temp_labelstring(b->label));
        for(AS_instrList il = b->instrs; il; il = il->tail){
            if(is_Phi(il->head))
                handle_phi_null(il->head);
        }
    }

    //清空VAR表
    var_clear();
    used_clear();
}