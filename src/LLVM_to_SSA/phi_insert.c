#include <stdio.h>
#include <assert.h>
#include "phi_insert.h"
#include "LS_define.h"
#include "domi_frontier.h"
#include "../Utils/assemblock.h"
#include "../TIR_to_LLVM/TL_define.h"



static PHI_table phi_table = NULL;
static PHI_table phi_Table(){
    if(!phi_table) phi_table = PHI_empty();
    return phi_table;
}
static void PHI_clear(){
    if(phi_table)
        checked_free(phi_table);
    phi_table = NULL;
}


static bool tag_block(int id, Temp_temp tmp){
    Temp_tempList tl = PHI_look(phi_Table(), id);
    if(!tl){
        PHI_enter(phi_Table(), id, Temp_TempList(tmp, NULL));
        return TRUE;
    }
    string tmp_str = Temp_look(Temp_name(), tmp);
    assert(tmp_str);
    Temp_tempList last = NULL;
    while(tl){
        string in_tmp_str = Temp_look(Temp_name(), tl->head);
        assert(in_tmp_str);
        if(!strcmp(in_tmp_str, tmp_str)) return FALSE;
        last = tl;
        tl = tl->tail;
    }
    last->tail = Temp_TempList(tmp, NULL);
    return TRUE;
}



static Temp_tempList has_define(G_node n){
    AS_block b = G_nodeInfo(n);
    Temp_tempList tl = Temp_TempList(NULL, NULL);
    for(AS_instrList il = b->instrs; il; il = il->tail){
        AS_instr i = il->head;
        if(is_Inttoptr(i) || is_Icmp(i)) continue;
        switch (i->kind)
        {
        case I_OPER:
        {
            concatTempList(tl, i->u.OPER.dst);
        }   
        break;
        case I_LABEL:
        {
            //do nothing
        }
        break;
        case I_MOVE:
        {
            concatTempList(tl, i->u.MOVE.dst);
        }
        break;
        default:
        assert(0);
        break;
        }
    }
    return tl->tail;
}


static string get_Phi_template(int n){
    const int phi_extra_len = 10;
    const int single_var_len = 20;
    string ret = checked_malloc(phi_extra_len + single_var_len * n);
    strcat(ret, PHI_TEMPLATE);
    // assert(!strncmp(ret, PHI_TEMPLATE, 16));
    for(int i = 0; i < n; i++){
        string single_var = checked_malloc(single_var_len);
        sprintf(single_var, "[%%`s%d,%%`j%d],", i, i);
        strcat(ret, single_var);
    }
    ret[strlen(ret) - 1] = '\0';
    return ret;
}

static Temp_tempList get_duplicate_Temp(Temp_temp tmp, int n){
    assert(tmp);
    Temp_tempList tl = Temp_TempList(NULL, NULL);
    Temp_tempList ret = tl;
    for(int i = 0; i < n; i++){
        tl->tail = Temp_TempList(tmp, NULL);
        tl = tl->tail;
    }
    return ret->tail;
}

static AS_targets get_Label_from_Node(G_nodeList nl){
    Temp_labelList ret = Temp_LabelList(NULL, NULL), ll = ret;
    while(nl){
        AS_block b = G_nodeInfo(nl->head);
        ret->tail = Temp_LabelList(b->label, NULL);
        ret = ret->tail;
        nl = nl->tail;
    }
    assert(ll->tail);
    return AS_Targets(ll->tail);
}



void phi_insert(G_nodeList nl, Temp_tempList args){

    // 拿到DF
    domi_table DF = find_DomiFrontier(nl);


    bool is_hold = FALSE;
    G_nodeList iter_list = NULL;
    while(!is_hold){
        iter_list = nl;
        bool block_hold = TRUE;
        while(iter_list){
            //是否含有定义
            Temp_tempList tl = has_define(iter_list->head);
            if(G_mykey(iter_list->head) == 0){
                concatTempList(tl, args);
            }
            concatTempList(tl, PHI_look(phi_Table(), G_mykey(iter_list->head) + 1));
            //往DF的block里加phi的tag
            for(int i = 0; i < DF->n; i++){
                if(domi_lookup(DF, G_mykey(iter_list->head), i)){
                    while(tl){
                        block_hold &= !tag_block(i + 1, tl->head);
                        tl = tl->tail;
                    }
                }
            }
            freeTempList(tl);
            iter_list = iter_list->tail;
        }
        is_hold = block_hold;
    }
    //真正放置phi
    iter_list = nl;
    while(iter_list){
        Temp_tempList tl = PHI_look(phi_Table(), G_mykey(iter_list->head) + 1);
        int pred_num = 0;
        G_nodeList preds = G_pred(iter_list->head);
        for(G_nodeList now = preds; now; now = now->tail) pred_num++;
        while(tl){
            AS_block b = G_nodeInfo(iter_list->head);
            b->instrs->tail = AS_InstrList(
                AS_Oper(
                    get_Phi_template(pred_num), 
                    Temp_TempList(tl->head, NULL),
                    get_duplicate_Temp(tl->head, pred_num),
                    get_Label_from_Node(preds)
                ),
                b->instrs->tail
            );
            tl = tl->tail;
        }
        iter_list = iter_list->tail;
    }

    // 清空PHI表
    PHI_clear();
}


