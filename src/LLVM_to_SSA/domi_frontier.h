#pragma once
#include "LS_define.h"
#include "../cfg/graph.h"


void print_DomiFrontier(FILE* out, domi_table dt);
domi_table find_DomiFrontier(G_nodeList nl);
