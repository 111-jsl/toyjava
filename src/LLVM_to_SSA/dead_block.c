#include <stdio.h>
#include <assert.h>
#include "dead_block.h"

static PHI_table live_table = NULL;
static PHI_table live_Table(){
    if(!live_table) live_table = PHI_empty();
    return live_table;
}
static void live_clear(){
    if(live_table) checked_free(live_table);
    live_table = NULL;
}


static void block_dfs(G_node n){
    if(PHI_look(live_Table(), G_mykey(n) + 1)) return;
    PHI_enter(live_Table(), G_mykey(n) + 1, G_mykey(n) + 1);
    G_nodeList succs = G_succ(n);
    for(G_nodeList now = succs; now; now = now->tail){
        block_dfs(now->head);
    }
}


static void detach_single_block(G_nodeList nl, G_nodeList target, G_nodeList last){
    assert(nl && target && last);
    int id = G_mykey(target->head);
    for(G_nodeList now = nl; now; now = now->tail){
        G_node n = now->head;
        G_nodeList preds = G_pred(n);
        G_nodeList last = NULL;
        for(G_nodeList pred = preds; pred; pred = pred->tail){
            if(G_mykey(pred->head) == id){
                if(last){
                    last->tail = pred->tail;
                }else{
                    n->preds = pred->tail;
                }
            }
            last = pred;
        }
    }
    last->tail = target->tail;
}


void detach_dead_block(G_nodeList nl){

    block_dfs(nl->head);
    G_nodeList last = NULL;
    for(G_nodeList now = nl; now; now = now->tail){
        if(!PHI_look(live_Table(), G_mykey(now->head) + 1)){
            detach_single_block(nl, now, last);
        }else{
            last = now;
        }
    }


    live_clear();
}