#include "bitmap.h"

// initialize a bitmap with `size` bits. All bits are cleared.
void init_bitmap(BitmapCell* bitmap, int size){
    memset(bitmap, 0, size);
}

// get the bit at `index`.
bool bitmap_get(BitmapCell* bitmap, int index) {
    int idx, offset;
    BITMAP_PARSE_INDEX(index, idx, offset);
    return (bitmap[idx] >> offset) & 1;
}

// set the bit at `index` to 1.
void bitmap_set(BitmapCell* bitmap, int index) {
    int idx, offset;
    BITMAP_PARSE_INDEX(index, idx, offset);
    bitmap[idx] |= BIT(offset);
}

// set the bit at `index` to 0.
void bitmap_clear(BitmapCell* bitmap, int index) {
    int idx, offset;
    BITMAP_PARSE_INDEX(index, idx, offset);
    bitmap[idx] &= ~BIT(offset);
}