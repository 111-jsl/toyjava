#include <stdio.h>
#include <assert.h>
#include "domi_frontier.h"
#include "domi.h"

void print_DomiFrontier(FILE* out, domi_table dt){
    domi_print(out, dt, "DomiFrontier");
}

static void print_block_preds(G_nodeList nl){
    while(nl){
        fprintf(stderr, "(%d): ", G_mykey(nl->head));
        G_nodeList preds = G_pred(nl->head);
        while(preds){
            fprintf(stderr, "%d ", G_mykey(preds->head));
            preds = preds->tail;
        }
        fprintf(stderr, "\n");
        nl = nl->tail;
    }
}

static void print_block_succs(G_nodeList nl){
    while(nl){
        fprintf(stderr, "(%d): ", G_mykey(nl->head));
        G_nodeList succs = G_succ(nl->head);
        while(succs){
            fprintf(stderr, "%d ", G_mykey(succs->head));
            succs = succs->tail;
        }
        fprintf(stderr, "\n");
        nl = nl->tail;
    }
}


domi_table find_DomiFrontier(G_nodeList nl){
    // print_block_succs(nl);

    domi_table passive_D = find_Dominators(nl);
    int n = passive_D->n;
    domi_table DF = domi_Table(n);
    
    G_nodeList iter_list = nl;
    while(iter_list){
        G_nodeList pred = G_pred(iter_list->head);
        while(pred){
            for(int i = 0; i < n; i++){
                // fprintf(stderr, "%d\n", G_mykey(pred));
                if(domi_lookup(passive_D, G_mykey(pred->head), i)){
                    if(i == G_mykey(iter_list->head) || 
                    !domi_lookup(passive_D, G_mykey(iter_list->head), i)){
                        domi_set(DF, i, G_mykey(iter_list->head));
                    }
                }
            }
            pred = pred->tail;
        }
        iter_list = iter_list->tail;
    }
    domi_freeTable(passive_D);
    // print_DomiFrontier(stderr, DF);
    return DF;
}