#include <stdio.h>
#include <assert.h>
#include "degraph.h"


AS_blockList degraph(G_nodeList nl){
    AS_blockList ret = AS_BlockList(NULL, NULL);
    AS_blockList hd = ret;
    while(nl){
        G_node n = nl->head;
        assert(n);
        ret->tail = AS_BlockList(G_nodeInfo(n), NULL);
        ret = ret->tail;
        nl = nl->tail;
    }
    return hd->tail;
}