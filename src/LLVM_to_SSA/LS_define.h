#pragma once
#include "../Utils/util.h"
#include "../Utils/temp.h"
#include "../Utils/table.h"
#include "../Utils/assem.h"
#include "bitmap.h"


// 用来装必经结点的各种信息
typedef struct domi_table_ *domi_table;
struct domi_table_ {
    int n;
    BitmapCell **table;
};

domi_table domi_Table(int n);
void domi_freeTable(domi_table dt);
void domi_print(FILE* out, domi_table dt, string head);


bool domi_lookup(domi_table dt, int x, int y);
bool domi_set(domi_table dt, int x, int y);
bool domi_clear(domi_table dt, int x, int y);
bool domi_range_set(domi_table dt, int x, BitmapCell *y);

void domi_intersect_begin(domi_table dt);
void domi_intersect(BitmapCell *bmp);
BitmapCell* domi_intersect_end();


// 表示这个id的结点有多少变量需要使用phi
typedef struct TAB_table_ *PHI_table;
PHI_table PHI_empty();
void PHI_enter(PHI_table t, int id, void *value);
void *PHI_look(PHI_table t, int id);


// x -> x1, x2, ..., xn  变量重命名保存的栈
typedef struct TAB_table_ *VAR_table;
VAR_table VAR_empty();
void VAR_enter(VAR_table t, Temp_temp tmp, void *value);
void *VAR_look(VAR_table t, Temp_temp tmp);
void *VAR_pop(VAR_table t);


// templist的一些操作
Temp_tempList deepcopyTempList(Temp_tempList tl);
void concatTempList(Temp_tempList dst, Temp_tempList src);
void freeTempList(Temp_tempList tl);


// 对一些指令的特判
#define PHI_TEMPLATE_LEN 14
#define PHI_TEMPLATE ("%`d0 = phi i64 ")
#define INTTOPTR_TEMPLATE_LEN 19
#define INTTOPTR_TEMPLATE ("%`d0 = inttoptr i64 ")
#define ICMP_TEMPLATE_LEN 11
#define ICMP_TEMPLATE ("%`d0 = icmp ")

bool is_Inttoptr(AS_instr i);
bool is_Phi(AS_instr i);
bool is_Icmp(AS_instr i);
