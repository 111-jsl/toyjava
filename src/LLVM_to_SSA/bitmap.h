#pragma once
#include <string.h>
#include "../Utils/util.h"

// bitmap is a compact representation of boolean array.
// consecutive 64 bits are stored in one u64 (BitmapCell).
typedef unsigned char BitmapCell;

#define BIT(i) (1ull << (i))
#define BITMAP_BITS_PER_CELL (sizeof(BitmapCell) * 8)
#define BITMAP_TO_NUM_CELLS(size) (((size) + BITMAP_BITS_PER_CELL - 1) / BITMAP_BITS_PER_CELL)

// calculate cell index `idx` and in-cell `offset` from `index`.
#define BITMAP_PARSE_INDEX(index, idx, offset) \
    do { \
        idx = index / BITMAP_BITS_PER_CELL; \
        offset = index % BITMAP_BITS_PER_CELL; \
    } while (FALSE)

// declare a new bitmap with `size` bits.
#define Bitmap(name, size) BitmapCell name[BITMAP_TO_NUM_CELLS(size)]




// initialize a bitmap with `size` bits. All bits are cleared.
void init_bitmap(BitmapCell* bitmap, int size);

// get the bit at `index`.
bool bitmap_get(BitmapCell* bitmap, int index);

// set the bit at `index` to 1.
void bitmap_set(BitmapCell* bitmap, int index);

// set the bit at `index` to 0.
void bitmap_clear(BitmapCell* bitmap, int index);
