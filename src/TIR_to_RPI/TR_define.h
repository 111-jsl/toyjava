#pragma once
#include "../Utils/treep.h"

#define R_INST_MAX_SIZE 50

#define R_LABEL(l) (#l":")
#define ADD(s1) ("add `d0, `s0, "#s1)
#define SUB(s1) ("sub `d0, `s0, "#s1)
#define RSB(s1) ("rsb `d0, `s0, "#s1)
#define MUL ("mul `d0, `s0, `s1")
#define SDIV ("sdiv `d0, `s0, `s1")
#define AND(s1) ("and `d0, `s0, "#s1)
#define ORR(s1) ("orr `d0, `s0, "#s1)
#define MOV(s0) ("mov `d0, "#s0)
#define LDR(s1) ("ldr `d0, [`s0, "#s1"]")
#define LDR_NO_OFFSET ("ldr `d0, [`s0]")
#define LDR_LABEL ("ldr `d0, `j0")
#define STR(s2) ("str `s0, [`s1, "#s2"]")
#define STR_NO_OFFSET ("str `s0, [`s1]")
#define CMP(s1) ("cmp `s0, "#s1)
#define B(cond) ("b"#cond" `j0")
#define BL(cond) ("bl"#cond" `j0")
#define BX(cond) ("bx"#cond" `s0")
#define BLX(cond) ("blx"#cond" `s0")
#define NOP ("nop")
#define PUSH(sl) ("push {"#sl"}")
#define POP(sl) ("pop {"#sl"}")
#define LDMIA(s0, sl) ("ldmia "#s0", {"#sl"}")


#define REG_PC (TC("t15"))
#define REG_LR (TC("t14"))
#define REG_SP (TC("t13"))
#define REG_FP (TC("t11"))


const string R_mapCND(T_relOp r);


typedef struct Op2_bind_{
    string format;
    Temp_tempList tl;
} *Op2_bind;

typedef struct Src2_bind_{
    Op2_bind s0;
    Op2_bind s1;
} *Src2_bind;

Op2_bind Op2_Bind(string f, Temp_tempList tl);
Src2_bind Src2_Bind(Op2_bind s0, Op2_bind s1);

Op2_bind getOp2Type(T_exp e, int id);

Src2_bind getSrc2Type(T_exp e0, T_exp e1, int second_id);


const string R_T_expList_to_string(T_expList el, Temp_tempList tl, int st, string mode);
void handle_call_args(T_expList args);
void handle_ext_call_args(T_expList args);

void callee_save();
void callee_get();
void caller_save();
void caller_get();


Temp_tempList R_mergeTempList(Temp_tempList tl1, Temp_tempList tl2);

void R_checkTempListSafety(Temp_tempList tl);