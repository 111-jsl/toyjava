#include "R_Tileselect_Stm.h"
#include "R_Tileselect_Exp.h"
#include "TR_define.h"
#include <assert.h>
#include <string.h>

void R_emit(AS_instr i);

// tile gadgets
static bool isBOP(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_TEMP){
        switch (s->u.MOVE.src->kind)
        {
        case T_BINOP:
        {
            Temp_temp r = s->u.MOVE.dst->u.TEMP;
            T_exp e = s->u.MOVE.src;
            string inst = checked_malloc(R_INST_MAX_SIZE);
            
            
            switch (e->u.BINOP.op)
            {
            case T_plus:
            {
                Src2_bind sb = getSrc2Type(
                    e->u.BINOP.left,
                    e->u.BINOP.right,
                    1
                );
                sprintf(
                    inst,
                    ADD(%s),
                    sb->s1->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(sb->s0->tl, sb->s1->tl),
                        NULL
                    )
                );
            }
            break;
            case T_and:
            {
                Src2_bind sb = getSrc2Type(
                    e->u.BINOP.left,
                    e->u.BINOP.right,
                    1
                );
                sprintf(
                    inst,
                    AND(%s),
                    sb->s1->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(sb->s0->tl, sb->s1->tl),
                        NULL
                    )
                );
            }
            break;
            case T_or:
            {
                Src2_bind sb = getSrc2Type(
                    e->u.BINOP.left,
                    e->u.BINOP.right,
                    1
                );
                sprintf(
                    inst,
                    ORR(%s),
                    sb->s1->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(sb->s0->tl, sb->s1->tl),
                        NULL
                    )
                );
            }
            break;
            case T_minus:
            {
                Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
                Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
                if(!s0->tl && s1->tl){
                    sprintf(
                        inst,
                        RSB(%s),
                        s0->format
                    );
                    R_emit(
                        AS_Oper(
                            inst,
                            Temp_TempList(r, NULL),
                            R_mergeTempList(s1->tl, s0->tl),
                            NULL
                        )
                    );
                    break;
                }else if(!s0->tl && !s1->tl){
                    s0 = Op2_Bind(
                        "`s1", 
                        Temp_TempList(
                            R_munchExp(e->u.BINOP.left),
                            NULL
                        )
                    );
                }
                sprintf(
                    inst,
                    SUB(%s),
                    s1->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(s0->tl, s1->tl),
                        NULL
                    )
                );

            }
            break;
            case T_mul:
            {
                Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
                Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
                if(!s0->tl || s0->tl->head == s->u.MOVE.dst->u.TEMP){
                    s0 = Op2_Bind(
                        "`s1", 
                        Temp_TempList(
                            R_munchExp(e->u.BINOP.left),
                            NULL
                        )
                    );
                }
                if(!s1->tl || s1->tl->head == s->u.MOVE.dst->u.TEMP){
                    s1 = Op2_Bind(
                        "`s1", 
                        Temp_TempList(
                            R_munchExp(e->u.BINOP.right),
                            NULL
                        )
                    );
                }
                sprintf(
                    inst,
                    MUL
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(s0->tl, s1->tl),
                        NULL
                    )
                );
            }
            break;
            case T_div:
            {
                Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
                Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
                if(!s0->tl || s0->tl->head == s->u.MOVE.dst->u.TEMP){
                    s0 = Op2_Bind(
                        "`s1", 
                        Temp_TempList(
                            R_munchExp(e->u.BINOP.left),
                            NULL
                        )
                    );
                }
                if(!s1->tl || s1->tl->head == s->u.MOVE.dst->u.TEMP){
                    s1 = Op2_Bind(
                        "`s1", 
                        Temp_TempList(
                            R_munchExp(e->u.BINOP.right),
                            NULL
                        )
                    );
                }
                sprintf(
                    inst,
                    SDIV
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(s0->tl, s1->tl),
                        NULL
                    )
                );
            }
            break;
            default:
            assert(0);
            break;
            }
            return TRUE;
        }
        break;
        default:
        break;
        }
    }
    return FALSE;
}


static bool isMOVE(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_TEMP){
        switch (s->u.MOVE.src->kind)
        {
        case T_CONST:
        {
            string inst = checked_malloc(R_INST_MAX_SIZE);
            sprintf(
                inst,
                MOV(#%d),
                s->u.MOVE.src->u.CONST
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    NULL,
                    NULL
                )
            );
            return TRUE;
        }
        break;
        case T_TEMP:
        {
            string inst = checked_malloc(R_INST_MAX_SIZE);
            sprintf(
                inst,
                MOV(`s0)
            );
            R_emit(
                AS_Move(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    Temp_TempList(s->u.MOVE.src->u.TEMP, NULL)
                )
            );
            return TRUE;
        }
        break;
        case T_NAME:
        {
            string inst = checked_malloc(R_INST_MAX_SIZE);
            sprintf(
                inst,
                MOV(%s),
                Temp_labelstring(s->u.MOVE.src->u.NAME)
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    NULL,
                    NULL
                )
            );
            return TRUE;
        }
        default:
        {
            string inst = checked_malloc(R_INST_MAX_SIZE);
            Temp_temp t1 = R_munchExp(s->u.MOVE.src);
            sprintf(
                inst,
                MOV(`s0)
            );
            R_emit(
                AS_Move(
                    inst,
                    Temp_TempList(s->u.MOVE.dst->u.TEMP, NULL),
                    Temp_TempList(t1, NULL)
                )
            );
            return TRUE;
        }
        break;
        }
    }
    return FALSE;
}

static bool isLOAD(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_TEMP
    && s->u.MOVE.src->kind == T_MEM){
        string inst = checked_malloc(R_INST_MAX_SIZE);
        Temp_temp r = s->u.MOVE.dst->u.TEMP;
        T_exp e = s->u.MOVE.src;
        switch (e->u.MEM->kind)
        {
        case T_BINOP:
        {
            if(e->u.MEM->u.BINOP.op != T_plus){
                Op2_bind ob = getOp2Type(e->u.MEM, 0);
                sprintf(
                    inst,
                    LDR_NO_OFFSET
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        ob->tl,
                        NULL
                    )
                );
            }else{
                Src2_bind sb = getSrc2Type(
                    e->u.MEM->u.BINOP.left,
                    e->u.MEM->u.BINOP.right,
                    1
                );
                sprintf(
                    inst,
                    LDR(%s),
                    sb->s1->format
                );

                R_emit(
                    AS_Oper(
                        inst, 
                        Temp_TempList(r, NULL),
                        R_mergeTempList(sb->s0->tl, sb->s1->tl),
                        NULL
                    )
                );
            }
        }
        break;
        default:
        {
            Op2_bind ob = getOp2Type(e->u.MEM, 0);
            sprintf(
                inst,
                LDR_NO_OFFSET
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    ob->tl,
                    NULL
                )
            );
        }
        break;
        }
        return TRUE;
    }
    return FALSE;
}


static bool isSTORE(T_stm s){
    if(s->kind == T_MOVE
    && s->u.MOVE.dst->kind == T_MEM){
        string inst = checked_malloc(R_INST_MAX_SIZE);
        T_exp e = s->u.MOVE.dst;
        T_exp src = s->u.MOVE.src;
        
        switch (e->u.MEM->kind)
        {
        case T_BINOP:
        {
            if(e->u.MEM->u.BINOP.op != T_plus){
                Temp_temp t1 = R_munchExp(e->u.MEM);
                Temp_temp t2 = R_munchExp(src);
                sprintf(
                    inst,
                    STR_NO_OFFSET
                );
                R_emit(
                    AS_Oper(
                        inst,
                        NULL,
                        Temp_TempList(
                            t2,
                            Temp_TempList(
                                t1,
                                NULL
                            )
                        ),
                        NULL
                    )
                );
            }else{
                Src2_bind sb = getSrc2Type(
                    e->u.MEM->u.BINOP.left,
                    e->u.MEM->u.BINOP.right,
                    2
                );
                Temp_temp t1 = R_munchExp(src);
                sprintf(
                    inst,
                    STR(%s),
                    sb->s1->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        NULL,
                        Temp_TempList(
                            t1,
                            R_mergeTempList(
                                sb->s0->tl,
                                sb->s1->tl
                            )
                        ),
                        NULL
                    )
                );
            }
            return TRUE;
        }
        break;
        default:
        {
            Temp_temp t1 = R_munchExp(e->u.MEM);
            Temp_temp t2 = R_munchExp(src);
            sprintf(
                inst,
                STR_NO_OFFSET
            );
            R_emit(
                AS_Oper(
                    inst,
                    NULL,
                    Temp_TempList(
                        t2,
                        Temp_TempList(
                            t1,
                            NULL
                        )
                    ),
                    NULL
                )
            );
            return TRUE;
        }
        break;
        }
    }
    return FALSE;
}
static bool isCJUMP(T_stm s){
    if(s->kind == T_CJUMP){
        string inst1 = checked_malloc(R_INST_MAX_SIZE);
        string inst2 = checked_malloc(R_INST_MAX_SIZE);
        string extra_label = checked_malloc(R_INST_MAX_SIZE);
        string inst3 = checked_malloc(R_INST_MAX_SIZE);
        Src2_bind sb = getSrc2Type(
            s->u.CJUMP.left,
            s->u.CJUMP.right,
            1
        );
        sprintf(
            inst1,
            CMP(%s),
            sb->s1->format
        );
        sprintf(
            inst2,
            B(%s),
            R_mapCND(s->u.CJUMP.op)
        );
        sprintf(
            inst3,
            B()
        );
        Temp_label elabel = Temp_newlabel();
        sprintf(
            extra_label,
            R_LABEL(%s),
            Temp_labelstring(elabel)
        );
        R_emit(
            AS_Oper(
                inst1,
                NULL,
                R_mergeTempList(
                    sb->s0->tl,
                    sb->s1->tl
                ),
                NULL
            )
        );
        R_emit(
            AS_Oper(
                inst2,
                NULL,
                NULL,
                AS_Targets(
                    Temp_LabelList(
                        s->u.CJUMP.true,
                        Temp_LabelList(
                            elabel,
                            NULL
                        )
                    )
                )
            )
        );
        R_emit(
            AS_Label(
                extra_label,
                elabel
            )
        );
        R_emit(
            AS_Oper(
                inst3,
                NULL,
                NULL,
                AS_Targets(
                    Temp_LabelList(
                        s->u.CJUMP.false,
                        NULL
                    )
                )
            )
        );
        return TRUE;

    }
    return FALSE;
}


static bool isJUMP(T_stm s){
    if(s->kind == T_JUMP){
        string inst = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            inst,
            B()
        );
        R_emit(
            AS_Oper(
                inst,
                NULL,
                NULL,
                AS_Targets(Temp_LabelList(
                    s->u.JUMP.jump, NULL
                ))
            )
        );
        return TRUE;
    }
    return FALSE;
}

static bool isCALL_STM(T_stm s){
    if(s->kind == T_EXP){
        T_exp e = s->u.EXP;
        switch (e->kind)
        {
        case T_CALL:
        {
            // !
            // caller_save();

            
            Op2_bind ob = getOp2Type(e->u.CALL.obj, 0);
            assert(ob->tl);
            handle_call_args(e->u.CALL.args);
            string inst1 = checked_malloc(R_INST_MAX_SIZE);
            sprintf(
                inst1,
                BLX()
            );

            

            R_emit(
                AS_Oper(
                    inst1,
                    // !
                    Temp_TempList(
                        TC("t0"),
                        Temp_TempList(
                            TC("t1"),
                            Temp_TempList(
                                TC("t2"),
                                Temp_TempList(
                                    TC("t3"),
                                    Temp_TempList(
                                        REG_LR,
                                        NULL
                                    )
                                )
                            )
                        )
                    ),
                    // !
                    R_mergeTempList(
                        ob->tl,
                        Temp_TempList(
                            TC("t0"),
                            NULL
                        )
                    ),
                    NULL
                )
            );

            // !
            // caller_get();
            return TRUE;
        }    
        break;
        case T_ExtCALL:
        {
            if(!strcmp(e->u.ExtCALL.extfun, "putint")
            || !strcmp(e->u.ExtCALL.extfun, "putch")
            || !strcmp(e->u.ExtCALL.extfun, "putarray")
            || !strcmp(e->u.ExtCALL.extfun, "starttime")
            || !strcmp(e->u.ExtCALL.extfun, "stoptime")){
                // ?
                if(!strcmp(e->u.ExtCALL.extfun, "starttime")){
                    e->u.ExtCALL.extfun = "_sysy_starttime";
                }else if(!strcmp(e->u.ExtCALL.extfun, "stoptime")){
                    e->u.ExtCALL.extfun = "_sysy_stoptime";
                }
                // !
                // caller_save();

                handle_ext_call_args(e->u.ExtCALL.args);
                string inst1 = checked_malloc(R_INST_MAX_SIZE);
            
                sprintf(
                    inst1, 
                    "bl %s",
                    e->u.ExtCALL.extfun
                );
                
            
                R_emit(
                    AS_Oper(
                        inst1,
                        // !
                        Temp_TempList(
                            TC("t0"),
                            Temp_TempList(
                                TC("t1"),
                                Temp_TempList(
                                    TC("t2"),
                                    Temp_TempList(
                                        TC("t3"),
                                        Temp_TempList(
                                            REG_LR,
                                            NULL
                                        )
                                    )
                                )
                            )
                        ),
                        // !
                        Temp_TempList(
                            TC("t0"),
                            NULL
                        ),
                        NULL
                    )
                );

                // !
                // caller_get();
            }
            return TRUE;
        }
        break;
        default:
            break;
        }
    }
    return FALSE;
}
static bool isRET(T_stm s){
    if(s->kind == T_RETURN){
        string move_ret_to_r0 = checked_malloc(R_INST_MAX_SIZE);
        string move_fp_to_sp = checked_malloc(R_INST_MAX_SIZE);
        string pop_sp = checked_malloc(R_INST_MAX_SIZE);
        string bx = checked_malloc(R_INST_MAX_SIZE);
        Op2_bind ob = getOp2Type(s->u.EXP, 0);
        sprintf(
            move_ret_to_r0,
            MOV(%s),
            ob->format
        );
        sprintf(
            move_fp_to_sp,
            MOV(`s0)
        );
        string pop_sp_format = "`d0,`d1";
        sprintf(
            pop_sp,
            POP(%s),
            pop_sp_format
        );
        sprintf(
            bx,
            BX()
        );
        


        if(ob->tl){
            R_emit(
                AS_Move(
                    move_ret_to_r0,
                    Temp_TempList(TC("t0"), NULL),
                    ob->tl
                )
            );
        }else{
            R_emit(
                AS_Oper(
                    move_ret_to_r0,
                    Temp_TempList(TC("t0"), NULL),
                    ob->tl,
                    NULL
                )
            );
        }

        // !
        callee_get();


        R_emit(
            AS_Move(
                move_fp_to_sp,
                Temp_TempList(REG_SP, NULL),
                Temp_TempList(REG_FP, NULL)
            )
        );
        R_emit(
            AS_Oper(
                pop_sp,
                Temp_TempList(
                    REG_FP, 
                    Temp_TempList(
                        REG_LR,
                        NULL
                    )
                ),
                NULL,
                NULL
            )
        );
        R_emit(
            AS_Oper(
                bx,
                NULL,
                Temp_TempList(
                    REG_LR, 
                    // !
                    Temp_TempList( 
                        TC("t0"),
                        Temp_TempList(
                            TC("t4"),
                            Temp_TempList(
                                TC("t5"),
                                Temp_TempList(
                                    TC("t6"),
                                    Temp_TempList(
                                        TC("t7"),
                                        Temp_TempList(
                                            TC("t8"),
                                            Temp_TempList(
                                                TC("t9"),
                                                Temp_TempList(
                                                    TC("t10"),
                                                    NULL
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                NULL
            )
        );
        
        return TRUE;
    }
    return FALSE;
}

static bool isLABEL(T_stm s){
    if(s->kind == T_LABEL){
        string inst = checked_malloc(R_INST_MAX_SIZE);
        sprintf(inst, R_LABEL(%s), S_name(s->u.LABEL));
        R_emit(
            AS_Label(inst, s->u.LABEL)
        );
        return TRUE;
    }
    return FALSE;
}


bool R_munchStm(T_stm s){
    // fprintf(stderr, "%d\n", s->kind);
    if(isSTORE(s)){
        return TRUE;
    }else if(isLOAD(s)){
        return TRUE;
    }else if(isBOP(s)){
        return TRUE;
    }else if(isMOVE(s)){
        return TRUE;
    }else if(isCJUMP(s)){
        return TRUE;
    }else if(isJUMP(s)){
        return TRUE;
    }else if(isCALL_STM(s)){
        return TRUE;
    }else if(isRET(s)){
        return TRUE;
    }else if(isLABEL(s)){
        return TRUE;
    }
    assert(0);
    return FALSE;
}