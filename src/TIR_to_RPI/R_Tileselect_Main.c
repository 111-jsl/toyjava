#include "R_Tileselect_Main.h"
#include "R_Tileselect_Stm.h"
#include "TR_define.h"
#include "../Utils/symbol.h"
#include "../Canon/API_canon.h"
#include "../AST_to_TIR/AT_define.h"
#include <assert.h>
#include <string.h>

static AS_instrList il;
static const int SingleParam_Length = 20;
static const int PrologExtra = 60;

extern int UNIT_SIZE;
extern int ELE_SIZE;
void R_emit(AS_instr i){
    assert(i);
    il->tail = AS_InstrList(i, NULL);
    il = il->tail;
}



AS_instrList R_munchFuncProlog(string fname, Temp_tempList args, AS_blockList bl){    
    il = AS_InstrList(NULL, NULL);
    AS_instrList ret = il;
    string label_attr = checked_malloc(R_INST_MAX_SIZE);
    string label = checked_malloc(R_INST_MAX_SIZE);
    string nop_for_def = checked_malloc(R_INST_MAX_SIZE);
    string push_fp = checked_malloc(R_INST_MAX_SIZE);
    string save_fp = checked_malloc(R_INST_MAX_SIZE);
    string fetch_args_from_reg = NULL;
    string fetch_args_from_fp = NULL;
    string branch = checked_malloc(R_INST_MAX_SIZE);

    sprintf(
        label_attr,
        ".global %s",
        fname
    );
    R_emit(
        AS_Oper(label_attr, NULL, NULL, NULL)
    );

    sprintf(
        label,
        R_LABEL(%s),
        fname
    );
    R_emit(
        AS_Label(
            label,
            Temp_namedlabel(label)
        )
    );
    sprintf(
        nop_for_def,
        " "
    );
    R_emit(
        AS_Oper(
            nop_for_def,
            Temp_TempList(
                REG_FP,
                Temp_TempList(
                    REG_LR,
                    Temp_TempList(
                        REG_SP,
                        Temp_TempList(
                            TC("t4"),
                            Temp_TempList(
                                TC("t5"),
                                Temp_TempList(
                                    TC("t6"),
                                    Temp_TempList(
                                        TC("t7"),
                                        Temp_TempList(
                                            TC("t8"),
                                            Temp_TempList(
                                                TC("t9"),
                                                Temp_TempList(
                                                    TC("t10"),
                                                    NULL
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            NULL,
            NULL
        )
    );

    string push_fp_format = "`s0,`s1";
    sprintf(
        push_fp,
        PUSH(%s),
        push_fp_format
    );
    R_emit(
        AS_Oper(
            push_fp,
            NULL,
            Temp_TempList(
                REG_FP, 
                Temp_TempList(
                    REG_LR,
                    NULL
                )
            ),
            NULL
        )
    );
    sprintf(
        save_fp,
        MOV(`s0)
    );
    R_emit(
        AS_Move(
            save_fp,
            Temp_TempList(REG_FP, NULL),
            Temp_TempList(REG_SP, NULL)
        )
    );
    
    // !
    callee_save();
    
    int reg_num = 0;
    Temp_tempList now = NULL;
    for(now = args; now && reg_num < 4; now = now->tail, reg_num++){
        fetch_args_from_reg = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            fetch_args_from_reg,
            MOV(`s0)
        );
        string reg_id = checked_malloc(10);
        sprintf(reg_id, "t%d", reg_num);
        R_emit(
            AS_Move(
                fetch_args_from_reg,
                Temp_TempList(now->head, NULL),
                Temp_TempList(TC(reg_id), NULL)
            )
        );
    }

    int reg_instack_n = 0;
    for(Temp_tempList c = now; c; c = c->tail) reg_instack_n++;

    for(; now; now = now->tail, reg_instack_n--){
        fetch_args_from_fp = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            fetch_args_from_fp,
            LDR(#%d),
            (reg_instack_n + 2) * UNIT_SIZE
        );
        R_emit(
            AS_Oper(
                fetch_args_from_fp,
                Temp_TempList(now->head, NULL),
                Temp_TempList(REG_SP, NULL),
                NULL
            )
        );
        
    }
    
    


    
    
    sprintf(branch, B());
    R_emit(
        AS_Oper(
            branch,
            NULL,
            NULL,
            AS_Targets(Temp_LabelList(bl->head->label, NULL))
        )
    );
    
    return ret->tail;
}

AS_instrList R_munchFuncEpilog(struct C_block c){
    string last_label = checked_malloc(10);
    sprintf(last_label, "%s:", S_name(c.label));
    return 
    AS_InstrList(
        AS_Label(last_label, c.label),
        AS_InstrList(
            AS_Oper(BX(), NULL, Temp_TempList(REG_LR, NULL), NULL),
            NULL
        )
    );
    // return NULL;
        
}

static void checkBlockList(AS_blockList bl){
    while(bl){
        assert(bl->head);
        bl = bl->tail;
    }
}

static void print_AS_blockList(AS_blockList bl){
    while(bl){
        AS_block b = bl->head;
        for(AS_instrList il = b->instrs; il; il = il->tail){
            AS_instr i = il->head;
            switch (i->kind)
            {
            case I_OPER:
            {
                fprintf(stderr, "%s\n", i->u.OPER.assem);
            }
            break;
            case I_LABEL:
            {
                fprintf(stderr, "%s\n", i->u.LABEL.assem);
            }
            break;
            case I_MOVE:
            {
                fprintf(stderr, "%s\n", i->u.MOVE.assem);
            }
            break;
            default:
            break;
            }
        }
        bl = bl->tail;
    }
}


AS_blockList R_munchFunc(struct C_block c){
    AS_blockList bl = AS_BlockList(NULL,NULL);
    AS_blockList bl_hd = bl;
    for(C_stmListList now = c.stmLists; now; now = now->tail){
        
        il = AS_InstrList(NULL, NULL);
        AS_instrList ret = il;
        
        // 1.处理内部语句
       for(T_stmList s = now->head; s; s = s->tail){
            R_munchStm(s->head);
        }
        // 2.函数头部
        //   a.组装
        // AS_printInstrList(stderr, ret->tail, Temp_name());
        bl->tail = AS_BlockList(AS_Block(ret->tail), NULL);
        // print_AS_blockList(bl->tail->head);
        bl = bl->tail;
    }
    assert(!bl_hd->head);
    checkBlockList(bl_hd->tail);
   
    // print_AS_blockList(bl_hd->tail);
    return bl_hd->tail;
}
