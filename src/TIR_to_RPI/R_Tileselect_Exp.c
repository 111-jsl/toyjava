#include "R_Tileselect_Exp.h"
#include "TR_define.h"

#include <assert.h>
#include <string.h>

void R_emit(AS_instr i);


// tile gadgets
static Temp_temp isBOP(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_BINOP:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(R_INST_MAX_SIZE);
        
        
        switch (e->u.BINOP.op)
        {
        case T_plus:
        {
            Src2_bind sb = getSrc2Type(
                e->u.BINOP.left,
                e->u.BINOP.right,
                1
            );
            sprintf(
                inst,
                ADD(%s),
                sb->s1->format
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(sb->s0->tl, sb->s1->tl),
                    NULL
                )
            );
        }
        break;
        case T_and:
        {
            Src2_bind sb = getSrc2Type(
                e->u.BINOP.left,
                e->u.BINOP.right,
                1
            );
            sprintf(
                inst,
                AND(%s),
                sb->s1->format
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(sb->s0->tl, sb->s1->tl),
                    NULL
                )
            );
        }
        break;
        case T_or:
        {
            Src2_bind sb = getSrc2Type(
                e->u.BINOP.left,
                e->u.BINOP.right,
                1
            );
            sprintf(
                inst,
                ORR(%s),
                sb->s1->format
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(sb->s0->tl, sb->s1->tl),
                    NULL
                )
            );
        }
        break;
        case T_minus:
        {
            Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
            Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
            if(!s0->tl && s1->tl){
                sprintf(
                    inst,
                    RSB(%s),
                    s0->format
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        R_mergeTempList(s1->tl, s0->tl),
                        NULL
                    )
                );
                break;
            }else if(!s0->tl && !s1->tl){
                s0 = Op2_Bind(
                    "`s1", 
                    Temp_TempList(
                        R_munchExp(e->u.BINOP.left),
                        NULL
                    )
                );
            }
            sprintf(
                inst,
                SUB(%s),
                s1->format
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(s0->tl, s1->tl),
                    NULL
                )
            );

        }
        break;
        case T_mul:
        {
            Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
            Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
            if(!s0->tl){
                s0 = Op2_Bind(
                    "`s1", 
                    Temp_TempList(
                        R_munchExp(e->u.BINOP.left),
                        NULL
                    )
                );
            }
            if(!s1->tl){
                s1 = Op2_Bind(
                    "`s1", 
                    Temp_TempList(
                        R_munchExp(e->u.BINOP.right),
                        NULL
                    )
                );
            }
            sprintf(
                inst,
                MUL
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(s0->tl, s1->tl),
                    NULL
                )
            );
        }
        break;
        case T_div:
        {
            Op2_bind s0 = getOp2Type(e->u.BINOP.left, 1);
            Op2_bind s1 = getOp2Type(e->u.BINOP.right, 1);
            if(!s0->tl){
                s0 = Op2_Bind(
                    "`s1", 
                    Temp_TempList(
                        R_munchExp(e->u.BINOP.left),
                        NULL
                    )
                );
            }
            if(!s1->tl){
                s1 = Op2_Bind(
                    "`s1", 
                    Temp_TempList(
                        R_munchExp(e->u.BINOP.right),
                        NULL
                    )
                );
            }
            sprintf(
                inst,
                SDIV
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    R_mergeTempList(s0->tl, s1->tl),
                    NULL
                )
            );
        }
        break;
        default:
        assert(0);
        break;
        }
    }
    break;
    default:
    break;
    }
   
    return r;
}

static Temp_temp isMOVE(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_CONST:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            inst,
            MOV(#%d),
            e->u.CONST
        );
        R_emit(
            AS_Oper(
                inst,
                Temp_TempList(r, NULL),
                NULL,
                NULL
            )
        );
    }
    break;
    case T_TEMP:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            inst,
            MOV(`s0)
        );
        R_emit(
            AS_Move(
                inst,
                Temp_TempList(r, NULL),
                Temp_TempList(e->u.TEMP, NULL)
            )
        );
    }
    break;
    case T_NAME:
    {
        r = Temp_newtemp();
        string inst = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            inst,
            "ldr `d0, =%s",
            Temp_labelstring(e->u.NAME)
        );
        R_emit(
            AS_Oper(
                inst,
                Temp_TempList(r, NULL),
                NULL,
                NULL
            )
        );
    }
    break;
    default:
    break;
    }
    return r;
}


static Temp_temp isLOAD(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_MEM:
    {
        string inst = checked_malloc(R_INST_MAX_SIZE);
        r = Temp_newtemp();
        switch (e->u.MEM->kind)
        {
        case T_BINOP:
        {
            if(e->u.MEM->u.BINOP.op != T_plus){
                Op2_bind ob = getOp2Type(e->u.MEM, 0);
                sprintf(
                    inst,
                    LDR_NO_OFFSET
                );
                R_emit(
                    AS_Oper(
                        inst,
                        Temp_TempList(r, NULL),
                        ob->tl,
                        NULL
                    )
                );
            }else{
                Src2_bind sb = getSrc2Type(
                    e->u.MEM->u.BINOP.left,
                    e->u.MEM->u.BINOP.right,
                    1
                );
                sprintf(
                    inst,
                    LDR(%s),
                    sb->s1->format
                );

                R_emit(
                    AS_Oper(
                        inst, 
                        Temp_TempList(r, NULL),
                        R_mergeTempList(sb->s0->tl, sb->s1->tl),
                        NULL
                    )
                );
            }
        }
        break;
        default:
        {
            Op2_bind ob = getOp2Type(e->u.MEM, 0);
            sprintf(
                inst,
                LDR_NO_OFFSET
            );
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(r, NULL),
                    ob->tl,
                    NULL
                )
            );
        }
        break;
        }
    }
    break;
    default:
        break;
    }

    return r;
}



static Temp_temp isCALL_EXP(T_exp e){
    Temp_temp r = NULL;
    switch (e->kind)
    {
    case T_CALL:
    {
        r = Temp_newtemp();

        // !
        // caller_save();

        Op2_bind ob = getOp2Type(e->u.CALL.obj, 0);
        assert(ob->tl);
        handle_call_args(e->u.CALL.args);

        string inst1 = checked_malloc(R_INST_MAX_SIZE);
        string inst2 = checked_malloc(R_INST_MAX_SIZE);
        
        sprintf(
            inst1,
            BLX()
        );
        sprintf(
            inst2,
            MOV(`s0)
        );
        

        R_emit(
            AS_Oper(
                inst1,
                // !
                Temp_TempList(
                    TC("t0"),
                    Temp_TempList(
                        TC("t1"),
                        Temp_TempList(
                            TC("t2"),
                            Temp_TempList(
                                TC("t3"),
                                Temp_TempList(
                                    REG_LR,
                                    NULL
                                )
                            )
                        )
                    )
                ),
                // !
                R_mergeTempList(
                    ob->tl,
                    Temp_TempList(
                        TC("t0"),
                        NULL
                    )
                ),
                NULL
            )
        );
        

        R_emit(
            AS_Move(
                inst2,
                Temp_TempList(r, NULL),
                Temp_TempList(TC("t0"), NULL)
            )
        );

        // !
        // caller_get();
        
    }    
    break;
    case T_ExtCALL:
    {
        if(!strcmp(e->u.ExtCALL.extfun, "getint")
        || !strcmp(e->u.ExtCALL.extfun, "getch")
        || !strcmp(e->u.ExtCALL.extfun, "getarray")
        || !strcmp(e->u.ExtCALL.extfun, "malloc")){
            r = Temp_newtemp();
            // // ?
            // if(!strcmp(e->u.ExtCALL.extfun, "starttime")){
            //     e->u.ExtCALL.extfun = "_sysy_starttime";
            // }else if(!strcmp(e->u.ExtCALL.extfun, "stoptime")){
            //     e->u.ExtCALL.extfun = "_sysy_stoptime";
            // }

            // !
            // caller_save();

            handle_ext_call_args(e->u.ExtCALL.args);
            string inst1 = checked_malloc(R_INST_MAX_SIZE);
            string inst2 = checked_malloc(R_INST_MAX_SIZE);
        
            sprintf(
                inst1, 
                "bl %s",
                e->u.ExtCALL.extfun
            );
            sprintf(
                inst2,
                MOV(`s0)
            );

            

            R_emit(
                AS_Oper(
                    inst1,
                    // !
                    Temp_TempList(
                        TC("t0"),
                        Temp_TempList(
                            TC("t1"),
                            Temp_TempList(
                                TC("t2"),
                                Temp_TempList(
                                    TC("t3"),
                                    Temp_TempList(
                                        REG_LR,
                                        NULL
                                    )
                                )
                            )
                        )
                    ),
                    // !
                    Temp_TempList(
                        TC("t0"),
                        NULL
                    ),
                    NULL
                )
            );

            

            R_emit(
                AS_Move(
                    inst2,
                    Temp_TempList(r, NULL),
                    Temp_TempList(TC("t0"), NULL)
                )
            );

            // !
            // caller_get();
        }
    }
    break;
    default:
        break;
    }
    return r; 

}



Temp_temp R_munchExp(T_exp e){
    Temp_temp r;
    r = isLOAD(e);
    if(r) return r;
    r = isCALL_EXP(e);
    if(r) return r;
    r = isBOP(e);
    if(r) return r;
    r = isMOVE(e);
    if(r) return r;
    assert(0);
    return NULL;
}

