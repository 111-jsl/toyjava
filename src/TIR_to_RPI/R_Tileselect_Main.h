#pragma once
#include "../Utils/assemblock.h"
#include "../Canon/API_canon.h"

extern void R_emit(AS_instr i);
AS_instrList R_munchFuncProlog(string fname, Temp_tempList args, AS_blockList bl);
AS_instrList R_munchFuncEpilog(struct C_block c);
AS_blockList R_munchFunc(struct C_block c);
AS_instrList R_getBuildinFunc();