#include "TR_define.h"
#include "R_Tileselect_Exp.h"
#include <assert.h>
#include <string.h>

void R_emit(AS_instr i);



static const string CNDMap[6] = {
    "eq", "ne", "lt", "gt", "le", "ge"
};


const string R_mapCND(T_relOp r){
    assert(r >= 0 && r < 6);
    return CNDMap[r];
}

Op2_bind Op2_Bind(string f, Temp_tempList tl){
    Op2_bind ob = checked_malloc(sizeof(*ob));
    ob->format = f;
    ob->tl = tl;
    return ob;
}

Src2_bind Src2_Bind(Op2_bind s0, Op2_bind s1){
    Src2_bind sb = checked_malloc(sizeof(*sb));
    sb->s0 = s0;
    sb->s1 = s1;
    return sb;
}


Op2_bind getOp2Type(T_exp e, int id){
    Temp_tempList tl;
    string format = checked_malloc(8);

    switch (e->kind)
    {
    case T_CONST:
    {
        
        sprintf(format, "#%d", e->u.CONST);
        tl = NULL;
    }
    break;
    case T_TEMP:
    {
        sprintf(format, "`s%d", id);
        tl = Temp_TempList(e->u.TEMP, NULL);
    }
    break;
    case T_NAME:
    {
        sprintf(format, "%s", S_name(e->u.NAME));
        tl = NULL;
    }
    break;
    default:
    {
        Temp_temp t = R_munchExp(e);
        sprintf(format, "`s%d", id);
        tl = Temp_TempList(t, NULL);
    }
    break;
    }
    return Op2_Bind(format, tl);
}


Src2_bind getSrc2Type(T_exp e0, T_exp e1, int second_id){
    Op2_bind s0 = getOp2Type(e0, second_id);
    Op2_bind s1 = getOp2Type(e1, second_id);
    if(!s0->tl && s1->tl){
        Op2_bind tmp = s0;
        s0 = s1;
        s1 = tmp;
    }else if(!s0->tl && !s1->tl){
        s0 = Op2_Bind(
            s0->format, 
            Temp_TempList(
                R_munchExp(e0),
                NULL
            )
        );
    }
    return Src2_Bind(s0, s1);

}


static const int SingleParam_Length = 20;
const string R_T_expList_to_string(T_expList el, Temp_tempList tl, int st, string mode){
    assert(tl);
    Temp_tempList last = tl;
    Temp_tempList hd = tl;
    int params_cnt = 0;
    for(T_expList now = el; now; now = now->tail) params_cnt++;
    string ret = NULL;
    if(params_cnt > 0){
        ret = checked_malloc(params_cnt * SingleParam_Length);
        for(T_expList now = el; now; now = now->tail){
            string s = checked_malloc(SingleParam_Length);
            switch (now->head->kind)
            {
            case T_TEMP:
            {
                sprintf(s, "`%s%d,", mode, st++);
                tl->head = now->head->u.TEMP;
                tl->tail = Temp_TempList(NULL, NULL);
                last = tl;
                tl = tl->tail;
            }
            break;
            default:
            {
                sprintf(s, "`%s%d,", mode, st++);
                Temp_temp t = R_munchExp(now->head);
                tl->head = t;
                tl->tail = Temp_TempList(NULL, NULL);
                last = tl;
                tl = tl->tail;
            }
            break;
            }
            strcat(ret, s);
        }
        if(last->head != NULL){
            assert(last->tail->head == NULL);
            last->tail = NULL;
        }
        // fprintf(stderr, "%s\n", ret);
        ret[strlen(ret) - 1] = '\0';
    }else{
        ret = "";
    }
    return ret;
}



void handle_ext_call_args(T_expList args){
    int reg_num = 0;
    T_expList now = NULL;
    string inst = NULL;
    for(now = args; now && reg_num < 4; now = now->tail, reg_num++){
        inst = checked_malloc(R_INST_MAX_SIZE);
        Op2_bind ob = getOp2Type(now->head, 0);
        sprintf(
            inst,
            MOV(%s),
            ob->format
        );
        string reg_name = checked_malloc(10);
        sprintf(reg_name, "t%d", reg_num);
        if(ob->tl){
            R_emit(
                AS_Move(
                    inst,
                    Temp_TempList(TC(reg_name), NULL),
                    ob->tl
                )
            );
        }else{
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(TC(reg_name), NULL),
                    ob->tl,
                    NULL
                )
            );
        }
    }
    for(; now; now = now->tail){
        inst = checked_malloc(R_INST_MAX_SIZE);
        Op2_bind ob = getOp2Type(now->head, 0);
        sprintf(inst, PUSH(`s0));
        if(!ob->tl){
            ob->tl = Temp_TempList(Temp_newtemp(), NULL);
            string save_in_reg = checked_malloc(R_INST_MAX_SIZE);
            sprintf(save_in_reg, MOV(%s), ob->format);
            R_emit(AS_Oper(save_in_reg, ob->tl, NULL, NULL));
        }
        R_emit(AS_Oper(inst, NULL, ob->tl, NULL));
    }
}


void handle_call_args(T_expList args){
    int reg_num = 0;
    T_expList now = NULL;
    string inst = NULL;

    // 传参
    for(now = args; now && reg_num < 4; now = now->tail, reg_num++){
        inst = checked_malloc(R_INST_MAX_SIZE);
        Op2_bind ob = getOp2Type(now->head, 0);
        sprintf(
            inst,
            MOV(%s),
            ob->format
        );
        string reg_name = checked_malloc(10);
        sprintf(reg_name, "t%d", reg_num);
        if(ob->tl){
            R_emit(
                AS_Move(
                    inst,
                    Temp_TempList(TC(reg_name), NULL),
                    ob->tl
                )
            );
        }else{
            R_emit(
                AS_Oper(
                    inst,
                    Temp_TempList(TC(reg_name), NULL),
                    ob->tl,
                    NULL
                )
            );
        }
    }

    for(; now; now = now->tail){
        inst = checked_malloc(R_INST_MAX_SIZE);
        Op2_bind ob = getOp2Type(now->head, 0);
        sprintf(inst, PUSH(`s0));
        if(!ob->tl){
            ob->tl = Temp_TempList(Temp_newtemp(), NULL);
            string save_in_reg = checked_malloc(R_INST_MAX_SIZE);
            sprintf(save_in_reg, MOV(%s), ob->format);
            R_emit(AS_Oper(save_in_reg, ob->tl, NULL, NULL));
        }
        R_emit(AS_Oper(inst, NULL, ob->tl, NULL));
    }
}

// !
// static int callee_save_buf;
void callee_save(){
    for(int i = 4; i <= 10; i++){
        string callee_save_reg = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            callee_save_reg,
            MOV(`s0)
        );
        string reg_id = checked_malloc(10);
        sprintf(reg_id, "t%d", i);
        string new_reg_id = checked_malloc(10);
        sprintf(new_reg_id, "t%d", 100 + i - 4);
        R_emit(
            AS_Move(
                callee_save_reg,
                Temp_TempList(TC(new_reg_id), NULL),
                Temp_TempList(TC(reg_id), NULL)
            )
        );
    }
}
void callee_get(){
    for(int i = 4; i <= 10; i++){
        string callee_save_reg = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            callee_save_reg,
            MOV(`s0)
        );
        string reg_id = checked_malloc(10);
        sprintf(reg_id, "t%d", i);
        string new_reg_id = checked_malloc(10);
        sprintf(new_reg_id, "t%d", 100 + i - 4);
        R_emit(
            AS_Move(
                callee_save_reg,
                Temp_TempList(TC(reg_id), NULL),
                Temp_TempList(TC(new_reg_id), NULL)
            )
        );
    }
}
void caller_save(){
    const int caller_saved_reg[] = {0,1,2,3,14};
    for(int i = 0; i < 5; i++){
        string caller_save_reg = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            caller_save_reg,
            MOV(`s0)
        );
        string reg_id = checked_malloc(10);
        sprintf(reg_id, "t%d", caller_saved_reg[i]);
        string new_reg_id = checked_malloc(10);
        sprintf(new_reg_id, "t%d", 80 + i);
        R_emit(
            AS_Move(
                caller_save_reg,
                Temp_TempList(TC(new_reg_id), NULL),
                Temp_TempList(TC(reg_id), NULL)
            )
        );
    }
}
void caller_get(){
    const int caller_saved_reg[] = {0,1,2,3,14};
    for(int i = 0; i < 5; i++){
        string caller_save_reg = checked_malloc(R_INST_MAX_SIZE);
        sprintf(
            caller_save_reg,
            MOV(`s0)
        );
        string reg_id = checked_malloc(10);
        sprintf(reg_id, "t%d", caller_saved_reg[i]);
        string new_reg_id = checked_malloc(10);
        sprintf(new_reg_id, "t%d", 80 + i);
        R_emit(
            AS_Move(
                caller_save_reg,
                Temp_TempList(TC(reg_id), NULL),
                Temp_TempList(TC(new_reg_id), NULL)
            )
        );
    }
}



Temp_tempList R_mergeTempList(Temp_tempList tl1, Temp_tempList tl2){
    if(!tl1){
        return tl2;
    }
    if(!tl2){
        return tl1;
    }
    Temp_tempList last = NULL;
    for(Temp_tempList now = tl1; now; now = now->tail){
        last = now;
    }
    last->tail = tl2;
    return tl1;
}

void R_checkTempListSafety(Temp_tempList tl){
    for(Temp_tempList now = tl; now; now = now->tail){
        assert(now);
    }
}