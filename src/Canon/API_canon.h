#pragma once

#include "../Utils/treep.h"
#include "canon.h"

struct C_block canonFunc(T_funcDecl f);

void printCanon(T_funcDeclList fl);

void print_C_block(FILE* f, struct C_block c);