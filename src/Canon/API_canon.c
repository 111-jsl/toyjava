#include <stdio.h>
#include <string.h>
#include "../Utils/util.h"
#include "../Utils/symbol.h"
#include "../Utils/temp.h"
#include "../Utils/treep.h"
#include "canon.h"
#include "printtreep.h"
#include "../Utils/pr_tree_readable.h"
#include "pr_linearized.h"
#include "API_canon.h"


struct C_block canonFunc(T_funcDecl f)
{
  T_stm s = f->stm;
  T_stmList sl = C_linearize(s);
  struct C_block c = C_basicBlocks(sl);
  // T_stmList ret = C_traceSchedule(c);
  return c;
}


void printCanon(T_funcDeclList fl)
{
    T_stm s;

    while (fl) {
      fprintf(stdout, "Original IR Tree:\n");
      s=fl->head->stm;
      
	    printFuncDeclList(stdout, T_FuncDeclList(fl->head, NULL));
      
      T_stmList sl = C_linearize(s);
      fprintf(stdout, "\nLinearized IR Tree:\n");
      printStmList_linearized(stdout, sl, 0);
      fprintf(stdout, "\n");
    
      struct C_block c = C_basicBlocks(sl);

      printf("How It's Broken Up:\n");
      for (C_stmListList sList=c.stmLists; sList; sList=sList->tail) {
          fprintf(stdout, "\nFor Label=%s\n", S_name(sList->head->head->u.LABEL));
      printStmList_linearized(stdout, sList->head, 0);
      }

      fprintf(stdout, "\n\nThe Final Canonical Tree:\n");
      printStmList_linearized(stdout, C_traceSchedule(c), 0);
      
      fl=fl->tail;
    }

    fprintf(stdout, "\n");

}

void print_C_block(FILE* f, struct C_block c){
  fprintf(f, "How It's Broken Up:\n");
  for (C_stmListList sList=c.stmLists; sList; sList=sList->tail) {
      fprintf(f, "\nFor Label=%s\n", S_name(sList->head->head->u.LABEL));
  printStmList_linearized(f, sList->head, 0);
  }
}