#pragma once
/* function prototype from printtree.c */
#include "../Utils/treep.h"

void printStmList (FILE*, T_stmList, int);
void printFuncDeclList(FILE*, T_funcDeclList);
void printFuncDecl(FILE*, T_funcDecl);

