#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "arch_except.h"
#include "reg_coal.h"
#include "reg_define.h"
#include "../TIR_to_RPI/TR_define.h"

void print_ig_nodelist(G_nodeList nl){
    for(G_nodeList ns = nl; ns; ns = ns->tail){
        Temp_temp t = G_nodeInfo(ns->head);
        fprintf(stderr, "%s ", Temp_look(Temp_name(), t));
    }
    fprintf(stderr, "\n");
}

void print_ig(G_nodeList ig){
    fprintf(stderr, "------------ig-------------\n");
    for(G_nodeList now = ig; now; now = now->tail){
        Temp_temp t = G_nodeInfo(now->head);
        fprintf(stderr, "%s: ", Temp_look(Temp_name(), t));
        print_ig_nodelist(G_succ(now->head));
    }
    fprintf(stderr, "---------------------------\n");

}


void handle_arch_except(G_nodeList ig, AS_instrList il){

    // 修补ig
    for(G_nodeList now = ig; now; now = now->tail){
        now->head->succs = merge_nodelist(now->head->succs, now->head->preds);
        now->head->preds = deepcopy_nodelist(now->head->succs);
    }

    // 检查ig
    for(G_nodeList now = ig; now; now = now->tail){
        for(G_nodeList ns = G_succ(now->head); ns; ns = ns->tail){
            assert(G_inNodeList(ns->head, G_pred(now->head)));
        }
        for(G_nodeList np = G_pred(now->head); np; np = np->tail){
            assert(G_inNodeList(np->head, G_succ(now->head)));
        }
    }

    // print_ig(ig);
    
    
//120: 150 149 151 148 100 102 104 106 11 105 103 101

    // 某些指令dst和src不能相同，需要加边
    // mul, sdiv
    get_t2n_table(ig);
    // for(AS_instrList now = il; now; now = now->tail){
    //     if(now->head->kind == I_OPER){
    //         string assem = now->head->u.OPER.assem;
    //         Temp_tempList dsts = now->head->u.OPER.dst;
    //         Temp_tempList srcs = now->head->u.OPER.src;
    //         if(!strncmp(assem, "mul", 3) || !strncmp(assem, "sdiv", 4)){
    //             Temp_temp d0 = dsts->head;
    //             Temp_temp s0 = srcs->head;
    //             G_node nd = map_t2n(d0);
    //             G_node ns = map_t2n(s0);
    //             G_addEdge(nd, ns);
    //             G_addEdge(ns, nd);
            
    //         }

    //     }
    // }
    // fp, sp 不参与ig
    G_node fp_n = map_t2n(REG_FP);
    G_node sp_n = map_t2n(REG_SP);
    // assert(fp_n && sp_n);
    // print_ig_nodelist(G_succ(fp_n));

    // print_ig_nodelist(G_succ(sp_n));
    if(fp_n){
        for(G_nodeList n_succ = G_succ(fp_n); n_succ; n_succ = n_succ->tail){
            G_rmEdge(n_succ->head, fp_n);
            // fprintf(stderr, "aha\n");
            
            G_rmEdge(fp_n, n_succ->head);
        }
    }
    if(sp_n){
        for(G_nodeList n_succ = G_succ(sp_n); n_succ; n_succ = n_succ->tail){
            // print_ig_nodelist(G_succ(n_succ->head));
            G_rmEdge(n_succ->head, sp_n);
            // fprintf(stderr, "aha\n");
            G_rmEdge(sp_n, n_succ->head);
        }
    }
    // fprintf(stderr, "arch_except pass\n");
}
