#pragma once
#include "../cfg/graph.h"
#include "../Utils/util.h"
#include "../Utils/util.h"
#include "../Utils/temp.h"
#include "../Utils/table.h"

G_nodeList deepcopy_nodelist(G_nodeList x);
G_nodeList merge_nodelist(G_nodeList x, G_nodeList y);


typedef struct TAB_table_ *color_table;
color_table color_empty();
void color_enter(color_table t, G_node n, void *value);
void *color_look(color_table t, G_node n);

typedef struct TAB_table_ *edge_table;
edge_table edge_empty();
void edge_enter(edge_table t, G_node n, void *value);
void *edge_look(edge_table t, G_node n);
bool edge_stack_empty();
void *edge_pop(edge_table t);

typedef struct TAB_table_ *coal_table;
coal_table coal_empty();
void coal_enter(coal_table t, G_node n, void *value);
void *coal_look(coal_table t, G_node n);

typedef struct TAB_table_ *mov_table;
mov_table mov_empty();
void mov_enter(mov_table t, G_node n, void *value);
void *mov_look(mov_table t, G_node n);

typedef struct TAB_table_ *t2n_table;
t2n_table t2n_empty();
void t2n_enter(t2n_table t, Temp_temp tmp, void *value);
void *t2n_look(t2n_table t, Temp_temp tmp);

typedef struct TAB_table_ *spill_table;
spill_table spill_empty();
void spill_enter(spill_table t, Temp_temp tmp, void *value);
void *spill_look(spill_table t, Temp_temp tmp);