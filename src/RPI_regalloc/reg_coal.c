#include <stdio.h>
#include <assert.h>
#include "reg_coal.h"
#include "reg_define.h"
#include "../Utils/assem.h"

static int *mov_fa = NULL;
static int *mov_sz = NULL;
static G_node *mov_map = NULL;
static mov_table m_table = NULL;
static mov_table mov_Table(){
    if(!m_table){
        m_table = mov_empty();
    }
    return m_table;
}
void mov_clear(){
    if(m_table)
        TAB_free(m_table);
    if(mov_fa)
        checked_free(mov_fa);
    if(mov_sz)
        checked_free(mov_sz);
    if(mov_map)
        checked_free(mov_map);
    mov_fa = NULL;
    mov_sz = NULL;
    mov_map = NULL;
    m_table = NULL;
}


static void mov_init(G_nodeList ig){
    int n = G_nodecount(ig->head);
    // fprintf(stderr, "init n: %d\n", n);
    mov_fa = checked_malloc(sizeof(*mov_fa) * n);
    mov_sz = checked_malloc(sizeof(*mov_sz) * n);
    mov_map = checked_malloc(sizeof(*mov_map) * n);
    for(int i = 0; i < n; i++) mov_fa[i] = -1;
    for(G_nodeList now = ig; now; now = now->tail){
        mov_map[G_mykey(now->head)] = now->head;
    }
}

static int mov_find_root(int r){
    // fprintf(stderr, "findroot_now: %d\n", r);
    assert(mov_fa && mov_sz);
    if(mov_fa[r] == -1) return r;
    int p = mov_find_root(mov_fa[r]);
    mov_fa[r] = p;
    return p;
}
static void mov_union(int x, int y){
    // fprintf(stderr, "x: %d\n", x);
    // fprintf(stderr, "y: %d\n", y);
    assert(mov_fa && mov_sz);
    int rx = mov_find_root(x);
    int ry = mov_find_root(y);
    if(rx == ry) return;
    if(mov_sz[rx] > mov_sz[ry]){
        mov_fa[ry] = rx;
        mov_sz[rx] = mov_sz[rx] >= mov_sz[ry] + 1 ? mov_sz[rx]: mov_sz[ry] + 1;
    }else{
        mov_fa[rx] = ry;
        mov_sz[ry] = mov_sz[ry] >= mov_sz[rx] + 1 ? mov_sz[ry]: mov_sz[rx] + 1;
    }
}



static t2n_table t_table = NULL;
static t2n_table t2n_Table(){
    if(!t_table){
        t_table = t2n_empty();
    }
    return t_table;
}
void t2n_clear(){
    if(t_table)
        checked_free(t_table);
    t_table = NULL;
}



bool Brigg_strategy(G_node x, G_node y, int k){
    G_nodeList x_succ = G_succ(x);
    G_nodeList y_succ = G_succ(y);
    int more_k_neigh_num = 0;
    G_nodeList xy_succ = merge_nodelist(x_succ, y_succ);
    for(G_nodeList now = xy_succ; now; now = now->tail){
        if(G_degree(now->head) / 2 >= k){
            more_k_neigh_num++;
        }
    }
    return more_k_neigh_num < k;
}

bool George_strategy(G_node x, G_node y, int k){
    G_nodeList x_succ = G_succ(x);
    for(G_nodeList now = x_succ; now; now = now->tail){
        if(!G_goesTo(now->head, y) && G_degree(now->head) / 2 >= k){
            return FALSE;
        }
    }
    return TRUE;
}


void reg_coal(G_node x, G_node y){
    for(G_nodeList now = G_succ(y); now; now = now->tail){
        G_rmEdge(now->head, y);
        G_rmEdge(y, now->head);
        G_addEdge(now->head, x);
        G_addEdge(x, now->head);
    }
}

void get_t2n_table(G_nodeList ig){
    if(t_table) return;
    for(G_nodeList now = ig; now; now = now->tail){
        Temp_temp t = G_nodeInfo(now->head);
        // fprintf(stderr, "%s\n", Temp_look(Temp_name(), t));
        assert(!t2n_look(t2n_Table(), t));
        t2n_enter(t2n_Table(), t, now->head);
    }
}



void get_mov_table(AS_instrList il, G_nodeList ig){
    if(m_table) return;
    get_t2n_table(ig);
    mov_init(ig);
    // fprintf(stderr, "mov init pass\n");
    for(AS_instrList now = il; now; now = now->tail){
        assert(now && now->head);
        if(now->head->kind == I_MOVE){
            assert(now->head->u.MOVE.dst && now->head->u.MOVE.src);
            Temp_temp dst = now->head->u.MOVE.dst->head;
            Temp_temp src = now->head->u.MOVE.src->head;
            G_node dn = t2n_look(t2n_Table(), dst);
            G_node sn = t2n_look(t2n_Table(), src);
            if(!dn || !sn) continue;
            assert(dn && sn);
            int did = G_mykey(dn);
            int sid = G_mykey(sn);
            mov_union(did, sid);
        }
    }
    int n = G_nodecount(ig->head);
    // fprintf(stderr, "n: %d\n", n);
    // for(int i = 0; i < n; i++){
    //     fprintf(stderr, "%d\n", G_mykey(mov_map[i]));
    // }
    // mov_Table();
    // TAB_empty();
    // fprintf(stderr, "aha\n");
    for(int i = 0; i < n; i++){
        mov_enter(mov_Table(), mov_map[i], G_NodeList(mov_map[i], NULL));
    }
    // fprintf(stderr, "union set pass\n");

    for(int i = 0; i < n; i++){
        int fa = mov_find_root(i);
        G_nodeList nl = mov_look(mov_Table(), mov_map[fa]);
        assert(nl);
        nl->tail = G_NodeList(mov_map[i], nl->tail);
    }
}


G_nodeList have_mov(G_node x){
    int xfa = mov_find_root(G_mykey(x));
    return mov_look(mov_Table(), mov_map[xfa]);
}

G_node map_t2n(Temp_temp t){
    return t2n_look(t2n_Table(), t);
}