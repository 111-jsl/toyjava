#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "reg_alloc.h"
#include "reg_define.h"
#include "reg_coal.h"
#include "../TIR_to_RPI/TR_define.h"
#include "../Utils/stack.h"
#include "../Utils/temp.h"
#include "../AST_to_TIR/AT_define.h"
#include "arch_except.h"
// const
static const int k = 9;
static const int flag_moved = 1;
static const int flag_null = 2;
// 尽可能分配 r0-3, r14, 因为r4-10是callee saved registers
static const int legal_reg_id[] = {0,1,2,3,14,4,5,6,7};
static const int spill_reg_id[] = {8,9,10};
static const int spill_reg_n = 3;

extern int UNIT_SIZE;
extern int ELE_SIZE;


void show_color(G_node k, int id){

    fprintf(stderr, "kaddr: %p\tn: %s\tcolor: %d\n", k, Temp_look(Temp_name(), G_nodeInfo(k)), id);
}

static color_table c_table = NULL;
static color_table color_Table(){
    if(!c_table){
        c_table = color_empty();
    }
    return c_table;
}
static void color_clear(){
    if(c_table)
        TAB_free(c_table);
    c_table = NULL;
}

static coal_table co_table = NULL;
static coal_table coal_Table(){
    if(!co_table){
        co_table = coal_empty();
    }
    return co_table;
}
static void coal_clear(){
    if(co_table)
        TAB_free(co_table);
    co_table = NULL;
}


static edge_table e_table = NULL;
static edge_table edge_Table(){
    if(!e_table){
        e_table = edge_empty();
    }
    return e_table;
}
static void edge_clear(){
    if(e_table)
        TAB_free(e_table);
    e_table = NULL;
}


static spill_table sp_table = NULL;
static spill_table spill_Table(){
    if(!sp_table){
        sp_table = spill_empty();
    }
    return sp_table;
}
static void spill_clear(){
    if(sp_table)
        TAB_free(sp_table);
    sp_table = NULL;
}


static void remove_from_ig(G_node n){
    // fprintf(stderr, "remove from ig: %s\n", Temp_look(Temp_name(), G_nodeInfo(n)));
    G_nodeList es = deepcopy_nodelist(G_succ(n));
    if(es){
        // print_ig_nodelist(es);
        edge_enter(edge_Table(), n, es);
    }else{
        edge_enter(edge_Table(), n, flag_null);
    }
    for(G_nodeList n_succ = G_succ(n); n_succ; n_succ = n_succ->tail){
        // fprintf(stderr, "remove edge target: %s\n", Temp_look(Temp_name(), G_nodeInfo(n_succ->head)));
        G_rmEdge(n_succ->head, n);
        // fprintf(stderr, "aha\n");
        G_rmEdge(n, n_succ->head);
    }
}//126: 

static void add_to_ig(G_node n, G_nodeList e){
    for(G_nodeList nl = e; nl; nl = nl->tail){
        G_addEdge(nl->head, n);
        G_addEdge(n, nl->head);

    }
}


static bool try_mov(G_node n){
    G_nodeList mov_regs = have_mov(n);
    for(G_nodeList mov_reg = mov_regs; mov_reg; mov_reg = mov_reg->tail){
        G_node reg_node = mov_reg->head;
        // 1. 如果相同
        // 2. 如果对应的reg已经移除
        // 3. 如果两个节点有边
        if(n == reg_node
        || edge_look(edge_Table(), reg_node)
        || G_goesTo(n, reg_node)){
            continue;
        }
        if(Brigg_strategy(n, reg_node, k) 
        || George_strategy(n, reg_node, k)){
            // 把所有边给当前节点
            reg_coal(reg_node, n);
            // 恢复时按合并的情况恢复
            // edge_enter(
            //     edge_Table(), reg_node, 
            //     merge_nodelist(G_succ(n), G_succ(reg_node))
            // );
            edge_enter(edge_Table(), n, flag_moved);
            coal_enter(coal_Table(), n, reg_node);
            return TRUE;
        }
    }
    return FALSE;
}





void reg_alloc(G_nodeList ig, AS_instrList il){
    

    // 获取所有的mov关系   
    get_mov_table(il, ig);
    // fprintf(stderr, "get mov pass\n");

    // precolor
    int precolored_n = 0;
    int tot_n = 0;
    for(G_nodeList now = ig; now; now = now->tail){
        tot_n++;
        Temp_temp t = G_nodeInfo(now->head);
        int tid = atoi(Temp_look(Temp_name(), t));
        if(tid < 80){
            precolored_n++;
            // fprintf(stderr, "precolor: %d\n", tid);
            color_enter(color_Table(), now->head, tid+1);
        }
    }
    // fprintf(stderr, "precolor pass\n");
    // TAB_dump(color_Table(), show_color);

    //遍历n遍，找到 deg < k的节点，去掉
    while(tot_n > precolored_n){
        bool has_lt_k = FALSE;
        bool can_coal = FALSE;
        bool should_spill = TRUE;
        for(G_nodeList now = ig; now; now = now->tail){
            // 1. 如果已经着色
            // 2. 如果已经去掉
            // 3. 如果mov相关
            if(color_look(color_Table(), now->head)
            || edge_look(edge_Table(), now->head)
            || have_mov(now->head)->tail != NULL) 
                continue;

            // 若 deg < k，去掉
            if(G_degree(now->head) / 2 < k){
                has_lt_k = TRUE;
                remove_from_ig(now->head);
                tot_n--;
            }
        }
        // fprintf(stderr, "less than k remove pass\n");

        if(!has_lt_k){
            for(G_nodeList now = ig; now; now = now->tail){
                // 1. 如果已经着色
                // 2. 如果已经去掉
                if(color_look(color_Table(), now->head)
                || edge_look(edge_Table(), now->head)) 
                    continue;

                // 看能不能mov合并
                if(try_mov(now->head)){
                    tot_n--;
                    can_coal = TRUE;
                }
                
            }
        }
        // fprintf(stderr, "try mov pass\n");

        if(!has_lt_k && !can_coal){
            for(G_nodeList now = ig; now; now = now->tail){
                // 1. 如果已经着色
                // 2. 如果已经去掉
                if(color_look(color_Table(), now->head)
                || edge_look(edge_Table(), now->head)) 
                    continue;

                // 若 deg < k，去掉
                if(G_degree(now->head) / 2 < k){
                    should_spill = FALSE;
                    remove_from_ig(now->head);
                    tot_n--;
                }
            }
        }
        // fprintf(stderr, "remove mov pass\n");
        

        // 若没有 deg < k的，考虑去掉 deg >= k的，并且暂定为spill
        if(!has_lt_k && !can_coal && should_spill){
            // 选择一个最大的并且没有precolored的
            G_node max_node = NULL;
            G_nodeList now = ig;
            for(; now; now = now->tail){
                if(!color_look(color_Table(), now->head)){
                    max_node = now->head;
                    break;
                }
            }
            assert(max_node);
            for(; now; now = now->tail){
                if(!color_look(color_Table(), now->head)
                && G_degree(now->head) > G_degree(max_node)){
                    max_node = now->head;
                }
            }
            // 去掉
            remove_from_ig(max_node);
            tot_n--;
        }
        // fprintf(stderr, "more than k pass\n");

    }
//110: 181 178 180 179 177 175 176 172 174 173 171 170 169 168 167 166 165 115 161 162 164 163 114 150 160 159 158 157 113 153 14 3 2 1 0 154 156 155 112 151 152 111
    // fprintf(stderr, "iteration pass\n");
    // 把去掉的节点倒着插回来，确定暂定为spill的到底是不是spill
    int real_spill_n = 0;
    int *adj_color = checked_malloc(sizeof(int) * 20);
    while(!edge_stack_empty()){
        // fprintf(stderr, "add back start\n");
        G_nodeList neigh_pack = edge_pop(edge_Table());
        // fprintf(stderr, "add back pop\n");
        for(int i = 0; i < 20; i++) adj_color[i] = 0;
        G_node node = neigh_pack->head;
        G_nodeList neigh = neigh_pack->tail;
        // fprintf(stderr, "add back node: %s\n", Temp_look(Temp_name(), G_nodeInfo(node)));
        assert(neigh);
        if(neigh == flag_null) neigh = NULL;
        if(neigh != flag_moved){
            for(G_nodeList now = neigh; now; now = now->tail){
                G_nodeList n_es = edge_look(edge_Table(), now->head);
                // assert(n_es != flag_null);
                // if(n_es == flag_null){
                //     fprintf(stderr, "flag null: %s\n", Temp_look(Temp_name(), G_nodeInfo(now->head)));
                // }
                if(n_es != flag_moved){
                    int cid = color_look(color_Table(), now->head);
                    if(cid)
                        adj_color[cid - 1] = 1;
                }else{
                    G_node move_target = coal_look(coal_Table(), now->head);
                    assert(move_target);
                    int cid = color_look(color_Table(), move_target);
                    assert(cid);
                    if(cid)
                        adj_color[cid - 1] = 1;
                    
                }
            }
            // fprintf(stderr, "adj of %s: ", Temp_look(Temp_name(), G_nodeInfo(node)));
            // for(int i = 0; i < 20; i++) fprintf(stderr, "%d ", adj_color[i]);
            // fprintf(stderr, "\n");
            bool is_spill = TRUE;
            for(int i = 0; i < k; i++){
                if(!adj_color[legal_reg_id[i]]){
                    is_spill = FALSE;
                    color_enter(color_Table(), node, legal_reg_id[i] + 1);
                    break;
                }
            }
            if(is_spill){
                // fprintf(stderr, "is spill: %s\n", Temp_look(Temp_name(), G_nodeInfo(node)));
                spill_enter(spill_Table(), G_nodeInfo(node), real_spill_n+1);
                real_spill_n++;
            }
            // add_to_ig(node, neigh);
        }else{
            G_node move_target = coal_look(coal_Table(), node);
            assert(move_target);
            // fprintf(stderr, "mov_target: %s\n", Temp_look(Temp_name(), G_nodeInfo(move_target)));
            int cid = color_look(color_Table(), move_target);
            if(!cid){
                int offset = spill_look(spill_Table(), G_nodeInfo(move_target));
                assert(offset);
                spill_enter(spill_Table(), G_nodeInfo(node), offset);
            }else{
                color_enter(color_Table(), node, cid);
            }
        }
    }
    checked_free(adj_color);
    // fprintf(stderr, "add back and color pass\n");
    // TAB_dump(color_Table(), show_color);


    // 把寄存器分配后的结果放进il，如果spill，那么额外处理
    // assert(il);
    AS_instrList last = il;
    assert(last && last->head && last->head->kind == I_OPER);
    for(AS_instrList now = il->tail; now;){
        // fprintf(stderr, "spill start\n");
        AS_instrList n_now = now->tail;
        AS_instrList n_last = now;
        assert(now && now->head);
        switch (now->head->kind)
        {
        case I_OPER:
        {
            // fprintf(stderr, "oper start: %s\n", now->head->u.OPER.assem);
            // 一条指令最多有三个寄存器，多了报错
            int reg_inst_n = spill_reg_n;
            Temp_tempList dsts = now->head->u.OPER.dst;
            Temp_tempList srcs = now->head->u.OPER.src;
            // dst      
            for(Temp_tempList tmp = dsts; tmp; tmp = tmp->tail){
                G_node tnode = map_t2n(tmp->head);
                // 如果tnode为空，说明不在ig里，也就是没边，那直接给r0
                // 要判断一下小于80那就不要动
                int id = 0;
                if(tnode){
                    id = color_look(color_Table(), tnode);
                }else if(atoi(Temp_look(Temp_name(), tmp->head)) >= 100){
                    id = 1;
                }else{
                    id = atoi(Temp_look(Temp_name(), tmp->head)) + 1;
                }

                if(id){
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", id - 1);
                    tmp->head = TC(reg_id);
                }else{
                    // 拿掉一个寄存器
                    assert(reg_inst_n > 0);
                    reg_inst_n--;
                    // 修改原指令
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", spill_reg_id[reg_inst_n]);
                    // 添加存储指令
                    string op_str = checked_malloc(R_INST_MAX_SIZE);
                    // fprintf(stderr, "tmp: %s\n", Temp_look(Temp_name(), tmp));
                    int offset_id = spill_look(spill_Table(), tmp->head);
                    assert(offset_id);
                    sprintf(op_str, STR(#%d), -UNIT_SIZE * (offset_id));
                    now->tail = AS_InstrList(
                        AS_Oper(
                            op_str,
                            NULL,
                            Temp_TempList(
                                TC(reg_id), 
                                Temp_TempList(
                                    REG_FP,
                                    NULL
                                )
                            ),
                            NULL
                        ),
                        now->tail
                    );
                    if(n_last == now) n_last = now->tail;
                    // 修改原指令
                    tmp->head = TC(reg_id);
                }
                
            }
            // src
            for(Temp_tempList tmp = srcs; tmp; tmp = tmp->tail){
                G_node tnode = map_t2n(tmp->head);
                // 如果tnode为空，说明不在ig里，也就是没边，那直接给r0
                // 要判断一下小于80那就不要动
                int id = 0;
                if(tnode){
                    id = color_look(color_Table(), tnode);
                }else if(atoi(Temp_look(Temp_name(), tmp->head)) >= 100){
                    id = 1;
                }else{
                    id = atoi(Temp_look(Temp_name(), tmp->head)) + 1;
                }
                if(id){
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", id - 1);
                    tmp->head = TC(reg_id);
                }else{
                    // 拿掉一个寄存器
                    assert(reg_inst_n > 0);
                    reg_inst_n--;
                    // 修改原指令
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", spill_reg_id[reg_inst_n]);
                    // 添加加载指令
                    string op_ldr = checked_malloc(R_INST_MAX_SIZE);
                    int offset_id = spill_look(spill_Table(), tmp->head);
                    assert(offset_id);
                    sprintf(op_ldr, LDR(#%d), -UNIT_SIZE * (offset_id));
                    last->tail = AS_InstrList(
                        AS_Oper(
                            op_ldr,
                            Temp_TempList(TC(reg_id), NULL),
                            Temp_TempList(REG_FP, NULL),
                            NULL
                        ),
                        last->tail
                    );
                    // 修改原指令
                    tmp->head = TC(reg_id);

                }
                
            }
        }
        break;
        case I_MOVE:
        {
            // fprintf(stderr, "move start\n");

            Temp_tempList dsts = now->head->u.MOVE.dst;
            Temp_tempList srcs = now->head->u.MOVE.src;
            G_node dnode = map_t2n(dsts->head);
            G_node snode = map_t2n(srcs->head);
            int did = 0, sid = 0;
            if(dnode){
                did = color_look(color_Table(), dnode);
            }else if(atoi(Temp_look(Temp_name(), dsts->head)) >= 100){
                did = 1;
            }else{
                did = atoi(Temp_look(Temp_name(), dsts->head)) + 1;
            }
            if(snode){
                sid = color_look(color_Table(), snode);
            }else if(atoi(Temp_look(Temp_name(), srcs->head)) >= 100){
                sid = 1;
            }else{
                sid = atoi(Temp_look(Temp_name(), srcs->head)) + 1;
            }
           
            // TAB_dump(color_Table(), show_color);
            // fprintf(stderr, "saddr: %p\tsrcs: %s\tsid: %d\n", snode, Temp_look(Temp_name(), srcs->head), sid);
            // fprintf(stderr, "daddr: %p\tdsts: %s\tdid: %d\n", dnode, Temp_look(Temp_name(), dsts->head), did);
            if(did && sid && did == sid){
                last->tail = now->tail;
                n_last = last;
            }else{
                if(did){
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", did - 1);
                    dsts->head = TC(reg_id);
                }else{  
                    // 修改原指令
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", spill_reg_id[0]);
                    // 添加存储指令
                    string op_str = checked_malloc(R_INST_MAX_SIZE);
                    int offset_id = spill_look(spill_Table(), dsts->head);
                    assert(offset_id);
                    sprintf(op_str, STR(#%d), -UNIT_SIZE * (offset_id));
                    now->tail = AS_InstrList(
                        AS_Oper(
                            op_str,
                            NULL,
                            Temp_TempList(
                                TC(reg_id), 
                                Temp_TempList(
                                    REG_FP,
                                    NULL
                                )
                            ),
                            NULL
                        ),
                        now->tail
                    );
                    n_last = now->tail;
                    // 修改原指令
                    dsts->head = TC(reg_id);

                }
                if(sid){
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", sid - 1);
                    srcs->head = TC(reg_id);
                }else{
                    // 修改原指令
                    string reg_id = checked_malloc(10);
                    sprintf(reg_id, "t%d", spill_reg_id[1]);
                    // 添加加载指令
                    string op_ldr = checked_malloc(R_INST_MAX_SIZE);
                    // fprintf(stderr, "mov srcs: %s\n", Temp_look(Temp_name(), srcs->head));
                    int offset_id = spill_look(spill_Table(), srcs->head);
                    assert(offset_id);
                    sprintf(op_ldr, LDR(#%d), -UNIT_SIZE * (offset_id));
                    last->tail = AS_InstrList(
                        AS_Oper(
                            op_ldr,
                            Temp_TempList(TC(reg_id), NULL),
                            Temp_TempList(REG_FP, NULL),
                            NULL
                        ),
                        last->tail
                    );
                    // 修改原指令
                    srcs->head = TC(reg_id);
                }
            }

            
        }
        break;
        case I_LABEL:
        {
            // do nothing
        }
        break;
        default:
        assert(0);
        break;
        }
        last = n_last;
        now = n_now;
    }
    for(AS_instrList now = il; now; now = now->tail){
        if(now->head->kind == I_MOVE
        && now->head->u.MOVE.dst->head == REG_FP
        && now->head->u.MOVE.src->head == REG_SP){
            string add_sp = checked_malloc(R_INST_MAX_SIZE);
            sprintf(add_sp, SUB(#%d), real_spill_n * UNIT_SIZE);
            now->tail = AS_InstrList(
                AS_Oper(
                    add_sp,
                    Temp_TempList(REG_SP, NULL),
                    Temp_TempList(REG_SP, NULL),
                    NULL
                ),
                now->tail
            );
        }
    }
    // fprintf(stderr, "rename and spill pass\n");
    color_clear();
    coal_clear();
    edge_clear();
    spill_clear();
    mov_clear();
    t2n_clear();
}