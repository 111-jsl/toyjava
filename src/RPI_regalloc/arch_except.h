#pragma once
#include "../cfg/graph.h"
#include "../Utils/assem.h"

void handle_arch_except(G_nodeList ig, AS_instrList il);
void print_ig_nodelist(G_nodeList nl);
void print_ig(G_nodeList ig);
