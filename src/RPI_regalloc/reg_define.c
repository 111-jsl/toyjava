#include <stdio.h>
#include <assert.h>
#include "reg_define.h"
#include "../Utils/stack.h"
#include "../Utils/util.h"

G_nodeList deepcopy_nodelist(G_nodeList x){
    G_nodeList t = G_NodeList(NULL, NULL);
    G_nodeList ret = t;
    for(G_nodeList now = x; now; now = now->tail){
        t = t->tail = G_NodeList(now->head, NULL);
    }
    return ret->tail;
}

G_nodeList merge_nodelist(G_nodeList x, G_nodeList y){
    if(!x) return y;
    if(!y) return x;
    TAB_table t = TAB_empty();
    for(G_nodeList now = x; now; now = now->tail){
        if(TAB_look(t, now->head)) continue;
        TAB_enter(t, now->head, now->head);
    }
    for(G_nodeList now = y; now; now = now->tail){
        if(TAB_look(t, now->head)) continue;
        TAB_enter(t, now->head, now->head);
    }
    G_nodeList ret = G_NodeList(NULL, NULL);
    G_nodeList hd = ret;
    while(t->top){
        G_node n = TAB_pop(t);
        ret = ret->tail = G_NodeList(n, NULL);
    }
    TAB_free(t);
    return hd->tail;
}







color_table color_empty(){
    return TAB_empty();
}
void color_enter(color_table t, G_node n, void *value){
    TAB_enter(t, n, value);
}
void *color_look(color_table t, G_node n){
    return TAB_look(t, n);
}


static stack del_stack = NULL;

edge_table edge_empty(){
    del_stack = stack_create();
    return TAB_empty();
}
void edge_enter(edge_table t, G_node n, void *value){
    assert(del_stack);
    stack_push(del_stack, n);
    TAB_enter(t, n, value);
}
void *edge_look(edge_table t, G_node n){
    return TAB_look(t, n);
}
bool edge_stack_empty(){
    return !del_stack->top;
}
void *edge_pop(edge_table t){
    assert(del_stack);
    G_node n = stack_pop(del_stack);
    return G_NodeList(n, TAB_look(t, n));
}

coal_table coal_empty(){
    return TAB_empty();
}
void coal_enter(coal_table t, G_node n, void *value){
    TAB_enter(t, n, value);
}
void *coal_look(coal_table t, G_node n){
    return TAB_look(t, n);
}




mov_table mov_empty(){
    return TAB_empty();
}
void mov_enter(mov_table t, G_node n, void *value){
    TAB_enter(t, n, value);
}
void *mov_look(mov_table t, G_node n){
    return TAB_look(t, n);
}

t2n_table t2n_empty(){
    return TAB_empty();
}
void t2n_enter(t2n_table t, Temp_temp tmp, void *value){
    TAB_enter(t, tmp, value);
}
void *t2n_look(t2n_table t, Temp_temp tmp){
    return TAB_look(t, tmp);
}


spill_table spill_empty(){
    return TAB_empty();
}
void spill_enter(spill_table t, Temp_temp tmp, void *value){
    TAB_enter(t, tmp, value);

}
void *spill_look(spill_table t, Temp_temp tmp){
    return TAB_look(t, tmp);

}