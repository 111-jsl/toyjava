#pragma once
#include "../Utils/util.h"
#include "../cfg/graph.h"
#include "../Utils/assem.h"
#include "../Utils/temp.h"

bool Brigg_strategy(G_node x, G_node y, int k);
bool George_strategy(G_node x, G_node y, int k);

void reg_coal(G_node x, G_node y);
void mov_clear();
void t2n_clear();


void get_t2n_table(G_nodeList ig);
void get_mov_table(AS_instrList il, G_nodeList ig);

G_nodeList have_mov(G_node x);
G_node map_t2n(Temp_temp t);