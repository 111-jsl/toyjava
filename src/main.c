#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "fdmjast.h"
#include "Utils/assemblock.h"
#include "Utils/treep.h"


// print
#include "printast.h"
#include "Utils/pr_tree_readable.h"
#include "canon/pr_linearized.h"
// #include 

// code
#include "TypeCheck/mainCheck.h"
#include "AST_to_TIR/mainConvert.h"
#include "Canon/API_canon.h"
#include "TIR_to_LLVM/Tileselect_Main.h"
#include "TIR_to_RPI/R_Tileselect_Main.h"
#include "RPI_regalloc/arch_except.h"
#include "RPI_regalloc/reg_alloc.h"
#include "cfg/API_cfg.h"
#include "LLVM_to_SSA/phi_insert.h"
#include "LLVM_to_SSA/var_rename.h"
#include "LLVM_to_SSA/dead_block.h"
#include "LLVM_to_SSA/degraph.h"


extern int yyparse();
extern FILE *yyin, *yyout;
A_prog program;

void TIR_to_LLVM(T_funcDeclList TIR, const char* target_path, const char* filename){
    // operate on every function
    FILE* OFP = fopen(target_path, "w");
    char* pathbuf = checked_malloc(strlen(filename)+20);
    sprintf(pathbuf, "%s.llvm.stm", filename);
    FILE* stm_out = fopen(pathbuf, "w");
   
    while(TIR){
        // get FunName
        string FunName;
        if(!strcmp(TIR->head->name, "main")){
            FunName = "main";
        }else if(TIR->head->stm->kind == T_LABEL){
            FunName = S_name(TIR->head->stm->u.LABEL);
            TIR->head->stm = NULL;
        }else if(TIR->head->stm->kind == T_SEQ){
            assert(TIR->head->stm->u.SEQ.left->kind == T_LABEL);
            FunName = S_name(TIR->head->stm->u.SEQ.left->u.LABEL);
            TIR->head->stm = TIR->head->stm->u.SEQ.right;
        }

        
        // Canonicalize
        struct C_block canonTIR = canonFunc(TIR->head);
        print_C_block(stm_out, canonTIR);

        // Tiger IR + to LLVM
        AS_blockList bl = munchFunc(canonTIR);
        // AS_instrList il = AS_traceSchedule(
        //     bl, munchFuncProlog(FunName, TIR->head->args),
        //     munchFuncEpilog(canonTIR), FALSE
        // );
        // AS_printInstrList(stderr, il, Temp_name());


        // LLVM to SSA
        G_nodeList bg = Create_bg(bl);
        detach_dead_block(bg);
        phi_insert(bg, TIR->head->args);
        var_rename(bg, TIR->head->args);
        bl = degraph(bg);
        // fprintf(stderr, "\tLLVM to SSA pass\n");

        AS_instrList il = AS_traceSchedule(
            bl, munchFuncProlog(FunName, TIR->head->args),
            munchFuncEpilog(canonTIR), FALSE
        );
        AS_printInstrList(OFP, il, Temp_name());
        // fprintf(stderr, "\tLLVM print completed\n");

        TIR = TIR->tail;
    }
    AS_printInstrList(OFP, getBuildinFunc(), Temp_name());
    fclose(OFP);
    fclose(stm_out);
}

void TIR_to_RPI(T_funcDeclList TIR, const char* target_path, const char* filename){
    // printCanon(TIR);

    // operate on every function
    FILE* OFP = fopen(target_path, "w");
    char* pathbuf = checked_malloc(strlen(filename)+20);
    sprintf(pathbuf, "%s.rpi.stm", filename);
    FILE* stm_out = fopen(pathbuf, "w");
    sprintf(pathbuf, "%s.rpi.liv", filename);
    FILE* liv_out = fopen(pathbuf, "w");
    while(TIR){
        // get FunName
        string FunName;
        if(!strcmp(TIR->head->name, "main")){
            FunName = "main";
        }else if(TIR->head->stm->kind == T_LABEL){
            FunName = S_name(TIR->head->stm->u.LABEL);
            TIR->head->stm = NULL;
        }else if(TIR->head->stm->kind == T_SEQ){
            assert(TIR->head->stm->u.SEQ.left->kind == T_LABEL);
            FunName = S_name(TIR->head->stm->u.SEQ.left->u.LABEL);
            TIR->head->stm = TIR->head->stm->u.SEQ.right;
        }

        // print FunName
        // fprintf(stderr, "%s:\n", FunName);
        
        // Canonicalize
        struct C_block canonTIR = canonFunc(TIR->head);
        // fprintf(stderr, "\tCanonicalize pass\n");
        print_C_block(stm_out, canonTIR);

        // printStmList_readable(stderr, canonTIR.stmLists->head, 0);

        // Tiger IR + to RPI
        AS_blockList bl = R_munchFunc(canonTIR);
        // fprintf(stderr, "\tTiger IR+ to RPI pass\n");

       

        AS_instrList il = AS_traceSchedule(
            bl, R_munchFuncProlog(FunName, TIR->head->args, bl),
            R_munchFuncEpilog(canonTIR), FALSE
        );

         // liveness analysis
        // printCFG(stderrr, il);
        G_nodeList ig = getIG(liv_out, il);
        // Create_ig_Code(stderr, ig);

        // fprintf(stderr, "\tLiveness Analysis pass\n");

        // register allocation
        handle_arch_except(ig, il);
        reg_alloc(ig, il);

        // print into file
        AS_printInstrList(OFP, il, Temp_name());
        // fprintf(stderr, "\tRPI print completed\n");

        TIR = TIR->tail;
    }
    fclose(OFP);
    fclose(stm_out);
    fclose(liv_out);

}



int main(int argc, const char *argv[])
{
    assert(argc == 4);
    const char* source_path = argv[1];
    const char* target_path = argv[2];
    const char* target_language = argv[3];

    // fprintf(stderr, source_path);

    // Parsing
    yyin = fopen(source_path, "r");
    yyparse();
    fclose(yyin);

    // get name
    char* _filename = strtok(source_path, ".");
    char* filename = checked_malloc(strlen(_filename)+3);
    sprintf(filename, "..%s", _filename);
    // fprintf(stderr, filename);
    char* pathbuf = checked_malloc(strlen(filename)+20);

    // print AST
    sprintf(pathbuf, "%s.ast", filename);
    FILE* ast_out = fopen(pathbuf, "w");
    printA_Prog(ast_out, program);
    fclose(ast_out);

    // Type Checking
    if (!checkProg(program))
    {
        fprintf(stderr, "Type Checking not passed, abort\n");
        return 0;
    }
    // fprintf(stderr, "aaha\n");
    // AST to Tiger IR +
    T_funcDeclList TIR = convertProg(program, target_language);
    // fprintf(stderr, "aaha\n");

    // print IR +
    sprintf(pathbuf, "%s.irp", filename);
    FILE* irp_out = fopen(pathbuf, "w");
    printFuncDeclList_readable(irp_out, TIR);
    fclose(irp_out);

    // TIR to target language
    if(!strcmp(target_language, "LLVM")){
        TIR_to_LLVM(TIR, target_path, filename);
    }else{
        TIR_to_RPI(TIR, target_path, filename);
    }
    return 0;
}
