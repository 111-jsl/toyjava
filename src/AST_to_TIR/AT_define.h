#pragma once
#include "../Utils/symbol.h"
#include "../Utils/temp.h"
#include "../Utils/treep.h"

// #define UNIT_SIZE 4
// #define ELE_SIZE 4
#define Class_ThisKey (".this")



typedef struct temp_bind_ *temp_bind;

struct temp_bind_
{
    Temp_temp tmp;
    S_table class_table;
};


temp_bind temp_Bind(Temp_temp tmp, S_table class_table);
string AT_concat(string a, string b);
T_stm checkSeqSafety(T_stm stm);
T_stm Seq_concat(T_stm stm1, T_stm stm2);
