#include <assert.h>
#include "mainConvert.h"
#include "classConvert.h"
#include "../TypeCheck/mainCheck.h"
#include "AT_define.h"
#include "../printast.h"
#include "../Utils/stack.h"

S_table currentScope = NULL;
extern int UNIT_SIZE;
extern int ELE_SIZE;


static S_table MainTable = NULL; // global table
// 帮助break continue 判断是否在while里，是的话跳哪里
static stack while_test = NULL, 
              while_true = NULL,
              while_false = NULL;
static S_table recentClass = NULL;
static Temp_temp recentObj = NULL;

T_funcDeclList convertProg(A_prog prog, string target_language)
{
    if(!prog)
    {
        fprintf(stderr, "Prog NULL\n");
        return NULL;
    }
    
    // ini 
    if(!strcmp(target_language, "LLVM")){
      UNIT_SIZE = ELE_SIZE = 8;
    }else{
      UNIT_SIZE = ELE_SIZE = 4;
    }
    MainTable = S_empty();
    while_test = stack_create();
    while_true = stack_create();
    while_false = stack_create();


    T_funcDeclList Prog = T_FuncDeclList(NULL, NULL);
    getClassOffset(prog->cdl);
    T_funcDeclList Class_Funs = convertClassDeclList(prog->cdl);
    
    T_funcDecl Main = convertMainMethod(prog->m);

    
    if(!Main){
      Prog = Class_Funs;
    }else{
      Prog->head = Main;
      Prog->tail = Class_Funs;
    }




    return Prog;
}

T_funcDecl convertMainMethod(A_mainMethod m)
{
    if(!m)
    {
        fprintf(stderr, "main NULL\n");
        return NULL;
    }
    T_funcDecl Main = T_FuncDecl(String("main"), NULL, NULL);
    T_stm MainSeq = NULL;
    // not serialize for now
    currentScope = MainTable;
    T_stm VarSeq = convertVarDeclList(m->vdl);
    T_stm StmSeq = convertStmList(m->sl);
    
    
    MainSeq = Seq_concat(VarSeq, StmSeq);
    
    Main->stm = MainSeq;
    if(Main->stm == NULL){
      fprintf(stderr, "main NULL\n");
      return NULL;
    }
    return Main;
}


T_stm convertVarDeclList(A_varDeclList vdl)
{
    if(!vdl)
      return NULL;
    T_stm VarSeq = T_Seq(NULL, NULL), tmp = VarSeq;
    T_stm Var = NULL;
    
    A_varDeclList now = vdl;
  
    for(; now; now = now->tail)
    {
        Var = convertVarDecl(now->head);
        if(Var){
          tmp->u.SEQ.right = T_Seq(Var, NULL);
          tmp = tmp->u.SEQ.right;
        }
    }
    
    return checkSeqSafety(VarSeq);
}

T_stm convertVarDecl(A_varDecl vd)
{
    if(!vd)
        return NULL;
    //put into the table
    Temp_temp tmp = Temp_newtemp();
    S_table tmp_c = NULL;
    
    if(vd->t->t == A_idType)
    {
      S_bind c_sbind = S_look(t, S_ClassSymbol(vd->t->id));
      tmp_c = c_sbind->value->u.module.table;
    }

    //move
    T_stm Var = NULL;
    if(vd->elist && vd->elist->head)
    {
      switch (vd->t->t)
      {
      case A_intType:
      {
        Var = T_Move(T_Temp(tmp), convertExp(vd->elist->head));
      }
      break;
      case A_intArrType:
      {
        T_exp right = convertIntConstList(vd->elist);
        Var = T_Move(T_Temp(tmp), right);
      } 
      break;
      default:
          assert(0);
          break;
      }
    }

    S_enter(currentScope, S_Symbol(vd->v), temp_Bind(tmp, tmp_c));
    return Var;
}

//处理条件
// if(a || b || c)
// if(a && b && c)
// 对于or，左边如果为真，那么取整体为真的label；
//            如果为假，那么到右边；
//        右边如果为真，那么取整体为真的label；
//            如果为假，那么取整体为假的label；
// 对于and，左边如果为真，那么到右边；
//             如果为假，那么取整体为假的label；
//         右边如果为真，那么取整体为真的label；
//             如果为假，那么取整体为假的label；
// if((a || (b && c)) || d)   s: T; ns: F
// a || (b && c)              s: T; ns: d
// a                          s: T; ns: bc
// (b && c)                   s: T; ns: d
// b                          s: c; ns: d
// c                          s: T; ns: d
// d                          s: T; ns: F

//what if: 
// # a && (b && c)           s: T; ns: d
// a                          s: bc; ns: d


//what if:
// # b || c                   s: T; ns: 
// b                          s: bc; ns: c
// c                          s: bc; ns: bc


// if(((a || b) && c) && (d || e))   s: F
// if((a || b) && c)
static T_stm if_condition(
  A_exp exp, 
  Temp_label trueLabel, 
  Temp_label falseLabel, 
  Temp_label lastLabel)
{
  T_stm Root = NULL;
  switch (exp->kind)
  {
  case A_opExp:
    switch (exp->u.op.oper)
    {
    case A_and:
    {
      Temp_label rightLabel = Temp_newlabel();
      T_stm left = if_condition(
        exp->u.op.left,
        rightLabel,
        falseLabel,
        NULL
      );
      T_stm right = if_condition(
        exp->u.op.right,
        trueLabel,
        falseLabel,
        rightLabel
      );
      Root = T_Seq(left, right);
    }
    break;
    case A_or:
    {
      Temp_label rightLabel = Temp_newlabel();
      T_stm left = if_condition(
        exp->u.op.left,
        trueLabel,
        rightLabel,
        NULL
      );
      T_stm right = if_condition(
        exp->u.op.right,
        trueLabel,
        falseLabel,
        rightLabel
      );
      Root = T_Seq(left, right);
    }
    break;
    case A_less:
    {
      Root = T_Cjump(T_lt, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_le:
    {
      Root = T_Cjump(T_le, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_greater:
    {
      Root = T_Cjump(T_gt, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_ge:
    {
      Root = T_Cjump(T_ge, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_eq:
    {
      Root = T_Cjump(T_eq, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_ne:
    {
      Root = T_Cjump(T_ne, 
        convertExp(exp->u.op.left),
        convertExp(exp->u.op.right),
        trueLabel,
        falseLabel
      );
    }
    break;
    case A_plus:
    case A_minus:
    case A_times:
    case A_div:
    {
      T_exp leftExp = convertExp(exp);
      Root = T_Cjump(T_ne,
        leftExp,
        T_Const(0),
        trueLabel,
        falseLabel
      );
    }
    break;
    default:
      assert(0);
    break;
    }
  break;
  case A_boolConst:
  {
    Root = T_Cjump(T_ne,
      T_Const(exp->u.b),
      T_Const(0),
      trueLabel,
      falseLabel
    );
  }
  break;
  case A_numConst:
  {
    Root = T_Cjump(T_ne,
      T_Const(exp->u.num),
      T_Const(0),
      trueLabel,
      falseLabel
    );
  }
  break;
  case A_idExp:
  {
    temp_bind tbind = S_look(currentScope, S_Symbol(exp->u.v));
    Temp_temp tmp = tbind->tmp;
    assert(tmp);
    Root = T_Cjump(T_ne,
      T_Temp(tmp),
      T_Const(0),
      trueLabel,
      falseLabel
    );
  }
  break;
  case A_notExp:
  {
    Root = if_condition(exp->u.e, falseLabel, trueLabel, NULL);
  }
  break;
  case A_minusExp:
  {
    Root = if_condition(exp->u.e, trueLabel, falseLabel, NULL);
  }
  break;
  case A_escExp:
  case A_arrayExp:
  case A_callExp:
  case A_classVarExp:
  case A_thisExp:
  case A_lengthExp:
  case A_newIntArrExp:
  case A_newObjExp:
  case A_getint:
  case A_getch:
  case A_getarray:
  {
    Root = T_Cjump(T_ne,
      convertExp(exp),
      T_Const(0),
      trueLabel,
      falseLabel
    );
  }
  break;
  default:
    assert(0);
  break;
  }
  if(lastLabel)
  {
    Root = T_Seq(T_Label(lastLabel), Root);
  }
  return Root;
}


static T_exp Bool_to_Esc(A_exp exp)
{
  // 确保是布尔操作
  assert(exp->kind == A_opExp
      || exp->kind == A_notExp);
  
  // 声明最终结果
  T_exp result = T_Temp(Temp_newtemp());

  //声明标签
  Temp_label if_true = Temp_newlabel();
  Temp_label if_false = Temp_newlabel();
  Temp_label if_end = Temp_newlabel();

  // 建立true语句，false语句
  T_stm true_stm = 
  T_Seq(
    T_Move(result, T_Const(1)),
    T_Jump(if_end)
  );
  T_stm false_stm = T_Move(result, T_Const(0));

  // 建立if模板
  
  T_stm if_stm = T_Seq(
    if_condition(exp, if_true, if_false, NULL),
    T_Seq(
      T_Seq(
        T_Label(if_true),
        true_stm
      ),
      T_Seq(
        T_Seq(
          T_Label(if_false),
          false_stm
        ),
        T_Label(if_end)
      )
    )
  );

  T_exp Esc = T_Eseq(if_stm, result);
  return Esc;
}


T_stm convertStmList(A_stmList sl)
{
    if(!sl)
      return NULL;
    T_stm StmSeq = T_Seq(NULL, NULL);
    T_stm Stm = NULL;

    A_stmList now = sl;
    Stm = convertStm(now->head);
    if(!Stm)
      return NULL;
    StmSeq->u.SEQ.left = Stm;
    now = now->tail;

    T_stm tmp = StmSeq;
    T_stm last = NULL;
    for(; now; now = now->tail)
    {
        Stm = convertStm(now->head);
        if(Stm)
        {
            tmp->u.SEQ.right = T_Seq(Stm, NULL);
            last = tmp;
            tmp = tmp->u.SEQ.right;
        }
    }
    if(!last)
    {
      return StmSeq->u.SEQ.left;
    }else{
      last->u.SEQ.right = tmp->u.SEQ.left;
      return StmSeq;
    }
}

T_stm convertStm(A_stm stm)
{
    if(!stm)
        return NULL;
    T_stm Stm = NULL;
    switch (stm->kind)
    {
    case A_nestedStm:
        Stm = convertStmList(stm->u.ns);
		break;
    case A_ifStm:
    {
        // critical
        //记住三个标签
        Temp_label if_true = Temp_newlabel();
        Temp_label if_false = Temp_newlabel();
        Temp_label if_end = Temp_newlabel();

        //按顺序转换
        T_stm conditionStm = if_condition(stm->u.if_stat.e, if_true, if_false, NULL);
        T_stm trueStm = convertStm(stm->u.if_stat.s1);
        if(trueStm)
        {
          trueStm = T_Seq(trueStm, T_Jump(if_end));
        }else{
          trueStm = T_Jump(if_end);
        }
        T_stm falseStm = convertStm(stm->u.if_stat.s2);
        if(falseStm)
        {
          falseStm = T_Seq(T_Label(if_false), falseStm);
        }else{
          falseStm = T_Label(if_false);
        }

        //建立模板
        Stm = T_Seq(
          conditionStm,
          T_Seq(
            T_Seq(
              T_Label(if_true),
              trueStm
            ),
            T_Seq(
              falseStm,
              T_Label(if_end)
            )
          )
        );  
    }
		break;
    case A_whileStm:
    {
        // critical
        stack_push(while_test, Temp_newlabel());
        stack_push(while_true, Temp_newlabel());
        stack_push(while_false, Temp_newlabel());
        Temp_label wtest = stack_top(while_test);
        Temp_label wtrue = stack_top(while_true);
        Temp_label wfalse = stack_top(while_false);
        assert(wtest && wtrue && wfalse);
        //按顺序转换
        T_stm conditionStm = 
          if_condition(
            stm->u.while_stat.e,
            wtrue,
            wfalse,
            NULL
          );
        T_stm trueStm = convertStm(stm->u.while_stat.s);
        if(trueStm)
        {
          trueStm = T_Seq(trueStm, T_Jump(wtest));
        }else{
          trueStm = T_Jump(wtest);
        }

        // 建立模板
        Stm = T_Seq(
          T_Label(wtest),
          T_Seq(
            conditionStm,
            T_Seq(
              T_Seq(
                T_Label(wtrue),
                trueStm
              ),
              T_Label(wfalse)
            )
          )
        );
  
        stack_pop(while_test);
        stack_pop(while_true);
        stack_pop(while_false);
    }
		break;
    case A_assignStm:
    {
        T_exp arr = convertExp(stm->u.assign.arr);
        T_exp val = convertExp(stm->u.assign.value);
        
        Stm = T_Move(arr, val);
    }
		break;
    case A_arrayInit:
    {
        T_exp arr = convertExp(stm->u.array_init.arr);
        Stm = T_Move(
          arr,
          convertIntConstList(stm->u.array_init.init_values)
        );
    }
		break;
    case A_callStm:
    {
        T_expList params = convertParams(stm->u.call_stat.el);
        T_exp obj = convertExp(stm->u.call_stat.obj);
        assert(recentClass);
        S_bind f_sbind = S_look(
          recentClass,
          S_FunSymbol(stm->u.call_stat.fun)
        );
        assert(f_sbind);
        int offset = f_sbind->offset;
        params = T_ExpList(obj, params);
        Stm = 
        T_Exp(
          T_Call(
            stm->u.call_stat.fun, 
            T_Mem(T_Binop(T_plus, obj, T_Const(offset))), 
            params
          )
        );
    }
		break;
    case A_continue:
    {
        // critical
        Temp_label wtest = stack_top(while_test);
        assert(wtest);
        Stm = T_Jump(wtest);
    }
		break;
    case A_break:
    {
        // critical
        Temp_label wfalse = stack_top(while_false);
        assert(wfalse);
        Stm = T_Jump(wfalse);
    }
		break;
    case A_return:
    {
        // critical
        Stm = T_Return(convertExp(stm->u.e));
    }
		break;
    case A_putint:
        Stm = T_Exp(T_ExtCall("putint", T_ExpList(convertExp(stm->u.e), NULL)));
		break;
    case A_putarray:
        Stm = T_Exp(T_ExtCall("putarray", 
        T_ExpList(
          convertExp(stm->u.putarray.e1), 
          T_ExpList(
            T_Binop(T_plus, convertExp(stm->u.putarray.e2), T_Binop(T_mul, T_Const(1), T_Const(UNIT_SIZE))), 
            NULL))));
		break;
    case A_putch:
        Stm = T_Exp(T_ExtCall("putch", T_ExpList(convertExp(stm->u.e), NULL)));
		break;
    case A_starttime:
        Stm = T_Exp(T_ExtCall("starttime", NULL));
		break;
    case A_stoptime:
        Stm = T_Exp(T_ExtCall("stoptime", NULL));
		break;
    
    default:
        assert(0);
        break;
    }
    return Stm;
}

T_exp convertIntConstList(A_expList elist)
{
    if(!elist)
        return NULL;
    // if(!elist->tail)
    // {
    //   return convertExp(elist->head);
    // }

    Temp_temp temp = Temp_newtemp();
    int offset = ELE_SIZE;
    T_stm stm = 
      T_Move(
        T_Mem(
          T_Binop(T_plus, T_Temp(temp), T_Const(offset))
        ),
        convertExp(elist->head)
      );
    T_stm ret = T_Seq(stm, NULL);
    T_stm last = NULL;
    stm = ret;
    offset += ELE_SIZE;
    for(A_expList now = elist->tail; now; now = now->tail)
    {
        assert(now->head->kind == A_numConst);
        last = stm;
        stm->u.SEQ.right = 
          T_Seq( 
            T_Move(
              T_Mem(
                T_Binop(T_plus, T_Temp(temp), T_Const(offset))
              ),
              convertExp(now->head)
            ),
            NULL
          );
        stm = stm->u.SEQ.right;
        offset += ELE_SIZE;
    }
    if(last)
    {
      last->u.SEQ.right = last->u.SEQ.right->u.SEQ.left;
    }else{
      ret = ret->u.SEQ.left;
    }
    T_stm alloc = T_Move(
      T_Temp(temp), 
      T_ExtCall("malloc", 
        T_ExpList(T_Const(offset / ELE_SIZE * UNIT_SIZE), NULL))
    );
    ret = T_Seq(
      T_Move(
        T_Mem(T_Temp(temp)),
        T_Const(offset / ELE_SIZE - 1)
      ),
      ret
    );
    ret = T_Seq(alloc, ret);
    
    return T_Eseq(ret, T_Temp(temp));
}

static T_stm assignNewClass_stm = NULL;
static T_stm assignNewClass_tmp = NULL;
static T_exp assignNewClass_object = NULL;
static S_symbol assignNewClass_name = NULL;
static int offset_count = 0;
static void assignNewClass(S_symbol sym, S_bind bind)
{
  assert(assignNewClass_tmp && assignNewClass_object && assignNewClass_name);
  if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
    return;
  offset_count += UNIT_SIZE;
  // fprintf(stderr, "%s: %d\n", S_name(sym), bind->offset);
  if(bind->value->kind == Ty_function)
  {
    assignNewClass_tmp->u.SEQ.right = 
      T_Seq(T_Move(
          T_Mem(
            T_Binop(T_plus, assignNewClass_object, T_Const(bind->offset))
          ),
          T_Name(S_Symbol(AT_concat(S_name(sym), S_name(bind->value->u.function.class))))
        ), NULL);
    assignNewClass_tmp = assignNewClass_tmp->u.SEQ.right;
  }else if(bind->ast_node){
    T_exp init_exp = NULL;
    if(bind->value->kind == Ty_int){
      A_expList ast_node = bind->ast_node;
      init_exp = convertExp(ast_node->head);
    }else if(bind->value->kind == Ty_array){
      init_exp = convertIntConstList(bind->ast_node);
    }else{
      assert(0);
    }
      assignNewClass_tmp->u.SEQ.right = 
        T_Seq(T_Move(
          T_Mem(
            T_Binop(T_plus, assignNewClass_object, T_Const(bind->offset))
          ),
          init_exp
        ), NULL);
    assignNewClass_tmp = assignNewClass_tmp->u.SEQ.right;
  }
}




T_exp convertExp(A_exp exp)
{
    if(!exp)
        return NULL;
    // printA_Exp(stderr, exp);
    // fprintf(stderr, "\n%d\n", exp->kind);
    T_exp Exp = NULL;
    switch (exp->kind)
    {
    case A_opExp:
    {
      switch (exp->u.op.oper)
      {
      case A_and:
      case A_or:
      case A_less:
      case A_le:
      case A_greater:
      case A_ge:
      case A_eq:
      case A_ne:
        Exp = Bool_to_Esc(exp);
      break;
      case A_plus:
        Exp = T_Binop(
          T_plus,
          convertExp(exp->u.op.left),
          convertExp(exp->u.op.right)
        );
      break;
      case A_minus:
        Exp = T_Binop(
          T_minus,
          convertExp(exp->u.op.left),
          convertExp(exp->u.op.right)
        );
      break;
      case A_times:
        Exp = T_Binop(
          T_mul,
          convertExp(exp->u.op.left),
          convertExp(exp->u.op.right)
        );
      break;
      case A_div:
        Exp = T_Binop(
          T_div,
          convertExp(exp->u.op.left),
          convertExp(exp->u.op.right)
        );
      break;
      default:
        assert(0);
        break;
      }
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_arrayExp:
    {
      T_exp arr = convertExp(exp->u.array_pos.arr);
      T_exp arr_pos = convertExp(exp->u.array_pos.arr_pos);

      Exp = T_Mem(
        T_Binop(T_plus, arr, 
          T_Binop(T_mul, 
            T_Binop(T_plus, arr_pos, T_Const(1)), 
            T_Const(ELE_SIZE)
          )
        )
      );
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_callExp:
    {
      T_expList params = convertParams(exp->u.call.el);
      T_exp obj = convertExp(exp->u.call.obj);
      assert(recentClass && recentObj);

      params = T_ExpList(T_Temp(recentObj), params);

      
      S_bind f_sbind = S_look(
        recentClass,
        S_FunSymbol(exp->u.call.fun)
      );
      int offset = f_sbind->offset;
      // fprintf(stderr, "%s: %d\n", exp->u.call.fun, offset);
      assert(f_sbind);
      S_bind ret_sbind = S_look(
        f_sbind->value->u.function.table,
        S_Symbol(Fun_ReturnKey)
      );
      assert(ret_sbind);
      if(ret_sbind->value->kind == Ty_class)
      {
        recentObj = Temp_newtemp();
        recentClass = ret_sbind->value->u.module.table;
        Exp = T_Eseq(
          T_Move(
            T_Temp(recentObj), 
            T_Call(exp->u.call.fun, T_Mem(T_Binop(T_plus, obj, T_Const(offset))), params)
          ),
          T_Temp(recentObj)
        );
      }else{
        recentObj = NULL;
        recentClass = NULL;
        Exp = T_Call(exp->u.call.fun, T_Mem(T_Binop(T_plus, obj, T_Const(offset))), params);
      }

    }
		break;
    case A_classVarExp:
    {
      T_exp obj = convertExp(exp->u.classvar.obj);
      assert(recentClass);
      S_bind v_sbind = S_look(
        recentClass, 
        S_Symbol(exp->u.classvar.var));
      if(v_sbind->value->kind == Ty_class)
      {
        recentClass = v_sbind->value->u.module.table;
        assert(recentObj);
        recentObj = Temp_newtemp();
        
      
        Exp = T_Eseq(
          T_Move(
            T_Temp(recentObj), 
            T_Mem(T_Binop(T_plus, obj, T_Const(v_sbind->offset)))),
          T_Temp(recentObj));
        
      }else{
        recentClass = NULL;
        recentObj = NULL;
        Exp = T_Mem(T_Binop(T_plus, obj, T_Const(v_sbind->offset)));
      }
      
    }
		break;
    case A_boolConst:
    {
      Exp = T_Const(exp->u.b);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_numConst:
    {
      Exp = T_Const(exp->u.num);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_idExp:
    {
      temp_bind tbind = S_look(currentScope, S_Symbol(exp->u.v));
      assert(tbind);
      if(tbind->class_table)
      {
        recentClass = tbind->class_table;
        recentObj = tbind->tmp;
      }else{
        recentClass = NULL;
        recentObj = NULL;
      }
      Temp_temp tmp = tbind->tmp;
      assert(tmp);
      // showit(tmp, "shit");
      Exp = T_Temp(tmp);
    }
		break;
    case A_thisExp:
    {
      temp_bind this_bind = S_look(currentScope, S_Symbol(Class_ThisKey));
      assert(this_bind);
      assert(this_bind->class_table);
      recentClass = this_bind->class_table;
      recentObj = this_bind->tmp;
      Exp = T_Temp(this_bind->tmp);
    }
		break;
    case A_lengthExp:
    {
      T_exp arr = convertExp(exp->u.e);
      Exp = T_Mem(arr);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_newIntArrExp:
    {
      T_exp pos = convertExp(exp->u.e);
      Temp_temp tmp1 = Temp_newtemp();
      Temp_temp tmp2 = Temp_newtemp();

      Exp =  
      T_Eseq(
        T_Seq(
          T_Move(
            T_Temp(tmp1),
            pos
          ),
          T_Seq(
            T_Move(
              T_Temp(tmp2), 
              T_ExtCall("malloc", T_ExpList(
                T_Binop(T_mul, 
                  T_Binop(T_plus, T_Temp(tmp1), T_Const(1)),
                  T_Const(UNIT_SIZE)
                ), NULL)
              )
            ),
            T_Move(
              T_Mem(T_Temp(tmp2)), 
              T_Temp(tmp1)
            )
          )
        ),
        T_Temp(tmp2)
      );

      recentClass = NULL;
      recentObj = NULL;
    }
    break;
    case A_newObjExp:
    {
      S_bind c_sbind = S_look(t, S_ClassSymbol(exp->u.v));
      assert(c_sbind);
      

      Temp_temp c_tmp = Temp_newtemp();
      assignNewClass_object = T_Temp(c_tmp);
      assignNewClass_stm = T_Seq(NULL, NULL);
      assignNewClass_tmp = assignNewClass_stm;
      assignNewClass_name = c_sbind->value->u.module.sym;
      offset_count = 0;
      S_dump(c_sbind->value->u.module.table, assignNewClass);
      assignNewClass_stm = assignNewClass_stm->u.SEQ.right;
      // assert(assignNewClass_last);
      
      Exp = T_Eseq(
        checkSeqSafety(
          T_Seq(
            T_Move(assignNewClass_object, T_ExtCall("malloc", T_ExpList(T_Const(offset_count), NULL))),
            checkSeqSafety(assignNewClass_stm)
          )
        ),
        assignNewClass_object
      );

      recentClass = c_sbind->value->u.module.table;
      recentObj = c_tmp;
    }
		break;
    case A_notExp:
    {
      Exp = Bool_to_Esc(exp);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_minusExp:
    {
      Exp = T_Binop(
        T_minus, 
        T_Const(0),
        convertExp(exp->u.e));
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_escExp:
    {
      T_stm StmList = convertStmList(exp->u.escExp.ns);
      T_exp escExp = convertExp(exp->u.escExp.exp);
      Exp = T_Eseq(StmList, escExp);
    }
		break;
    case A_getint:
    {
      Exp = T_ExtCall("getint",NULL);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_getch:
    {
      Exp = T_ExtCall("getch",NULL);
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    case A_getarray:
    {
      T_exp extExp = convertExp(exp->u.e);

      Temp_temp tmp1 = Temp_newtemp();
      Temp_temp tmp2 = Temp_newtemp();
      Exp = T_Eseq(
        T_Seq(
          T_Seq(
            T_Move(T_Temp(tmp1), extExp),
            T_Move(T_Temp(tmp2),
              T_ExtCall("getarray", 
                T_ExpList(T_Binop(T_plus, T_Temp(tmp1), T_Binop(T_mul, T_Const(1), T_Const(UNIT_SIZE))), NULL))
            )
          ),
          T_Move(T_Mem(T_Temp(tmp1)), T_Temp(tmp2))
        ),
        T_Temp(tmp2)
      );
      recentClass = NULL;
      recentObj = NULL;
    }
		break;
    
    default:
      assert(0);
      break;
    }
    return Exp;
}

T_expList convertParams(A_expList elist)
{
  if(!elist)
    return NULL;

  T_expList ret = T_ExpList(convertExp(elist->head), NULL),
            ret_tmp = ret;
  for(A_expList now = elist->tail; now; now = now->tail)
  {
    ret_tmp->tail = T_ExpList(convertExp(now->head), NULL);
    ret_tmp = ret_tmp->tail;
  }
  return ret;
}