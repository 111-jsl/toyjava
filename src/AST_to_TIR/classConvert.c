#include <assert.h>
#include <stdio.h>
#include "classConvert.h"
#include "../TypeCheck/mainCheck.h"
#include "AT_define.h"
#include "mainConvert.h"


extern int UNIT_SIZE;
extern int ELE_SIZE;

static void printOffset(S_symbol sym, S_bind binding)
{
    if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
        return;
    fprintf(stderr, "%s: %d\n", S_name(sym), binding->offset);
}

static S_table FunTable = NULL;

static int offset = 0;
static bool has_changed = FALSE;
static int max_offset_of_extend = 0;
static S_symbol parentclass = NULL;
static void ini_extractOffset(S_symbol sym, S_bind binding){
    // fprintf(stderr, "%d\n", strlen(S_name(sym)));
    if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
        return;
    binding->offset = offset;
    offset += ELE_SIZE;
}
static void get_max_offset(S_symbol sym, S_bind binding){
    if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
        return;
    S_bind cbind = S_look(t, parentclass);
    S_table parenttable = cbind->value->u.module.table;
    S_bind pbind = S_look(parenttable, S_Symbol(S_name(sym)));
    if(pbind){
        if(binding->offset > max_offset_of_extend)
            max_offset_of_extend = binding->offset;
    }
}
static void extractOffset(S_symbol sym, S_bind binding)
{
    // fprintf(stderr, "%s par: %s\n", S_name(sym), S_name(parentclass));
    if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
        return;
    S_bind cbind = S_look(t, parentclass);
    S_table parenttable = cbind->value->u.module.table;
    S_bind pbind = S_look(parenttable, S_Symbol(S_name(sym)));

    if(pbind && pbind->offset != binding->offset){
        has_changed = TRUE;
        binding->offset = pbind->offset;
        // fprintf(stderr, "%s: %d\n", S_name(sym), binding->offset);
    }
}
static void wrap_remainOffset(S_symbol sym, S_bind binding){
    if(sym == S_Symbol(Class_DoneKey) || sym == S_Symbol(ParentClass_KeySuffix))
        return;
    S_bind cbind = S_look(t, parentclass);
    S_table parenttable = cbind->value->u.module.table;
    S_bind pbind = S_look(parenttable, sym);
    if(!pbind){
        max_offset_of_extend += UNIT_SIZE;
        binding->offset = max_offset_of_extend;
    }
}






static T_funcDeclList flist_to_end(T_funcDeclList flist)
{
    T_funcDeclList last = flist;
    while(flist)
    {
        last = flist;
        flist = flist->tail;
    }           
    return last;
}

void getClassOffset(A_classDeclList cdl){  
    for(A_classDeclList now = cdl; now; now = now->tail){
        offset = 0;
        // fprintf(stderr, "class name %s\n", S_name(S_ClassSymbol(now->head->id)));
        S_bind class_bind = S_look(t, S_ClassSymbol(now->head->id));
        S_dump(class_bind->value->u.module.table, ini_extractOffset);
    }

    has_changed = TRUE;
    while(has_changed){
        has_changed = FALSE;
        for(A_classDeclList now = cdl; now; now = now->tail){
            //处理class table，获得offset
            // fprintf(stderr, "%s\n", now->head->id);
            S_bind class_bind = S_look(t, S_ClassSymbol(now->head->id));
            parentclass = S_look(class_bind->value->u.module.table, S_Symbol(ParentClass_KeySuffix));
            if(parentclass){
                S_dump(class_bind->value->u.module.table, extractOffset);
            }
        }
    }
    for(A_classDeclList now = cdl; now; now = now->tail){
        S_bind class_bind = S_look(t, S_ClassSymbol(now->head->id));
        parentclass = S_look(class_bind->value->u.module.table, S_Symbol(ParentClass_KeySuffix));
        if(parentclass){
            max_offset_of_extend = 0;
            S_dump(class_bind->value->u.module.table, get_max_offset);
            S_dump(class_bind->value->u.module.table, wrap_remainOffset);
            
        }
    }


}

T_funcDeclList convertClassDeclList(A_classDeclList cdl)
{
    if(!cdl)
        return NULL;

    
    T_funcDeclList flist = NULL, flist_tmp = NULL;
    for(A_classDeclList now = cdl; now; now = now->tail)
    {
        
        T_funcDeclList fun = convertClassDecl(now->head);
        if(!fun)
            continue;
        if(!flist)
        {
            flist = fun;
            flist_tmp = flist_to_end(flist);
        }else{
            flist_tmp->tail = fun;
            flist_tmp = flist_to_end(flist_tmp);
        }
        
        
    }
    return flist;
}




T_funcDeclList convertClassDecl(A_classDecl cd)
{
    if(!cd)
        return NULL;
    S_bind class_bind = S_look(t, S_ClassSymbol(cd->id));
    //记录当前class
    current_class = class_bind->value;
    return convertMethodDeclList(cd->mdl);

}

T_funcDeclList convertMethodDeclList(A_methodDeclList mdl)
{
    if(!mdl)
        return NULL;
    T_funcDeclList flist = T_FuncDeclList(NULL, NULL), flist_tmp = flist;
    T_funcDecl fun = convertMethodDecl(mdl->head);
    assert(fun);
    flist_tmp->head = fun;
    for(A_methodDeclList now = mdl->tail; now; now = now->tail)
    {
        fun = convertMethodDecl(now->head);
        assert(fun);
        flist_tmp->tail = T_FuncDeclList(fun, NULL);
        flist_tmp = flist_tmp->tail;
        
    }
    if(flist->head == NULL)
        return NULL;
    return flist;
}

T_funcDecl convertMethodDecl(A_methodDecl md)
{
    if(!md)
        return NULL;
    // create new table
    FunTable = S_empty();
    currentScope = FunTable;
    // first put label
    
    T_stm fun_start = T_Label(Temp_namedlabel(AT_concat(S_name(S_FunSymbol(md->id)), S_name(current_class->u.module.sym))));
    
    // handle formal list
    // first put class into templist
    Temp_temp class_temp = Temp_newtemp();
    S_enter(
        FunTable, 
        S_Symbol(Class_ThisKey),
        temp_Bind(class_temp, current_class->u.module.table)
    );
    Temp_tempList params = Temp_TempList(class_temp, NULL),
                  params_tmp = params;

    // then put other params 
    A_formalList fl = md->fl;
    for(A_formalList now = fl; now; now = now->tail)
    {
        A_formal f = now->head;
        Temp_temp f_temp = Temp_newtemp();
        if(f->t->t == A_idType){
            S_bind class_bind = S_look(t, S_ClassSymbol(f->t->id));
            assert(class_bind);
            S_enter(FunTable, S_Symbol(f->id), temp_Bind(f_temp, class_bind->value->u.module.table));
        }else{
            S_enter(FunTable, S_Symbol(f->id), temp_Bind(f_temp, NULL));
        }
        
        params_tmp->tail = Temp_TempList(f_temp, NULL);
        params_tmp = params_tmp->tail;
    }

    // then handle vardecl and stm
    T_stm var = convertVarDeclList(md->vdl);
    T_stm stm = convertStmList(md->sl);
    T_stm seq;
    if(var)
    {
        seq = T_Seq(var, stm);
    }else{
        seq = stm;
    }


    return T_FuncDecl(md->id, params, T_Seq(fun_start, seq));
}