#pragma once


#include "../Utils/pr_tree_readable.h"
#include "../Utils/treep.h"
#include "../Utils/temp.h"

extern S_table currentScope;

T_funcDeclList convertProg(A_prog prog, string target_language);
T_funcDecl convertMainMethod(A_mainMethod m);

T_stm convertVarDeclList(A_varDeclList vdl);
T_stm convertVarDecl(A_varDecl vd);

T_stm convertStmList(A_stmList sl);
T_stm convertStm(A_stm stm);

T_exp convertIntConstList(A_expList elist);
T_exp convertExp(A_exp exp);

T_expList convertParams(A_expList elist);