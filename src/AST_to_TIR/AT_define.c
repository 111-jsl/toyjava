#include "AT_define.h"
#include "../Utils/util.h"
#include <string.h>
#include <assert.h>

int UNIT_SIZE;
int ELE_SIZE;

temp_bind temp_Bind(Temp_temp tmp, S_table class_table)
{
    temp_bind b = checked_malloc(sizeof(*b));
    b->tmp = tmp;
    b->class_table = class_table;
    return b;
}
string AT_concat(string a, string b)
{
    string ret = checked_malloc(strlen(a) + 1 + strlen(b));
    sprintf(ret, "%s.%s", a, b);
    return ret;
}

static void checkSeq(T_stm stm){
    if(!stm) return;
    T_stm now;
    for(now = stm; now->kind == T_SEQ && now->u.SEQ.right; now = now->u.SEQ.right){
        assert(now->u.SEQ.left);
    }
    assert(now->kind != T_SEQ);
}



T_stm checkSeqSafety(T_stm stm)
{
    
    if(!stm || stm->kind != T_SEQ) return stm;
    if(!stm->u.SEQ.right) return stm->u.SEQ.left;

    T_stm ret = T_Seq(NULL, NULL), tmp = ret;

    T_stm now = stm, last = NULL;
    for(; now->kind == T_SEQ && now->u.SEQ.right; now = now->u.SEQ.right){
        if(now->u.SEQ.left){
            tmp->u.SEQ.left = now->u.SEQ.left;
            tmp->u.SEQ.right = T_Seq(NULL, NULL);
            last = tmp;
            tmp = tmp->u.SEQ.right;
        }
    }
    if(now->kind != T_SEQ){
        if(last){
            last->u.SEQ.right = now;
        }else{
            ret = now;
        }
    }else{
        if(last){
            last->u.SEQ.right = now->u.SEQ.left;
            assert(now->u.SEQ.left != T_SEQ);
        }
        else{
            ret = now->u.SEQ.left;
        }
    }
    checkSeq(ret);
    return ret;
}

T_stm Seq_concat(T_stm stm1, T_stm stm2){
    checkSeq(stm1);
    checkSeq(stm2);
    if(stm1 && stm2){
        return T_Seq(stm1, stm2);
    }else if(stm1){
        return stm1;
    }else if(stm2){
        return stm2;
    }else{
        return NULL;
    }
    
    
}
