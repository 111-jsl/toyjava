#pragma once


#include "../Utils/treep.h"
#include "../Utils/temp.h"

void getClassOffset(A_classDeclList cdl);

T_funcDeclList convertClassDeclList(A_classDeclList cdl);
T_funcDeclList convertClassDecl(A_classDecl cd);

T_funcDeclList convertMethodDeclList(A_methodDeclList mdl);
T_funcDecl convertMethodDecl(A_methodDecl md);
