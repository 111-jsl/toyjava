#pragma once
#include "TC_define.h"

extern S_table t;           // global table
extern Ty_ty current_class; // local table
extern Ty_ty current_fun;   // local table


bool checkProg(A_prog prog);
bool checkMainMethod(A_mainMethod m);

bool checkClassDeclList(A_classDeclList cdl, Resolve_t code);
bool checkClassDecl(A_classDecl cd, Resolve_t code);

bool checkVarDeclList(A_varDeclList vdl, S_table table);
bool checkVarDecl(A_varDecl vd, S_table table);

bool checkStmList(A_stmList sl);
bool checkStm(A_stm stm);

bool checkIntConstList(A_expList elist);
bool checkExp(A_exp exp, Ty_tyList tylist);