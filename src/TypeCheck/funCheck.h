#pragma once

#include "TC_define.h"



bool add_FunParam(A_formal f);

bool checkParamList(A_expList elist, Ty_ty params);

bool checkMethodDeclList(A_methodDeclList mdl, Resolve_t code);
bool checkMethodDecl(A_methodDecl md, Resolve_t code);

bool checkFormalList(A_formalList fl);
bool checkFormal(A_formal f);