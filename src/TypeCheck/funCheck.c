#include "funCheck.h"
#include "../Utils/table.h"
#include "mainCheck.h"
#include "extendCheck.h"

// extern S_table t;           // global table
// extern Ty_ty current_class; // local table
// extern Ty_ty current_fun;   // local table


static A_expList CallParams = NULL;
static bool checkParam_isFail = FALSE;
static bool enterParamZone = FALSE;
static int formal_cnt = 0;

bool add_FunParam(A_formal f)
{
    assert(current_fun);
    S_table funTable = current_fun->u.function.table;
    if(S_look(funTable, S_Symbol(f->id)))
    {
        fprintf(stderr, "Fun Decl failed: Params multiple defined\n");
        goto error;
    }
    string ParamKey = checked_malloc(sizeof(char) * 10 + strlen(Fun_ParamKey));
    sprintf(ParamKey, "%s%d", Fun_ParamKey, formal_cnt++);
    
    S_bind bind = NULL;
    switch (f->t->t)
    {
    case A_intType:
    {
        bind = S_Bind(Ty_Int(), Ty_Int(), f->pos, NULL);
    }
    break;
    case A_idType:
    {
        bind = S_look(t, S_ClassSymbol(f->t->id));
        if (!bind)
        {
            fprintf(stderr, "Fun Decl failed: Param Class not defined, %s\n", f->t->id);
            goto error;
        }
    }
    break;
    case A_intArrType:
    {
        Ty_ty ty = Ty_Array(Ty_Int());
        bind =  S_Bind(ty, ty, f->pos, NULL);
    }
    break;
    default:
        assert(0);
        break;
    }
    assert(bind);
    S_enter(funTable, S_Symbol(f->id), bind);
    S_enter(funTable, S_Symbol(ParamKey), bind);
    return TRUE;
error:
    printPos(f->pos);
    return FALSE;
}



static void checkSingleParam(S_symbol id, Ty_tyList tylist)
{
    string name = S_name(id);
    // printf("name %s\n", name);
    // mark以后才是真正的参数
    if (!strcmp(name, "<mark>"))
    {
        enterParamZone = TRUE;
        return;
    }
    if (!enterParamZone)
    {
        return;
    }

    // ignore "ret" and "num"
    if (!strcmp(name, Fun_ReturnKey) || !strcmp(name, Fun_NumKey))
    {
        return;
    }

    // 已经fail了提早结束
    if (checkParam_isFail)
        return;

    // 如果explist已经到底了，param没到底
    if (!CallParams)
    {
        return;
    }
    Ty_ty ty = Ty_Value(tylist);
    // 检查类型是否匹配
    Ty_tyList e_tylist = Ty_TyList_Init();
    if (!checkExp(CallParams->head, e_tylist))
        return FALSE;
    if (Ty_Value(e_tylist)->kind != ty->kind)
    {
        // fprintf(stderr, "Param failed: %d and %d\n", Ty_Value(e_tylist)->kind, ty->kind);
        checkParam_isFail = TRUE;
        return;
    }
    CallParams = CallParams->tail;
}


bool checkParamList(A_expList elist, Ty_ty params)
{
    // 保证类型是function
    assert(params->kind == Ty_function);
    S_table funTable = params->u.function.table;

    // 检查个数
    int elist_cnt = 0;
    for (A_expList now = elist; now; now = now->tail)
    {
        elist_cnt++;
    }
    int num = S_look(funTable, S_Symbol(Fun_NumKey));
    if (num != elist_cnt)
    {
        fprintf(stderr, "Param failed: false number of params\n\
        expected %d, given %d\n",
                num, elist_cnt);
        goto error;
    }

    int ParamId = 1;
    string key = checked_malloc(sizeof(char)*strlen(Fun_ParamKey) + 10);
    for (A_expList now = elist; now; now = now->tail)
    {
        sprintf(key, "%s%d", Fun_ParamKey, ParamId++);
        S_bind bind = S_look(funTable, S_Symbol(key));
        assert(bind);
        Ty_tyList e_tylist = Ty_TyList_Init();
        if (!checkExp(now->head, e_tylist))
            return FALSE;
        if (Ty_Value(e_tylist)->kind != bind->value->kind)
        {
            fprintf(stderr, "Param failed: incompatible type, %d and %d\n",
            Ty_Value(e_tylist)->kind, bind->value->kind);
            goto error;
        }else if(bind->value->kind == Ty_class 
            && Ty_Value(e_tylist)->u.module.sym != bind->value->u.module.sym
            && !can_Upcast(S_name(bind->value->u.module.sym), S_name(Ty_Value(e_tylist)->u.module.sym)))
        {
            fprintf(stderr, "Param failed: incompatible type, %s and %s\n",
            S_name(Ty_Value(e_tylist)->u.module.sym), 
            S_name(bind->value->u.module.sym));
            goto error;
        }
    }
    return TRUE;

error:
    // printPos(elist->head->pos);
    return FALSE;
}


bool checkMethodDeclList(A_methodDeclList mdl, Resolve_t code)
{
    for (A_methodDeclList now = mdl; now; now = now->tail)
    {
        if (!checkMethodDecl(now->head, code))
            return FALSE;
    }
    return TRUE;
}

bool checkMethodDecl(A_methodDecl md, Resolve_t code)
{
    switch (code)
    {
    case Resolve_VarDecl:
    {   
        if(S_look(current_class->u.module.table, S_FunSymbol(md->id)))
        {
            fprintf(stderr, "Fun Decl failed: Fun multiple declared\n");
            goto error;
        }
        S_table funTable = S_empty();
        current_fun = Ty_Function(S_FunSymbol(md->id), funTable, current_class->u.module.sym);

        formal_cnt = 0;

        // return
        A_formal ret = A_Formal(md->pos, md->t, Fun_ReturnKey);
        if (!add_FunParam(ret))
            return FALSE;

        // 一堆变量放进去
        if (!checkFormalList(md->fl))
            return FALSE;

        // funTable放进去
        assert(current_class && current_fun);
        S_enter(current_class->u.module.table, S_FunSymbol(md->id), S_Bind(current_fun, current_fun, md->pos, NULL));
    }
    break;
    case Resolve_Stm:
    {
        assert(current_class);
        S_bind fun_bind = S_look(current_class->u.module.table, S_FunSymbol(md->id));
        assert(fun_bind);
        current_fun = fun_bind->value;
        S_table funTable = current_fun->u.function.table;
        S_beginScope(funTable);
        if (!checkVarDeclList(md->vdl, funTable))
            return FALSE;
        if (!checkStmList(md->sl))
            return FALSE;
        S_endScope(funTable);
        // checked_free(funTable);
    }
    break;
    default:
        assert(0);
        break;
    }
    current_fun = NULL;

    return TRUE;
error:
    printPos(md->pos);
    return FALSE;
}

bool checkFormalList(A_formalList fl)
{
    int formal_cnt = 0;
    for (A_formalList now = fl; now; now = now->tail)
    {
        formal_cnt++;
        if (!checkFormal(now->head))
            return FALSE;
    }
    assert(current_fun);
    S_enter(
        current_fun->u.function.table,
        S_Symbol(Fun_NumKey),
        formal_cnt);
    return TRUE;
}
bool checkFormal(A_formal f)
{
    if (!add_FunParam(f))
        return FALSE;
    return TRUE;
error:
    printPos(f->pos);
    return FALSE;
}