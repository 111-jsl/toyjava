#include "extendCheck.h"
#include "mainCheck.h"
// extern S_table t;           // global table
// extern Ty_ty current_class; // local table
// extern Ty_ty current_fun;   // local table

static Extend_isFail = FALSE;
static S_table ExtendStack = NULL; // key: sym, value: ty
static S_symbol TargetSym = NULL;
static S_bind TargetBind = NULL;


static bool checkFormalAlign(Ty_ty fun1, Ty_ty fun2)
{
    assert(fun1 && fun2);
    assert(fun1->kind == Ty_function && fun2->kind == Ty_function);
    S_table t1 = fun1->u.function.table, t2 = fun2->u.function.table;
    int n1 = S_look(t1, S_Symbol(Fun_NumKey));
    int n2 = S_look(t2, S_Symbol(Fun_NumKey));
    if(n1 != n2){
        goto error;
    }
    string key = checked_malloc(sizeof(char)*strlen(Fun_ParamKey) + 10);
    for(int i = 0; i < n1; i++){
        sprintf(key, "%s%d", Fun_ParamKey, i);
        S_bind b1 = S_look(t1, S_Symbol(key));
        S_bind b2 = S_look(t2, S_Symbol(key));
        assert(b1 && b2);
        if(b1->value->kind != b2->value->kind){
            goto error;
        }
    }
    return TRUE;
error:
    fprintf(stderr, "Extend failed: inherent function is not the same\n");
    return FALSE;
}

// 对target遍历每个son进行检查
static void checkSingleSon(S_symbol sym, S_bind bind)
{
    // printf("in checkSingleSon: %s\n", S_name(sym));
    if (Extend_isFail)
        return;
    if (!strcmp(S_name(sym), "<mark>"))
    {
        return;
    }
    Ty_ty ty = bind->value;
    assert(ty->kind == Ty_class);
    S_bind same = S_look(ty->u.module.table, TargetSym);
    if (same)
    {
        Ty_ty same_ty = same->value;
        if (same_ty->kind == Ty_function)
        {
            if(!checkFormalAlign(same_ty, TargetBind->value)){
                goto error;
            }
        }
        else
        {
            fprintf(stderr, "Extend failed: inherent variable\n");
            goto error;
        }
    }
    else
    {
        S_enter(ty->u.module.table, TargetSym, 
        S_Bind(
            TargetBind->location,
            TargetBind->value,
            TargetBind->pos,
            TargetBind->ast_node));
    }
    return;
error:
    printPos(bind->pos);
    Extend_isFail = TRUE;
}
// 对当前栈上的class，对它每个变量分别查它的儿子有没有
//  invalid sym: parentid, done
static void checkSingleVar(S_symbol sym, S_bind bind)
{
    if (Extend_isFail)
        return;
    string name = S_name(sym);
    if (!strcmp(name, ParentClass_KeySuffix) || !strcmp(name, Class_DoneKey))
    {
        return;
    }
    TargetSym = sym;
    TargetBind = bind;
    S_dump(ExtendStack, checkSingleSon);
}

// 对每个声明的class进行继承的检查
static void ExtendsRecur(S_bind bind)
{
    if (Extend_isFail)
        return;
    assert(bind);
    // printf("now %s\n", S_name(Ty_Value(tylist)->u.module.sym));
    Ty_ty ty = bind->value;
    assert(ty->kind == Ty_class);
    S_table table = ty->u.module.table;
    S_dump(table, checkSingleVar);

    if (S_look(table, S_Symbol(Class_DoneKey)))
    {
        return;
    }
    S_enter(table, S_Symbol(Class_DoneKey), Class_DoneKey);
    S_enter(ExtendStack, ty->u.module.sym, bind);
    S_symbol ParentId = S_look(table, S_Symbol(ParentClass_KeySuffix));
    // printf("parent: %s\n", ParentId);
    if (!ParentId)
        return;
    S_bind circle = S_look(ExtendStack, ParentId);
    if (circle)
    {
        fprintf(stderr, "Extend failed: Circular inherence\n");
        goto error;
    }
    // S_dump(t, S_dump_print);
    S_bind ParentTy = S_look(t, ParentId);
    if (!ParentTy)
    {
        fprintf(stderr, "Extend failed: Parent not defined %s\n", S_name(ParentId));
        goto error;
    }

    ExtendsRecur(ParentTy);
    return;
error:
    printPos(bind->pos);
    Extend_isFail = TRUE;
}
// 查看global表，如果是class则检查
static void checkSingleClass(S_symbol sym, S_bind bind)
{

    if (Extend_isFail)
        return;
    assert(ExtendStack);
    // printf("name: %s\n", S_name(sym));
    // assert(tylist);
    // printf("check: %d\n", tylist->tail && tylist);
    if (bind->value->kind == Ty_class)
    {
        S_beginScope(ExtendStack);
        ExtendsRecur(bind);
        S_endScope(ExtendStack);
        // printf("%s Extend check completed\n", S_name(Ty_Value(tylist)->u.module.sym));
    }
}

bool checkExtend()
{
    assert(t);
    ExtendStack = S_empty();
    Extend_isFail = FALSE;
    S_dump(t, checkSingleClass);
    return Extend_isFail;
}

bool can_Upcast(string dst, string src)
{
    if (!strcmp(src, dst))
    {
        return TRUE;
    }
    S_bind src_bind = S_look(t, S_Symbol(src));
    assert(src_bind);
    S_symbol ParentId = S_look(src_bind->value->u.module.table, S_Symbol(ParentClass_KeySuffix));

    if (!ParentId)
    {
        return FALSE;
    }
    return can_Upcast(dst, S_name(ParentId));
}