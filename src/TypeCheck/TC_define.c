#include "TC_define.h"

inline S_symbol S_ClassSymbol(string class_name)
{
    string ret = checked_malloc(strlen(class_name) + strlen(Class_KeySuffix));
    sprintf(ret, "%s%s", class_name, Class_KeySuffix);
    return S_Symbol(ret);
}

inline S_symbol S_FunSymbol(string fun)
{
    string ret = checked_malloc(strlen(fun) + strlen(Fun_KeySuffix));
    sprintf(ret, "%s%s", fun, Fun_KeySuffix);
    return S_Symbol(ret);
}

void printPos(A_pos p)
{
    fprintf(stderr, "line:\t%d\tpos:\t%d\t\n", p->line, p->pos);
}

inline Ty_ty Ty_Location(Ty_tyList tylist)
{
    assert(tylist);
    return tylist->head;
}

inline Ty_ty Ty_Value(Ty_tyList tylist)
{
    // printf("%d\n", tylist);
    assert(tylist && tylist->tail);
    return tylist->tail->head;
}

inline Ty_tyList Ty_TyList_Init()
{
    return Ty_TyList(Ty_Nil(), Ty_TyList(Ty_Nil(), NULL));
}

Ty_tyList buildSingleTy(bool has_value, Ty_ty ty)
{
    Ty_tyList tylist = NULL;
    if (has_value)
    {
        tylist = Ty_TyList(ty, tylist);
        tylist = Ty_TyList(ty, tylist);
    }
    else
    {
        tylist = Ty_TyList(Ty_Nil(), tylist);
        tylist = Ty_TyList(ty, tylist);
    }
    return tylist;
}