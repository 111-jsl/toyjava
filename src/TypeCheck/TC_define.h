#pragma once
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "../fdmjast.h"
#include "../Utils/types.h"
#include "../Utils/symbol.h"
// #include "../Utils/util.h"

// #define TC_Seperator (".")

#define Fun_ReturnKey (".ret")
#define Fun_KeySuffix (".fun")
#define Fun_NumKey (".num")
#define Fun_ParamKey (".param")

#define Class_KeySuffix (".class")
#define ParentClass_KeySuffix (".parentclass")
#define Class_DoneKey (".done")

void printPos(A_pos p);

typedef enum{
    Resolve_VarDecl,
    Resolve_Stm
} Resolve_t;






S_symbol S_ClassSymbol(string class_name);
S_symbol S_FunSymbol(string fun);
Ty_ty Ty_Location(Ty_tyList tylist);
Ty_ty Ty_Value(Ty_tyList tylist);
Ty_tyList Ty_TyList_Init();
Ty_tyList buildSingleTy(bool has_value, Ty_ty ty);
