#include "mainCheck.h"
#include "funCheck.h"
#include "extendCheck.h"




S_table t = NULL;           // global table
Ty_ty current_class = NULL; // local table
Ty_ty current_fun = NULL;   // local table




bool checkProg(A_prog prog)
{
    t = NULL;
    current_class = NULL;
    current_fun = NULL;
    if (!prog)
    {
        fprintf(stderr, "Program is NULL\n");
        goto error;
    }
    if (!prog->m)
    {
        fprintf(stderr, "Main is NULL\n");
        goto error;
    }
    // 初始化全局表
    t = S_empty();
    // 第一次遍历ast，获得所有class声明
    // printf("\n1: traverse AST, get Class Declaration...\n");
    for (A_classDeclList now = prog->cdl; now; now = now->tail)
    {
        S_table class_table = S_empty();
        if (now->head->parentID)
        {
            S_enter(
                class_table,
                S_Symbol(ParentClass_KeySuffix),
                S_ClassSymbol(now->head->parentID));
        }
        if(S_look(t, S_ClassSymbol(now->head->id)))
        {
            fprintf(stderr, "Class Decl failed: Class name multiple defined\n");
            printPos(now->head->pos);
            goto error;
        }
        current_class = Ty_Class(
            S_ClassSymbol(now->head->id),
            class_table);
        
        S_enter(t, S_ClassSymbol(now->head->id), S_Bind(current_class, current_class, now->head->pos, NULL));
        // printf("first: %s\n", now->head->id);
    }
    current_class = NULL;
    // 第二次遍历ast，解决所有声明
    // printf("\n2: traverse AST, get inClass Declaration...\n");
    if (!checkClassDeclList(prog->cdl, Resolve_VarDecl))
        return FALSE;
    // S_dump(t, S_dump_print);

    // 第三次遍历表，解决继承
    // 1. super class的变量无法在sub class重定义，
    //    但是方法可以重载（只能一模一样，不能变输入输出）
    // 2. circular extends
    // 3. 把所有继承来的变量都放好

    // printf("\n3: traverse Table, resolve inherence...\n");
    if (checkExtend())
    {
        goto error;
    }

    // 第四次遍历ast，解决所有语句
    // printf("\n4: traverse AST, resolve inFunction Declaration and Stm...\n");
    if (!checkClassDeclList(prog->cdl, Resolve_Stm))
        return FALSE;
    // //debug
    // Ty_tyList c1 = S_look(t, S_ClassSymbol("c1"));
    // S_dump(Ty_Value(c1)->u.module.table, S_dump_print);
    
    



    // printf("\n5: check Main...\n");
    if (!checkMainMethod(prog->m))
        return FALSE;

    return TRUE;

error:
    return FALSE;
}

bool checkMainMethod(A_mainMethod m)
{
    // S_beginScope(t);
    // S_table main_table = S_empty();
    S_enter(t, S_Symbol(Fun_ReturnKey), S_Bind(Ty_Int(), Ty_Int(), m->pos, NULL));
    // S_dump(t, S_dump_print);
    current_fun = Ty_Function(S_Symbol("Main"), t, NULL);

    if (!checkVarDeclList(m->vdl, t))
        return FALSE;
    if (!checkStmList(m->sl))
        return FALSE;
    // S_endScope(t);
    return TRUE;
error:
    printPos(m->pos);
    return FALSE;
}

bool checkClassDeclList(A_classDeclList cdl, Resolve_t code)
{

    for (A_classDeclList now = cdl; now; now = now->tail)
    {
        if (!checkClassDecl(now->head, code))
            return FALSE;
    }
    return TRUE;
}

bool checkClassDecl(A_classDecl cd, Resolve_t code)
{
    // printf("checkClass: %s\n", cd->id);
    S_bind class_bind = S_look(t, S_ClassSymbol(cd->id));
    assert(class_bind);
    current_class = class_bind->value;
    switch (code)
    {
    case Resolve_VarDecl:
    {
        if (!checkVarDeclList(cd->vdl, current_class->u.module.table))
            return FALSE;
        if (!checkMethodDeclList(cd->mdl, code))
            return FALSE;
    }
    break;
    case Resolve_Stm:
    {
        if (!checkMethodDeclList(cd->mdl, code))
            return FALSE;
    }
    break;
    default:
        assert(0);
        break;
    }
    current_class = NULL;
    return TRUE;
error:
    printPos(cd->pos);
    return FALSE;
}

bool checkVarDeclList(A_varDeclList vdl, S_table table)
{
    for (A_varDeclList now = vdl; now; now = now->tail)
    {
        if (!checkVarDecl(now->head, table))
            return FALSE;
    }
    return TRUE;
}

bool checkVarDecl(A_varDecl vd, S_table table)
{
    // 检查初始化是否正确
    S_symbol sym = S_Symbol(vd->v);
    if (vd->elist && !checkIntConstList(vd->elist))
        return FALSE;
    // 检查重复定义
    S_bind multi_bind = S_look(table, sym);
    if (multi_bind)
    {
        fprintf(stderr, "VarDecl failed: multiple defined variable: %s\n", vd->v);
        goto error;
    }
    // 存储定义到表中
    switch (vd->t->t)
    {
    case A_intType:
        S_enter(table, sym, S_Bind(Ty_Int(), Ty_Int(), vd->t->pos, vd->elist));
        break;
    case A_idType:
    {

        S_bind class_t = S_look(t, S_ClassSymbol(vd->t->id));
        if (!class_t)
        {
            fprintf(stderr, "VarDecl failed: using class not defined: %s\n", vd->t->id);
            goto error;
        }
        S_enter(table, sym, class_t);
    }
    break;
    case A_intArrType:
    {
        Ty_ty ty = Ty_Array(Ty_Int());
        S_enter(table, sym, S_Bind(ty, ty, vd->t->pos, vd->elist));
    }
    break;
    default:
        assert(0);
        break;
    }
    return TRUE;
error:
    printPos(vd->pos);
    return FALSE;
}

bool checkStmList(A_stmList sl)
{
    for (A_stmList now = sl; now; now = now->tail)
    {
        if (!checkStm(now->head))
            return FALSE;
    }
    return TRUE;
}
bool checkStm(A_stm stm)
{
    if (!stm)
    {
        return TRUE;
    }
    switch (stm->kind)
    {
    case A_nestedStm:
        if (!checkStmList(stm->u.ns))
            return FALSE;
        break;
    case A_ifStm:
    {
        Ty_tyList if_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.if_stat.e, if_tylist))
            return FALSE;
        if (Ty_Value(if_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "if failed: exp in if expected Int\n");
            goto error;
        }
        if (!checkStm(stm->u.if_stat.s1))
            return FALSE;
        if (!checkStm(stm->u.if_stat.s2))
            return FALSE;
    }
    break;
    case A_whileStm:
    {
        Ty_tyList while_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.while_stat.e, while_tylist))
            return FALSE;
        if (Ty_Value(while_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "while failed: exp in while expected Int\n");
            goto error;
        }
        if (!checkStm(stm->u.while_stat.s))
            return FALSE;
    }
    break;
    case A_assignStm:
    {
        Ty_tyList arr_tylist = Ty_TyList_Init();
        Ty_tyList val_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.assign.arr, arr_tylist))
            return FALSE;
        if (!checkExp(stm->u.assign.value, val_tylist))
            return FALSE;
        Ty_ty arr_ty = Ty_Location(arr_tylist);
        Ty_ty val_ty = Ty_Value(val_tylist);
        if (arr_ty->kind != val_ty->kind)
        {
            fprintf(stderr, "assign failed: type incompatible\n\
            %d and %d\n",
                    arr_ty->kind, val_ty->kind);
            goto error;
        }

        if (arr_ty->kind == Ty_class)
        {
            string arr_cname = S_name(arr_ty->u.module.sym);
            string val_cname = S_name(val_ty->u.module.sym);
            if (!can_Upcast(arr_cname, val_cname))
            {
                fprintf(stderr, "assign failed: class type incompatible\n\
                %s and %s\n",
                        arr_cname, val_cname);
                goto error;
            }
        }
    }
    break;
    case A_arrayInit:
    {
        Ty_tyList arr_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.array_init.arr, arr_tylist))
            return FALSE;
        if (Ty_Location(arr_tylist)->kind != Ty_array)
        {
            fprintf(stderr, "ArrayInit failed: Variable incompatible\n");
            goto error;
        }
        if (!checkIntConstList(stm->u.array_init.init_values))
            return FALSE;
        arr_tylist->tail->head = arr_tylist->head;
    }
    break;
    case A_callStm:
    {
        Ty_tyList obj_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.call_stat.obj, obj_tylist))
            return FALSE;
        if (Ty_Value(obj_tylist)->kind != Ty_class)
        {
            fprintf(stderr, "Class Fun failed: access non-Object type\n");
            goto error;
        }
        S_bind fun = S_look(Ty_Value(obj_tylist)->u.module.table, S_FunSymbol(stm->u.call_stat.fun));
        if (!fun || fun->value->kind != Ty_function)
        {
            fprintf(stderr, "Class Fun failed: no Method named %s in Object\n", stm->u.call_stat.fun);
            goto error;
        }
        if (!checkParamList(stm->u.call_stat.el, fun->value))
        {
            goto error;
        }
    }
    break;
    case A_continue:
        break;
    case A_break:
        break;
    case A_return:
    {
        Ty_tyList ret_tylist = Ty_TyList_Init();
        // printf("aha\n");

        if (!checkExp(stm->u.e, ret_tylist))
            return FALSE;

        if (!current_fun)
        {
            fprintf(stderr, "Return failed: Return outside Function\n");
            goto error;
        }

        S_bind current_ret = S_look(current_fun->u.function.table, S_Symbol(Fun_ReturnKey));
        assert(current_ret);
        if (Ty_Value(ret_tylist)->kind != current_ret->value->kind)
        {
            fprintf(stderr, "Return failed: Type incompatible\n");
            goto error;
        }
    }
    break;
    case A_putint:
    {
        Ty_tyList int_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.e, int_tylist))
            return FALSE;
        if (Ty_Value(int_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "PutInt failed: Type incompatible\n");
            goto error;
        }
    }
    break;
    case A_putarray:
    {
        Ty_tyList int_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.putarray.e1, int_tylist))
            return FALSE;
        if (Ty_Value(int_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "PutArray failed: Type incompatible\n");
            goto error;
        }
        Ty_tyList arr_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.putarray.e2, arr_tylist))
            return FALSE;
        if (Ty_Value(arr_tylist)->kind != Ty_array)
        {
            fprintf(stderr, "PutArray failed: Type incompatible\n");
            goto error;
        }
    }
    break;
    case A_putch:
    {
        Ty_tyList ch_tylist = Ty_TyList_Init();
        if (!checkExp(stm->u.e, ch_tylist))
            return FALSE;
        if (Ty_Value(ch_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "PutCh failed: Type incompatible\n");
            goto error;
        }
    }
    break;
    case A_starttime:
        break;
    case A_stoptime:
        break;
    default:
        assert(0);
        break;
    }
    return TRUE;
error:
    printPos(stm->pos);
    return FALSE;
}



bool checkIntConstList(A_expList elist)
{
    if (!elist)
        return TRUE;
    Ty_tyList ele_tylist = Ty_TyList_Init();
    if (!checkExp(elist->head, ele_tylist))
        return FALSE;
    for (A_expList now = elist->tail; now; now = now->tail)
    {
        Ty_tyList tmp_tylist = Ty_TyList_Init();
        if (!checkExp(now->head, tmp_tylist))
            return FALSE;
        if (Ty_Value(tmp_tylist)->kind != Ty_Value(ele_tylist)->kind)
        {
            fprintf(stderr, "IntConstList failed: Array elements type are not the same\n");
            goto error;
        }
    }
    if (Ty_Value(ele_tylist)->kind != Ty_int)
    {
        fprintf(stderr, "IntConstList failed: Array elements type is not Int, not support for now\n");
        goto error;
    }
    return TRUE;
error:
    printPos(elist->head->pos);
    return FALSE;
}


bool checkExp(A_exp exp, Ty_tyList tylist)
{
    switch (exp->kind)
    {
    case A_opExp:
    {
        Ty_tyList tylist1 = Ty_TyList_Init();
        Ty_tyList tylist2 = Ty_TyList_Init();
        if (!checkExp(exp->u.op.left, tylist1))
            return FALSE;
        if (!checkExp(exp->u.op.right, tylist2))
            return FALSE;
        if (Ty_Value(tylist1)->kind != Ty_int ||
            Ty_Value(tylist2)->kind != Ty_int)
        {
            // debug
            fprintf(stderr, "exp1 type: %d\texp2 type: %d\n",
                    Ty_Value(tylist1)->kind, Ty_Value(tylist2)->kind);
            fprintf(stderr, "binary Op failed: incompatible type\n");
            goto error;
        }
        // 原地修改
        tylist->tail->head = Ty_Value(tylist1);
    }
    break;
    case A_arrayExp:
    {
        Ty_tyList arr_tylist = Ty_TyList_Init();
        Ty_tyList pos_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.array_pos.arr, arr_tylist))
            return FALSE;
        if (Ty_Value(arr_tylist)->kind != Ty_array)
        {
            fprintf(stderr, "Array failed: indexing non-Array type\n");
            goto error;
        }
        if (!checkExp(exp->u.array_pos.arr_pos, pos_tylist))
            return FALSE;
        if (Ty_Value(pos_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "Array failed: index not Int\n");
            goto error;
        }
        // 原地修改
        // 如果array是刚刚new的，那就没有value
        tylist->head = Ty_Value(arr_tylist)->u.array;
        tylist->tail->head = Ty_Value(arr_tylist)->u.array;
    }
    break;
    case A_callExp:
    {

        Ty_tyList obj_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.call.obj, obj_tylist))
            return FALSE;

        if (Ty_Value(obj_tylist)->kind != Ty_class)
        {
            fprintf(stderr, "Class Fun failed: access non-Object type\n");
            goto error;
        }
        // printf("call fun %s\n", exp->u.call.fun);
        // S_dump(Ty_Value(obj_tylist)->u.module.table, S_dump_print);
        S_bind fun_bind = S_look(Ty_Value(obj_tylist)->u.module.table, S_FunSymbol(exp->u.call.fun));
        if (!fun_bind)
        {
            fprintf(stderr, "Class Fun failed: no Method named %s in Object\n", exp->u.call.fun);
            goto error;
        }
        Ty_ty fun = fun_bind->value;
        // printf("think about call\n");

        if (fun->kind != Ty_function)
        {
            fprintf(stderr, "Class Fun failed: no Method named %s in Object\n", exp->u.call.fun);
            goto error;
        }
        if (!checkParamList(exp->u.call.el, fun))
        {
            goto error;
        }

        S_bind ret_bind = S_look(fun->u.function.table, S_Symbol(Fun_ReturnKey));
        // 原地修改
        tylist->tail->head = ret_bind->value;
    }
    break;
    case A_classVarExp:
    {

        Ty_tyList obj_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.classvar.obj, obj_tylist))
            return FALSE;
        if (Ty_Value(obj_tylist)->kind != Ty_class)
        {
            fprintf(stderr, "Class Var failed: access non-Object type\n");
            goto error;
        }

        S_bind var = S_look(Ty_Value(obj_tylist)->u.module.table, S_Symbol(exp->u.classvar.var));
        if (!var)
        {
            fprintf(stderr, "Class Var failed: Class doesn't have Var %s\n", exp->u.classvar.var);
            goto error;
        }
        // printf("Var: %s\n", exp->u.classvar.var);
        // printf("Var get: %d\n", )

        tylist->head = var->location;
        tylist->tail->head = var->value;
    }
    break;
    case A_boolConst:
        // 原地修改
        tylist->tail->head = Ty_Int();
        break;
    case A_numConst:
        // 原地修改
        tylist->tail->head = Ty_Int();
        break;
    case A_idExp:
    {
        S_table scope = NULL;
        if (current_fun)
        {
            scope = current_fun->u.function.table;
        }
        else
        {
            scope = t;
        }
        S_bind id_bind = S_look(scope, S_Symbol(exp->u.v));

        if (!id_bind)
        {
            fprintf(stderr, "Var failed: Variable not defined\n");
            goto error;
        }

        // 原地修改
        tylist->head = id_bind->location;
        tylist->tail->head = id_bind->value;
    }
    break;
    case A_thisExp:
    {
        if (!current_class)
        {
            fprintf(stderr, "This failed: use This outside Class\n");
            goto error;
        }

        // 原地修改
        tylist->head = tylist->tail->head = current_class;
    }
    break;
    case A_lengthExp:
    {
        Ty_tyList len_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.e, len_tylist))
            return FALSE;
        if (Ty_Value(len_tylist)->kind != Ty_array)
        {
            fprintf(stderr, "Length failed: param is not Array\n");
            goto error;
        }
        // 原地修改
        tylist->tail->head = Ty_Int();
    }
    break;
    case A_newIntArrExp:
    {
        Ty_tyList newInt_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.e, newInt_tylist))
            return FALSE;
        if (Ty_Value(newInt_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "NewIntArr failed: param is not Int\n");
            goto error;
        }
        // 原地修改
        tylist->tail->head = Ty_Array(Ty_Int());
    }
    break;
    case A_newObjExp:
    {
        S_bind newObj_bind = S_look(t, S_ClassSymbol(exp->u.v));
        if (!newObj_bind)
        {
            fprintf(stderr, "NewObj failed: Class is not defined\n");
            goto error;
        }
        assert(newObj_bind->value->kind == newObj_bind->location->kind && newObj_bind->value->kind == Ty_class);
        // 原地修改
        tylist->tail->head = newObj_bind->value;
    }
    break;
    case A_notExp:
    {
        Ty_tyList not_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.e, not_tylist))
            return FALSE;
        if (Ty_Value(not_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "Not Expression failed: param is not Int\n");
            goto error;
        }
        tylist->tail->head = Ty_Int();
    }
    break;
    case A_minusExp:
    {
        Ty_tyList minus_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.e, minus_tylist))
            return FALSE;
        if (Ty_Value(minus_tylist)->kind != Ty_int)
        {
            fprintf(stderr, "Minus Expression failed: param is not Int\n");
            goto error;
        }
        tylist->tail->head = Ty_Int();
    }
    break;
    case A_escExp:
        if (!checkStmList(exp->u.escExp.ns))
            return FALSE;
        if (!checkExp(exp->u.escExp.exp, tylist))
            return FALSE;
        break;
    case A_getint:
        // 原地修改
        tylist->tail->head = Ty_Int();
        break;
    case A_getch:
        tylist->tail->head = Ty_Int();
        break;
    case A_getarray:
    {
        Ty_tyList arr_tylist = Ty_TyList_Init();
        if (!checkExp(exp->u.e, arr_tylist))
            return FALSE;
        if (Ty_Location(arr_tylist)->kind != Ty_array)
        {
            fprintf(stderr, "GetArray failed: param is not Array\n");
            goto error;
        }
        tylist->tail->head = Ty_Int();
    }
    break;
    default:
        assert(0);
        break;
    }
    return TRUE;
error:
    printPos(exp->pos);
    return FALSE;
}