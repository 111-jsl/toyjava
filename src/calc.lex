%{
#include <stdio.h>
#include "fdmjast.h"
#include "y.tab.h"

int line = 1, pos = 1;

char c;
int calc(char *s, int len);
%}
/* 状态声明 */
%s COMMENT1 COMMENT2
/* 正规声明 */
/* 基本声明 */
nondigit ([_A-Za-z])
digit ([0-9])
integer ([1-9]{digit}*)
identifier ({nondigit}({nondigit}|{digit})*)
/* 注释声明 */
comment1_begin ("//")
comment1_end ("\n")
comment2_begin ("/*")
comment2_end ("*/")
/* 运算符声明 */
plus ("+")
minus ("-")
times ("*")
div ("/")
or ("||")
and ("&&")
less ("<")
le ("<=")
greater (">")
ge (">=")
eq ("==")
ne ("!=")



%%
<INITIAL>" "    {
    pos++;
}
<INITIAL>"\n"   {
    pos = 0;
    line++;
}
<INITIAL>"\t"   {
    //1 \t = 8 space
    pos += 8;
}
<COMMENT1>{comment1_end}    {
    pos = 0;
    line++;
	 // printf("INITIAL\n");
    BEGIN INITIAL;
}
<COMMENT1>. {
    pos++;
	 // printf("COMMENT WORKS\n");
}
<COMMENT2>{comment2_end}	{
	pos += yyleng;
	BEGIN INITIAL;
}
<COMMENT2>"\n"	{
	pos = 0;
	line++;
}
<COMMENT2>.	{
	pos++;
}
<INITIAL>public  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("PUBLIC\n");
	return PUBLIC;
}
<INITIAL>class     {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("CLASS\n");
	return CLASS;
}
<INITIAL>extends    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("EXTENDS\n");
	return EXTENDS;
} 
<INITIAL>this    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("THIS\n");
	return THIS;
}
<INITIAL>new {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("NEW\n");
	return NEW;
}
<INITIAL>int {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("INT\n");
	return INT;
}
<INITIAL>true    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("BOOL_TRUE\n");
	return BOOL_TRUE;
}
<INITIAL>false   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("BOOL_FALSE\n");
	return BOOL_FALSE;
}
<INITIAL>if  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("IF\n");
	return IF;
}
<INITIAL>else    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("ELSE\n");
	return ELSE;
}
<INITIAL>while   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("WHILE\n");
	return WHILE;
}
<INITIAL>continue    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("CONTINUE\n");
	return CONTINUE;
}
<INITIAL>break   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("BREAK\n");
	return BREAK;
}
<INITIAL>main    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("MAIN\n");
	return MAIN;
}
<INITIAL>return  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("RETURN\n");
	return RETURN;
}
<INITIAL>putint  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("PUTINT\n");
	return PUTINT;
}
<INITIAL>putch   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("PUTCH\n");
	return PUTCH;
}
<INITIAL>putarray    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("PUTARRAY\n");
	return PUTARRAY;
}
<INITIAL>getint  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("GETINT\n");
	return GETINT;
}
<INITIAL>getch   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("GETCH\n");
	return GETCH;
}
<INITIAL>getarray    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("GETARRAY\n");
	return GETARRAY;
}
<INITIAL>starttime   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("STARTTIME\n");
	return STARTTIME;
}
<INITIAL>stoptime    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("STOPTIME\n");
	return STOPTIME;
}
<INITIAL>length    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	//  printf("LENGTH\n");
	return LENGTH;
}
<INITIAL>{plus}  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("PLUS\n");
	return PLUS;
}
<INITIAL>{minus} {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("MINUS\n");
	return MINUS;
}
<INITIAL>{times} {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("TIMES\n");
	return TIMES;
}
<INITIAL>{div}   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("DIV\n");
	return DIV;
}
<INITIAL>{or}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("OR\n");
	return OR;
}   
<INITIAL>{and}   {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("AND\n");
	return AND;
}
<INITIAL>{less}  {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("LESS\n");
	return LESS;
}
<INITIAL>{le}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("LE\n");
	return LE;
}
<INITIAL>{greater}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("LE\n");
	return GREATER;
}
<INITIAL>{ge}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("LE\n");
	return GE;
}
<INITIAL>{eq}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("EQ\n");
	return EQ;
}
<INITIAL>{ne}    {
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("NE\n");
	return NE;
}
<INITIAL>{comment1_begin}  {
	pos += yyleng;
	 // printf("comment1 encountered\n");
    BEGIN COMMENT1;
}
<INITIAL>{comment2_begin}	{
	pos += yyleng;
	BEGIN COMMENT2;
}
<INITIAL>{identifier}    {
	 // printf("id:\t%s\n", yytext);
    yylval.str.v = strdup(yytext);
    yylval.str.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("IDENTIFIER\n");
	return IDENTIFIER;
}
<INITIAL>{integer} {
    yylval.integer.v = calc(yytext, yyleng);
    yylval.integer.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("INT_CONST\n");
	return INT_CONST;
}
<INITIAL>0 {
    yylval.integer.v = 0;
    yylval.integer.pos = A_Pos(line, pos);
	pos += yyleng;
	 // printf("INT_CONST\n");
	return INT_CONST;
}

<INITIAL>[^\b]   {
    c = yytext[0];
    return c;
}
%%
int calc(char *s, int len) {
    int ret = 0;
    for(int i = 0; i < len; i++)
        ret = ret * 10 + (s[i] - '0');
    return ret;
}